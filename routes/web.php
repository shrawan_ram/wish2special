<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/change-city/{city_id}', 'HomeController@change_city');
Route::get('/', 'HomeController@index');  
Route::post('/ajax_request', 'AjaxController@index')->name('ajax-route');
Route::get('/project', 'ProjectController@index'); 
Route::get('/about-us', 'AboutController@index'); 
Route::get('project/{slug:slug}', 'SingleProjectController@index'); 
Route::get('/menu', 'ServiceController@index');
Route::get('service/{slug:slug}', 'SingleServiceController@index');
Route::get('/our-team', 'OurTeamController@index'); 
Route::get('/pages', 'PagesController@index'); 
Route::get('/blog', 'BlogController@index'); 
Route::get('/menu', 'MenuController@index'); 
Route::get('/privacy', 'PagesController@privacy'); 
Route::get('/term', 'PagesController@term'); 
Route::get('/security', 'PagesController@security'); 
Route::get('blog/info/{slug:slug}', 'BlogController@single')->name('bloginfo');
Route::post('/blog/save_comment', 'BlogController@save_comment')->name('save_comment'); 
// Route::get('/category', 'CategoryController@index'); 
Route::get('blog/{category:slug}', 'BlogController@index');
// Route::get('category/{category:slug_category}', 'CategoryController@index');
Route::get('/cost-calculater', 'CostCalculaterController@index'); 
Route::get('/contact', 'ContactController@index'); 
// Route::post('/contact/save', 'ContactController@save')->name('contact');
Route::post('contact/ajax_request', 'AjaxController@contactInquiry')->name('contact-route'); 

Route::get('/menu/{slug:slug}', 'ServiceController@single');

Route::group(['prefix' => 'cart'], function () {
    Route::get('/', 'CartController@index')->name('cart');
    Route::post('add', 'CartController@add');
    Route::post('update', 'CartController@update');
    Route::post('remove', 'CartController@remove');
});
Route::get('/terms-and-conditions', 'HomeController@terms')->name('terms');


Route::post('user_login', 'AjaxController@auth');
Route::post('user_register', 'AjaxController@register');
Route::post('user_verifyotp', 'AjaxController@verifyOtp');
Route::post('user_forgotpass', 'AjaxController@forgot');
Route::post('user_verifyforgototp', 'AjaxController@verifyForgotOtp');
Route::post('user_recoverpass', 'AjaxController@save_password');
Route::post('resend-otp', 'AjaxController@resendOtp');
Route::post('reservation_table', 'AjaxController@reservation');

Route::group(['prefix' => 'checkout_resv'], function () {
    Route::get('/', 'CheckoutresvController@index')->name('checkout-resv');
    Route::post('/', 'CheckoutresvController@save')->name('dopayment2');
    Route::get('thankyou', 'CheckoutresvController@thank_you');
});

Route::group(['prefix' => 'checkout', 'middleware' => 'auth'], function () {
    Route::get('/', 'CheckoutController@index')->name('checkout');
    Route::post('/', 'CheckoutController@save')->name('dopayment');
    Route::get('thank-you', 'CheckoutController@thank_you');
});



Route::group(['prefix' => 'guest'], function () {
    Route::get('/', 'GuestController@index')->name('guest');
    Route::post('/', 'GuestController@save')->name('dopayment1');
    Route::get('thank_you', 'GuestController@thank_you');
});
Route::group(['prefix' => 'ajax'], function () {
    Route::post('send-otp', 'AjaxController@sendOtp');
    Route::post('check-coupon', 'AjaxController@checkCoupon')->middleware('auth');
    Route::post('remove-coupon', 'AjaxController@removeCoupon')->middleware('auth');
    Route::get('/google', 'ContactController@google');
});

Route::group(['prefix' => 'account', 'middleware' => 'auth', 'namespace' => 'account'], function () {
    Route::get('/', 'UserController@profile')->name('profile');
    Route::get('/edit', 'UserController@edit_profile')->name('edit_profile');
    Route::post('/edit', 'UserController@save_profile');
    Route::get('/my-orders', 'OrderController@my_orders')->name('my_orders');
    Route::get('order-info/{order}', 'OrderController@single')->name('order_info');
    Route::post('/save-rating', 'OrderController@save_rating')->name('save_rating');
    Route::get('/change-password', 'UserController@change_password')->name('change_password');
    Route::post('/change-password', 'UserController@save_password');
    Route::get('/logout', 'UserController@logout')->name('logout');
    Route::get('/wishlist', 'OrderController@wishlist')->name('wishlist');
    
    Route::get('address', 'AddressController@list')->name('address');
    Route::get('add_address', 'AddressController@index');
    Route::post('add_address', 'AddressController@add')->name('add_address');
    Route::get('edit_address/{id}', 'AddressController@edit');
    Route::post('edit_address/{id}', 'AddressController@update')->name('edit_address');
    Route::get('delete_address/{id}', 'AddressController@destroy');
});
 




    
