<?php


use Illuminate\Support\Facades\Route;

// Login
    Route::get('admin-control/mobile', 'admin\MobileController@index');
    Route::get('admin-control/reset-password', 'admin\ResetpasswordController@index');

Route::group(['prefix' => 'admin-control', 'middleware' => 'guest'], function() {
    Route::get( '/', [ 'as' => 'login', 'uses' => 'admin\UserController@login'] );
    Route::post( 'login-auth', 'admin\UserController@auth' );
});

// Logout

Route::group(['prefix' => 'admin-control', 'middleware' => 'auth'], function() {
    Route::get( 'logout', 'admin\UserController@logout' );
    Route::get( 'dashboard', ['as' => 'home', 'uses' => 'admin\DashboardController@index' ]  );

// dashboard

    Route::get('dashboard','admin\DashboardController@index');
    Route::get('/inquiry','admin\InquiryController@index');
    Route::get('inquiry/remove/{id}','admin\InquiryController@remove');
    Route::post('inquiry/removeMultiple','admin\InquiryController@removeMultiple');
    
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return 'clearede';
});



// Marquee-img

    Route::get('marquee-img', 'admin\MarqueeController@index');    
    Route::get('marquee-img/add','admin\MarqueeController@add');
    Route::post('marquee-img/add','admin\MarqueeController@adddata');
    Route::get('marquee-img/edit/{id}', 'admin\MarqueeController@edit');
    Route::post('marquee-img/edit/{id}', 'admin\MarqueeController@editdata');
    Route::get('marquee-img/remove/{id}','admin\MarqueeController@remove');
    Route::post('marquee-img/removeMultiple','admin\MarqueeController@removeMultiple');


// Unit

    Route::get('unit', 'admin\UnitController@index');    
    Route::get('unit/add','admin\UnitController@add');
    Route::post('unit/add','admin\UnitController@adddata');
    Route::get('unit/edit/{id}', 'admin\UnitController@edit');
    Route::post('unit/edit/{id}', 'admin\UnitController@editdata');
    Route::get('unit/remove/{id}','admin\UnitController@remove');
    Route::post('unit/removeMultiple','admin\UnitController@removeMultiple'); 

// Unit

    Route::get('coupon', 'admin\CouponController@index');    
    Route::get('coupon/add','admin\CouponController@add');
    Route::post('coupon/add','admin\CouponController@adddata');
    Route::get('coupon/edit/{id}', 'admin\CouponController@edit');
    Route::post('coupon/edit/{id}', 'admin\CouponController@editdata');
    Route::get('coupon/remove/{id}','admin\CouponController@remove');
    Route::post('coupon/removeMultiple','admin\CouponController@removeMultiple'); 

// City

    Route::get('city', 'admin\CityController@index');    
    Route::get('city/add','admin\CityController@add');
    Route::post('city/add','admin\CityController@adddata');
    Route::get('city/edit/{id}', 'admin\CityController@edit');
    Route::post('city/edit/{id}', 'admin\CityController@editdata');
    Route::get('city/remove/{id}','admin\CityController@remove');
    Route::post('city/removeMultiple','admin\CityController@removeMultiple'); 

// Vendor

    Route::get('vendor', 'admin\VendorController@index');    
    Route::get('vendor/add','admin\VendorController@add');
    Route::post('vendor/add','admin\VendorController@adddata');
    Route::get('vendor/edit/{id}', 'admin\VendorController@edit');
    Route::post('vendor/edit/{id}', 'admin\VendorController@editdata');
    Route::get('vendor/remove/{id}','admin\VendorController@remove');
    Route::post('vendor/removeMultiple','admin\VendorController@removeMultiple'); 

// Product

    Route::get('product', 'admin\ProductController@index');    
    Route::get('product/add','admin\ProductController@add');
    Route::post('product/add','admin\ProductController@adddata');
    Route::get('product/edit/{id}', 'admin\ProductController@edit');
    Route::post('product/edit/{id}', 'admin\ProductController@editdata');
    Route::get('product/remove/{id}','admin\ProductController@remove');
    Route::post('product/removeMultiple','admin\ProductController@removeMultiple');

// Product

    Route::get('product-detail', 'admin\ProductdetailController@index');    
    Route::get('product-detail/add','admin\ProductdetailController@add');
    Route::post('product-detail/add','admin\ProductdetailController@adddata');
    Route::get('product-detail/edit/{id}', 'admin\ProductdetailController@edit');
    Route::post('product-detail/edit/{id}', 'admin\ProductdetailController@editdata');
    Route::get('product-detail/remove/{id}','admin\ProductdetailController@remove');
    Route::post('product-detail/removeMultiple','admin\ProductdetailController@removeMultiple');


// Project

    Route::get('project', 'admin\ProjectController@index');    
    Route::get('project/add','admin\ProjectController@add');
    Route::post('project/add','admin\ProjectController@adddata');
    Route::get('project/edit/{id}', 'admin\ProjectController@edit');
    Route::post('project/edit/{id}', 'admin\ProjectController@editdata');
    Route::get('project/remove/{id}','admin\ProjectController@remove');
    Route::post('project/removeMultiple','admin\ProjectController@removeMultiple');    


// Client

    Route::get('client', 'admin\ClientController@index');    
    Route::get('client/add','admin\ClientController@add');
    Route::post('client/add','admin\ClientController@adddata');
    Route::get('client/edit/{id}', 'admin\ClientController@edit');
    Route::post('client/edit/{id}', 'admin\ClientController@editdata');
    Route::get('client/remove/{id}','admin\ClientController@remove');
    Route::post('client/removeMultiple','admin\ClientController@removeMultiple');  


// Plan

    Route::get('plan', 'admin\PlanController@index');    
    Route::get('plan/add','admin\PlanController@add');
    Route::post('plan/add','admin\PlanController@adddata');
    Route::get('plan/edit/{id}', 'admin\PlanController@edit');
    Route::post('plan/edit/{id}', 'admin\PlanController@editdata');
    Route::get('plan/remove/{id}','admin\PlanController@remove');
    Route::post('plan/removeMultiple','admin\PlanController@removeMultiple');    


// blog

    Route::get('blog', 'admin\BlogController@index');    
    Route::get('blog/add','admin\BlogController@add');
    Route::post('blog/add','admin\BlogController@adddata');
    Route::get('blog/edit/{id}', 'admin\BlogController@edit');
    Route::post('blog/edit/{id}', 'admin\BlogController@editdata');
    Route::get('blog/remove/{id}','admin\BlogController@remove');
    Route::post('blog/removeMultiple','admin\BlogController@removeMultiple');    



// Slider

    Route::get('slider', 'admin\SliderController@index');    
    Route::get('slider/add','admin\SliderController@add');
    Route::post('slider/add','admin\SliderController@adddata');
    Route::get('slider/edit/{id}', 'admin\SliderController@edit');
    Route::post('slider/edit/{id}', 'admin\SliderController@editdata');
    Route::get('slider/remove/{id}','admin\SliderController@remove');
    Route::post('slider/removeMultiple','admin\SliderController@removeMultiple');


// Offer

    Route::get('offer', 'admin\OfferController@index');    
    Route::get('offer/add','admin\OfferController@add');
    Route::post('offer/add','admin\OfferController@adddata');
    Route::get('offer/edit/{id}', 'admin\OfferController@edit');
    Route::post('offer/edit/{id}', 'admin\OfferController@editdata');
    Route::get('offer/remove/{id}','admin\OfferController@remove');
    Route::post('offer/removeMultiple','admin\OfferController@removeMultiple');

// Setting

    Route::get('setting', 'admin\SettingController@index');
    Route::get('setting/edit/{id}', 'admin\SettingController@edit');
    Route::post('setting/edit/{id}', 'admin\SettingController@editdata');


// About us

    Route::get('about', 'admin\AboutController@index');
    Route::get('about/edit/{id}', 'admin\AboutController@edit');
    Route::post('about/edit/{id}', 'admin\AboutController@editdata');

// Privacy

    Route::get('privacy/edit/{id}', 'admin\PagesController@privacyedit');
    Route::post('privacy/edit/{id}', 'admin\PagesController@privacyeditdata');

// Term

    
    Route::get('term/edit/{id}', 'admin\PagesController@termedit');
    Route::post('term/edit/{id}', 'admin\PagesController@termeditdata');

// Security

    
    Route::get('security/edit/{id}', 'admin\PagesController@securityedit');
    Route::post('security/edit/{id}', 'admin\PagesController@securityeditdata');

// About us

    Route::get('whatdo', 'admin\whatdoController@index');
    Route::get('whatdo/edit/{id}', 'admin\whatdoController@edit');
    Route::post('whatdo/edit/{id}', 'admin\whatdoController@editdata');


// category

    Route::get('category', 'admin\CategoryController@index');
    Route::get('category/add', 'admin\CategoryController@add');
    Route::post('category/add', 'admin\CategoryController@adddata');
    Route::get('category/edit/{id}', 'admin\CategoryController@edit');
    Route::post('category/edit/{id}', 'admin\CategoryController@editdata');
    Route::get('category/remove/{id}','admin\CategoryController@remove');
    Route::post('category/removeMultiple','admin\CategoryController@removeMultiple');

//
   

// category

    Route::get('faqs', 'admin\FaqsController@index');
    Route::get('faqs/add', 'admin\FaqsController@add');
    Route::post('faqs/add', 'admin\FaqsController@adddata');
    Route::get('faqs/edit/{id}', 'admin\FaqsController@edit');
    Route::post('faqs/edit/{id}', 'admin\FaqsController@editdata');
    Route::get('faqs/remove/{id}','admin\FaqsController@remove');
    Route::post('faqs/removeMultiple','admin\FaqsController@removeMultiple');
    


// change password

    Route::get('/change-password', 'admin\ChangepasswordController@index');
    Route::post('/change-password','admin\ChangepasswordController@changePassword');

    Route::get('order', 'admin\OrderController@order');
    Route::get('user', 'admin\PagesController@user');
    // Route::get('order/status/{order}', 'admin\OrderController@change_status')->name('order_status');
    // Route::get('order/status/{id}', 'admin\OrderController@change_status')->name('order_status');
    Route::get('order/{list}', 'admin\OrderController@infopage');
    Route::get('/order/status/{customer}', 'admin\OrderController@change_status')->name('order_status');
    
    Route::get('send-mail', 'admin\InquiryController@mail');
	Route::post('send-mail', 'admin\InquiryController@mailsend');
});