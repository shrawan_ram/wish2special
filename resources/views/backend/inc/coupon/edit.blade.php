	@extends('backend.layout.master')

	@section('title','coupon')

	@section('contant')
<div class="admin-form-main-block mrgn-t-40">
    <h4 class="admin-form-text"><a href="{{url('admin-control/coupon')}}" data-toggle="tooltip" data-original-title="Go back" class="btn-floating"><i class="material-icons">reply</i></a> Edit Blog</h4> 
    {!! Form::open(['method' => 'POST', 'files' => true]) !!}
    <div class="row admin-form-block z-depth-1">
      <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
              {{Form::label('code', 'Enter Coupan Code')}}
              {{Form::text('record[code]', '', ['class' => 'form-control', 'placeholder'=>'Enter Coupan Code','id'=>'code','required'=>'required'])}}
            </div>
            <div class="form-group">
              {{Form::label('discount_type', 'Enter Discount Type')}}
              {{Form::select('record[discount_type]', $disArr, '', ['class' => 'form-control','id'=>'discount_type'])}}
            </div>
            <div class="form-group">
              {{Form::label('discount', 'Enter Discount')}}
              {{Form::number('record[discount]', '', ['class' => 'form-control','id'=>'discount', 'placeholder'=>'Enter Discount'])}}
            </div>
            <div class="form-group">
              {{Form::label('description', 'Enter Description')}}
              {{Form::textarea('record[description]', '', ['class' => 'form-control','id'=>'excerpt', 'placeholder'=>'Enter Description','rows'=>'4', 'col'=>'3'])}}
            </div>
          <!-- </div>
        <div class="col-lg-6"> -->
            <div class="form-group">
              <!-- <input type="checkbox" id="show" onclick="$('.limit_user').slideToggle(function(){$('#show').html($('.limit_user').is(':visible'));});"> -->
              {{Form::label('limit_user', 'Limit No. User')}}

              <div class="limit_user">
                <div class="form-group">
                  <!-- {{Form::label('limit_user', 'Enter Limit User')}} -->
                  {{Form::number('record[limit_user]', '', ['class' => 'form-control','id'=>'limit_user', 'placeholder'=>'Enter Limit User'])}}
                </div>
              </div>
            </div>

            <div class="form-group">
              <!-- <input type="checkbox" id="show" onclick="$('.limit_per_user').slideToggle(function(){$('#show').html($('.limit_per_user').is(':visible'));});"> -->
              {{Form::label('limit_per_user', 'Limit No. Per User')}}

              <div class="limit_per_user">
                <div class="form-group">
                  <!-- {{Form::label('limit_per_user', 'Enter Limit No. Per User')}} -->
                  {{Form::number('record[limit_per_user]', '', ['class' => 'form-control','id'=>'limit_per_user', 'placeholder'=>'Enter Limit No. Per User'])}}
                </div>
              </div>
            </div>

          </div>
          <div class="col-lg-6">
            <div class="form-group">
              {{Form::label('title', 'Enter Coupan Title')}}
              {{Form::text('record[title]', '', ['class' => 'form-control', 'placeholder'=>'Enter Coupan Title','id'=>'title','required'=>'required'])}}
            </div>
            <div class="form-group">
              {{Form::label('valid_from', 'Enter Valid From')}}
              {{Form::date('record[valid_from]','', ['class'=>'form-control', 'placeholder'=>'Enter Valid From', 'id'=>'valid_from'])}}
            </div>
            <div class="form-group">
              {{Form::label('term_condition', 'Enter Term & Condition')}}
              {{Form::textarea('record[term_condition]', '', ['class' => 'form-control','id'=>'term_condition', 'placeholder'=>'Enter Term & Condition','rows'=>'7', 'col'=>'3'])}}
            </div>
            <div class="form-group">
              <!-- <input type="checkbox" name="record[never_end]" value="1" id="show" onclick="$('.valid_upto').slideToggle(function(){$('#show').html($('.valid_upto').is(':visible'));});"> -->
              {{Form::label('never_end', 'Valid Upto')}}

              <div class="valid_upto">
                <div class="form-group">
                  {{Form::date('record[valid_upto]','', ['class'=>'form-control', 'placeholder'=>'Enter Valid Upto', 'id'=>'valid_upto'])}}
                </div>
              </div>
            </div>
            <!--<div class="form-group">-->
            <!--  {{Form::label('valid_upto', 'Enter Valid Upto')}}-->
            <!--  {{Form::date('record[valid_upto]','', ['class'=>'form-control', 'placeholder'=>'Enter Valid Upto', 'id'=>'valid_upto'])}}-->
            <!--</div>-->
            
            <!--<div class="form-group">-->
            <!--  {{Form::label('menu_includes', 'Enter Menu Includes')}}-->
            <!--  {{Form::textarea('record[menu_includes]', '', ['class' => 'form-control','id'=>'menu_includes', 'placeholder'=>'Enter Menu Includes','rows'=>'4', 'col'=>'3'])}}-->
            <!--</div>-->
            <!--<div class="form-group">-->
            <!--  {{Form::label('menu_excludes', 'Enter Menu Excludes')}}-->
            <!--  {{Form::textarea('record[menu_excludes]', '', ['class' => 'form-control','id'=>'menu_excludes', 'placeholder'=>'Enter Menu Excludes','rows'=>'4', 'col'=>'3'])}}-->
            <!--</div>-->
            
            <div class="form-group">
              <!-- <input type="checkbox" id="show" onclick="$('.total_coupon').slideToggle(function(){$('#show').html($('.total_coupon').is(':visible'));});"> -->
              {{Form::label('total_coupon', 'Total No. Of Coupon')}}

              <div class="total_coupon">
                <div class="form-group">
                  <!-- {{Form::label('total_coupon', 'Enter Total No. Of Coupan')}} -->
                  {{Form::number('record[total_coupon]', '', ['class' => 'form-control','id'=>'total_coupon', 'placeholder'=>'Enter Total No. Of Coupon'])}}
                </div>
              </div>
            </div>

            <div class="form-group">
              <!-- <input type="checkbox" id="show" onclick="$('.min_cart_amt').slideToggle(function(){$('#show').html($('.min_cart_amt').is(':visible'));});"> -->
              {{Form::label('min_cart_amt', 'Min Cart Total Amount')}}

              <div class="min_cart_amt">
                <div class="form-group">
                  <!-- {{Form::label('min_cart_amt', 'Enter Min Cart Total Amount')}} -->
                  {{Form::number('record[min_cart_amt]', '', ['class' => 'form-control','id'=>'min_cart_amt', 'placeholder'=>'Enter Min Cart Total Amount'])}}
                </div>
              </div>
              <div class="btn-group pull-right">
                  <button type="reset" class="btn btn-info">Reset</button>
                  <button type="submit" class="btn btn-success">Create</button>
                </div>
            </div>
            
          </div>
        </div>
      </div>
    {!! Form::close() !!}
  </div>
@stop

