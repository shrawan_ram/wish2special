 @extends('backend.layout.master')

@section('title','coupon')

@section('contant')
		<div class="content-main-block  mrg-t-40">
    <div class="admin-create-btn-block">
      <a href="{{ url('admin-control/coupon/add') }}" class="btn btn-danger btn-md">Add Coupon</a>
      <!-- Delete Modal -->
      <a type="button" class="btn btn-danger btn-md" data-toggle="modal" data-target="#bulk_delete"><i class="material-icons left">delete</i> Delete Selected</a>
         
      <!-- Modal -->
      <div id="bulk_delete" class="delete-modal modal fade" role="dialog">
        <div class="modal-dialog modal-sm">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <div class="delete-icon"></div>
            </div>
            <div class="modal-body text-center">
              <h4 class="modal-heading">Are You Sure ?</h4>
              <p>Do you really want to delete these records? This process cannot be undone.</p>
            </div>
            <div class="modal-footer">
              {!! Form::open(['method' => 'POST', 'url' => url('admin-control/coupon/removeMultiple') ,'id' => 'bulk_delete_form']) !!}
                <button type="reset" class="btn btn-gray translate-y-3" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-danger">Yes</button>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="content-block box-body">
      <table id="full_detail_table" class="table table-hover table-responsive">
        <thead>
          <tr class="table-heading-row">
            <th>
              <div class="inline">
                <input id="checkboxAll" type="checkbox" class="filled-in" name="checked[]" value="all" id="checkboxAll">
                <label for="checkboxAll" class="material-checkbox"></label>
              </div>
            #</th>
            <th>Code</th>
                       <!-- <th>DiscType</th> -->
            <th>Discount <span class="small">(%,₹)</span></th>
            <th>Valid Date <span class="small">(Start,End)</span></th>
            <th>Limit Users</th>
            <th>Limit Per User</th>
            <th>Total Coupan</th>
            <th>Min. Cart Amount</th>
            <th>Action</th>
          </tr>
        </thead>
          @if (isset($coupon))
          <tbody>
            @foreach ($coupon as $key => $list)
              <tr>
                <td>
                  <div class="inline">
                    <input type="checkbox" form="bulk_delete_form" class="filled-in material-checkbox-input" name="checked[]" value="{{$list->id}}" id="checkbox{{$list->id}}">
                    <label for="checkbox{{$list->id}}" class="material-checkbox"></label>
                  </div>
                  {{$key+1}}
                </td>
                <td class="nowrap">{{ $list->code }}</td>
                         @php
                            $disType = $list->discount_type;
                            $validupto = $list->never_end;
                         @endphp
                         @if($disType == 'Flat')
                            <td>{{ $list->discount }}₹</td>
                         @else
                            <td>{{ $list->discount }}%</td>
                         @endif
                         <!-- <td>{{ $list->discount }}</td> -->
                         <td>
                             {{ $list->valid_from }}<br>To<br>
                             @if($validupto == '0')
                                {{ $list->valid_upto }}
                            @else
                                Never End
                            @endif
                         </td>
                         <td>{{ $list->limit_user }}</td>
                         <td>{{ $list->limit_per_user }}</td>
                         <td>{{ $list->total_coupon }}</td>
                         <td>{{ $list->min_cart_amt }}₹</td>
                <td>
                  <div class="admin-table-action-block">
                    <a href="{{url('admin-control/coupon/edit/'.$list->id)}}" data-toggle="tooltip" data-original-title="Edit" class="btn-info btn-floating"><i class="material-icons">mode_edit</i></a>
                    <!-- Delete Modal -->
                    <button type="button" class="btn-danger btn-floating" data-toggle="modal" data-target="#{{$list->id}}deleteModal"><i class="material-icons">delete</i> </button>
                    <!-- Modal -->
                    <div id="{{$list->id}}deleteModal" class="delete-modal modal fade" role="dialog">
                      <div class="modal-dialog modal-sm">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <div class="delete-icon"></div>
                          </div>
                          <div class="modal-body text-center">
                            <h4 class="modal-heading">Are You Sure ?</h4>
                            <p>Do you really want to delete these records? This process cannot be undone.</p>
                          </div>
                          <div class="modal-footer">
                            {!! Form::open(['url' => url('admin-control/coupon/remove/'.$list->id),'method' => 'GET', $list->id]) !!}
                                <button type="reset" class="btn btn-gray translate-y-3" data-dismiss="modal">No</button>
                                <button type="submit" class="btn btn-danger">Yes</button>
                            {!! Form::close() !!}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            @endforeach
          </tbody>
        @endif  
      </table>
    </div>
  </div>
@stop