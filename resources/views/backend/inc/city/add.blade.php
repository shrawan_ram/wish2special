@extends('backend.layout.master')

@section('title','city')

@section('contant')
<div class="admin-form-main-block mrgn-t-40">
    <h4 class="admin-form-text"><a href="{{url('admin/city')}}" data-toggle="tooltip" data-original-title="Go back" class="btn-floating"><i class="material-icons">reply</i></a> Add City</h4> 
    {!! Form::open(['method' => 'POST', 'files' => true]) !!}
      <div class="row admin-form-block z-depth-1">
        <div class="col-md-6">
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              {!! Form::label('name', 'City Name') !!} - 
              {!! Form::text('name', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('name') }}</small>
          </div> 
		  
		  
		   <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
              {!! Form::label('slug', 'City Slug') !!} - <p class="inline info"></p>
              {!! Form::text('slug', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('slug') }}</small>
          </div> 
          <div class="form-group{{ $errors->has('sid') ? ' has-error' : '' }}">
              {!! Form::label('sid', 'Select State') !!}
             
              {{ Form::select('sid', $parentArr, '', ['class' => 'form-control select2']) }}
              <small class="text-danger">{{ $errors->first('sid') }}</small>
          </div>   
          <div class="btn-group pull-right">
            <button type="reset" class="btn btn-info">Reset</button>
            <button type="submit" class="btn btn-success">Create</button>
          </div>
          <div class="clear-both"></div>
        </div>  
      </div>
    {!! Form::close() !!}
  </div>
@stop
		    	
				    

			
		
	
