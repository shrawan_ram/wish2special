@extends('backend.layout.master')

@section('title','vendor')

@section('contant')

<div class="admin-form-main-block mrgn-t-40">
    <h4 class="admin-form-text"><a href="{{url('admin/vendor')}}" data-toggle="tooltip" data-original-title="Go back" class="btn-floating"><i class="material-icons">reply</i></a> Edit Vendor</h4> 
    {!! Form::open(['method' => 'POST', 'files' => true]) !!}
      <div class="row admin-form-block z-depth-1">
        <div class="col-md-12">

          <div class="col-md-6 form-group{{ $errors->has('owner_name') ? ' has-error' : '' }}">
              {!! Form::label('owner_name', 'Vendor Name') !!}
              {!! Form::text('owner_name', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('owner_name') }}</small>
          </div> 
      
      
          <div class="col-md-6 form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
              {!! Form::label('slug', 'Vendor Slug') !!} - <p class="inline info"></p>
              {!! Form::text('slug', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('slug') }}</small>
          </div>
          <div class="col-md-6 form-group{{ $errors->has('shop_name') ? ' has-error' : '' }}">
              {!! Form::label('shop_name', 'Vendor shop_name') !!} - <p class="inline info"></p>
              {!! Form::text('shop_name', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('shop_name') }}</small>
          </div>
          <div class="col-md-6 form-group{{ $errors->has('pincode') ? ' has-error' : '' }}">
              {!! Form::label('pincode', 'Vendor Pincode') !!} - <p class="inline info"></p>
              {!! Form::text('pincode', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('pincode') }}</small>
          </div>
          <div class="col-md-12 form-group{{ $errors->has('city_id') ? ' has-error' : '' }}">
              {!! Form::label('city_id', 'Select City') !!}            
              {{ Form::select('city_id', $cityArr, '', ['class' => 'form-control select2']) }}
              <small class="text-danger">{{ $errors->first('city_id') }}</small>
          </div>
          <div class="col-md-12 form-group{{ $errors->has('state_id') ? ' has-error' : '' }}">
              {!! Form::label('state_id', 'Select State') !!}            
              {{ Form::select('state_id', $stateArr, '', ['class' => 'form-control select2']) }}
              <small class="text-danger">{{ $errors->first('state_id') }}</small>
          </div> 
          <div class="col-md-6 form-group{{ $errors->has('address') ? ' has-error' : '' }}">
              {!! Form::label('address', 'Vendor Address') !!} - <p class="inline info"></p>
              {!! Form::text('address', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('address') }}</small>
          </div>          
          <!-- <div class="col-md-12 summernote-main form-group{{ $errors->has('description') ? ' has-error' : '' }}">
            {!! Form::label('description', 'Description') !!}
            {!! Form::textarea('description', null, ['id' => 'summernote-main', 'class' => 'form-control']) !!}
            <small class="text-danger">{{ $errors->first('description') }}</small>
          </div> -->
          <div class="col-md-12 form-group{{ $errors->has('image') ? ' has-error' : '' }} input-file-block">
            {!! Form::label('image', 'Vendor Image') !!} 
            {!! Form::file('image', ['class' => 'input-file', 'id'=>'image']) !!}
            <label for="image" class="btn btn-danger js-labelFile" data-toggle="tooltip" data-original-title="Category Image">
              <i class="icon fa fa-check"></i>
              <span class="js-fileName">Choose a File</span>
            </label>
            <p class="info">Choose image</p>
            <small class="text-danger">{{ $errors->first('image') }}</small>
          </div> 

          
          <div class="btn-group pull-right">
            <button type="reset" class="btn btn-info">Reset</button>
            <button type="submit" class="btn btn-success">Update</button>
          </div>
          <div class="clear-both"></div>
        </div>  
      </div>
    {!! Form::close() !!}
  </div>
@stop






