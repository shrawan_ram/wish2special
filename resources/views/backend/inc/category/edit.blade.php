@extends('backend.layout.master')

@section('title','category')

@section('contant')
<div class="admin-form-main-block mrgn-t-40">
    <h4 class="admin-form-text"><a href="{{url('admin/category')}}" data-toggle="tooltip" data-original-title="Go back" class="btn-floating"><i class="material-icons">reply</i></a> Edit Category</h4> 
    {!! Form::model(['method' => 'PATCH','files' => true]) !!}
      <div class="row admin-form-block z-depth-1">
        <div class="col-md-6">
          <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
              {!! Form::label('title', 'Category Name / Title') !!} - 
              {!! Form::text('title', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('title') }}</small>
          </div>
		  
		  
		  <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
              {!! Form::label('slug', 'Category Slug') !!} - <p class="inline info"></p>
              {!! Form::text('slug', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('slug') }}</small>
          </div> 
		  
		  
		  
		  
		  
      <!--    <div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }} currency-symbol-block">
            {!! Form::label('icon', 'Category Icon / Symbol') !!}
            <p class="inline info"> - Please select catgeory symbol or category image</p>
              <div class="input-group">
                {!! Form::text('icon', null, ['class' => 'form-control category-icon-picker']) !!}
                <span class="input-group-addon simple-input"><i class="glyphicon glyphicon-user"></i></span>
              </div>
            <small class="text-danger">{{ $errors->first('icon') }}</small>
          </div>
		  --->
		  
        
          <!--<div class="form-group{{ $errors->has('image_i') ? ' has-error' : '' }} input-file-block">-->
          <!--  {!! Form::label('image_i', 'Category Image') !!} -->
          <!--  {!! Form::file('image_i', ['class' => 'input-file', 'id'=>'image_i']) !!}-->
          <!--  <label for="image_i" class="btn btn-danger js-labelFile" data-toggle="tooltip" data-original-title="Category Image">-->
          <!--    <i class="icon fa fa-check"></i>-->
          <!--    <span class="js-fileName">Choose a File</span>-->
          <!--  </label>-->
          <!--  <p class="info">Choose custom image</p>-->
          <!--  <small class="text-danger">{{ $errors->first('image_i') }}</small>-->
          <!--</div> -->
          <div class="form-group{{ $errors->has('is_active') ? ' has-error' : '' }} switch-main-block">
            <div class="row">
              <div class="col-xs-4">
                {!! Form::label('is_active', 'Status') !!}
              </div>
              <div class="col-xs-5 pad-0">
                <label class="switch">                
                  {!! Form::checkbox('is_active', null, null, ['class' => 'checkbox-switch']) !!}
                  <span class="slider round"></span>
                </label>
              </div>
            </div>
            <div class="col-xs-12">
              <small class="text-danger">{{ $errors->first('is_active') }}</small>
            </div>
          </div>         
          <div class="btn-group pull-right">
            <button type="submit" class="btn btn-success">Update</button>
          </div>
          <div class="clear-both"></div>
        </div>  
      </div>
    {!! Form::close() !!}
  </div>
@stop
		    	
				    

			
		
	
