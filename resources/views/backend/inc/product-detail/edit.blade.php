@extends('backend.layout.master')

@section('title','product')

@section('contant')

<div class="admin-form-main-block mrgn-t-40">
    <h4 class="admin-form-text"><a href="{{url('admin/product')}}" data-toggle="tooltip" data-original-title="Go back" class="btn-floating"><i class="material-icons">reply</i></a>Edit Product</h4> 
    {!! Form::open(['method' => 'POST', 'files' => true]) !!}
      <div class="row admin-form-block z-depth-1">
        <div class="col-md-12">

          <div class="col-md-12 form-group{{ $errors->has('p_id') ? ' has-error' : '' }}">
              {!! Form::label('p_id', 'Select Category') !!}            
              {{ Form::select('p_id', $productArr, '', ['class' => 'form-control select2']) }}
              <small class="text-danger">{{ $errors->first('p_id') }}</small>
          </div>
          <div class="col-md-12">
            <div class="row" id="dynamic_field">
              <div class="col-md-2 form-group">
                <label>Weight</label>
                <input type="text" name="volume" value="{{ $edit->volume }}" class="form-control">
              </div>
              <div class="col-md-3 form-group">
                <label>Unit</label>
                <!-- <input type="text" name="" class="form-control"> -->
               {{ Form::select('unit', $unitArr, '', ['class' => 'form-control select2']) }}
              </div>
              <div class="col-md-2 form-group">
                <label>Price</label>
                <input type="text" name="regular_price" value="{{ $edit->regular_price }}" class="form-control" id="regular_price">
              </div>
              <div class="col-md-2 form-group">
                <label>Discount</label>
                <input type="text" name="discount" value="{{ $edit->discount }}" class="form-control" id="discount">
              </div>
              <div class="col-md-2 form-group">
                <label>Sale Price</label>
                <input type="text" name="sale_price" value="{{ $edit->sale_price }}" class="form-control" id="sale_price">
              </div>
              <!-- <div class="col-md-1 form-group">
                <input type="button" name="" id="add" value="+" class="btn btn-success">
              </div> -->
            </div>
          </div>
          
          <div class="btn-group pull-right">
            <button type="reset" class="btn btn-info">Reset</button>
            <button type="submit" class="btn btn-success">Update</button>
          </div>
          <div class="clear-both"></div>
        </div>  
      </div>
    {!! Form::close() !!}
  </div>
@stop






