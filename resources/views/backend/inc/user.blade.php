@extends('backend.layout.master')

@section('title','User')

@section('contant')
		<div class="content-main-block  mrg-t-40">
    <div class="admin-create-btn-block">
      <a  class="btn btn-danger btn-md">User</a>
    </div>
    <div class="content-block box-body">
      <table id="full_detail_table" class="table table-hover table-responsive">
        <thead>
          <tr class="table-heading-row">
            <th>
              <div class="inline">
                
              </div>
            #</th>
            <!-- <th>Image</th> -->
            <th>Name</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Role</th>
            
          </tr>
        </thead>
          @if (isset($user))
          <tbody>
            @foreach ($user as $key => $item)
              <tr>
                <td>
                  <div class="inline">
                    
                    <label for="checkbox{{$item->id}}" class="material-checkbox"></label>
                  </div>
                  {{$key+1}}
                </td>
                
                <td>{{ $item->name }}</td>
                <td>{{$item->email }}</td>
                <td>{{$item->mobile }}</td>
                <td>
                  @if($item->role = 3)
                      User
                      @else
                      -
                      @endif

                </td>
                
              </tr>
            @endforeach
          </tbody>
        @endif  
      </table>
    </div>
  </div>
@stop