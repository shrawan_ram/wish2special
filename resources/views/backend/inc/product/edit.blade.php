@extends('backend.layout.master')

@section('title','product')

@section('contant')

<div class="admin-form-main-block mrgn-t-40">
    <h4 class="admin-form-text"><a href="{{url('admin/product')}}" data-toggle="tooltip" data-original-title="Go back" class="btn-floating"><i class="material-icons">reply</i></a>Edit Product</h4> 
    {!! Form::open(['method' => 'POST', 'files' => true]) !!}
      <div class="row admin-form-block z-depth-1">
        <div class="col-md-12">

          <div class="col-md-6 form-group{{ $errors->has('title') ? ' has-error' : '' }}">
              {!! Form::label('title', 'Product Name / Title') !!} - <p class="inline info">Like electronics, clothing</p>
              {!! Form::text('title', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('title') }}</small>
          </div> 
		  
		  
		   <div class="col-md-6 form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
              {!! Form::label('slug', 'Product Slug') !!} - <p class="inline info"></p>
              {!! Form::text('slug', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('slug') }}</small>
          </div> 
          <!-- <div class="col-md-6 form-group{{ $errors->has('regular_price') ? ' has-error' : '' }}">
              {!! Form::label('regular_price', 'Regular Price') !!} - <p class="inline info"></p>
              {!! Form::text('regular_price', null, ['class' => 'form-control', 'id'=>'regular_price',]) !!}
              <small class="text-danger">{{ $errors->first('regular_price') }}</small>
          </div>
          <div class="col-md-6 form-group">
              {!! Form::label('discount_type', 'Discount Type') !!}
              <select name="" onchange="myFunction(this.value);" class="form-control select2">
               <option>Select Discount type</option>
               <option value="percent">percent</option>
               <option value="flat">flat</option>
               
            </select>
              <small class="text-danger">{{ $errors->first('discount_type') }}</small>
          </div>
         

            
          <div class="col-md-6 form-group{{ $errors->has('discount') ? ' has-error' : '' }} percent discount_none">
              {!! Form::label('discount', 'Discount') !!} - <p class="inline info"></p>
              {!! Form::text('discount', null, ['class' => 'form-control ', 'id'=>'discount']) !!}
              <small class="text-danger">{{ $errors->first('discount') }}</small>
          </div>
          <div class="col-md-6 form-group{{ $errors->has('discount') ? ' has-error' : '' }} flat discount_none">
              {!! Form::label('discount', 'Discount') !!} - <p class="inline info"></p>
              {!! Form::text('discount', null, ['class' => 'form-control ', 'id'=>'flat']) !!}
              <small class="text-danger">{{ $errors->first('discount') }}</small>
          </div>
        <div class="col-md-6 form-group{{ $errors->has('sale_price') ? ' has-error' : '' }}">
              {!! Form::label('sale_price', 'Sale Price') !!} - <p class="inline info"></p>
              {!! Form::text('sale_price', null, ['class' => 'form-control', 'id'=>'sale_price']) !!}
              <small class="text-danger">{{ $errors->first('sale_price') }}</small>
          </div>
          <div class="col-md-6 form-group{{ $errors->has('weight') ? ' has-error' : '' }}">
              {!! Form::label('weight', 'Weight') !!} - <p class="inline info"></p>
              <select name="weight[]" id="weight" class="form-control select2" multiple="multiple">
                  <option value="100gm">100gm</option>
                  <option value="250gm">250gm</option>
                  <option value="500gm">500gm</option>
                  <option value="750gm">750gm</option>
                  <option value="1kg">1kg</option>
              </select>
              <small class="text-danger">{{ $errors->first('weight') }}</small>
          </div> -->
          <div class="col-md-12 form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
              {!! Form::label('category_id', 'Select Category') !!}            
              {{ Form::select('category_id', $parentArr, '', ['class' => 'form-control select2']) }}
              <small class="text-danger">{{ $errors->first('category_id') }}</small>
          </div>
          <div class="col-md-12 form-group{{ $errors->has('vendor_id') ? ' has-error' : '' }}">
              {!! Form::label('vendor_id', 'Select Vendor') !!}            
              {{ Form::select('vendor_id', $vendorArr, '', ['class' => 'form-control select2']) }}
              <small class="text-danger">{{ $errors->first('vendor_id') }}</small>
          </div>
          <!-- <div class="col-md-12">
            <div class="row" id="dynamic_field">
              <div class="col-md-2 form-group">
                <label>Weight</label>
                <input type="text" name="weight[]" class="form-control">
              </div>
              <div class="col-md-3 form-group">
                <label>Unit</label>
                <select class="form-control select2" name="unit[]">
                  <option>Select Unit</option>
                  <option>gm</option>
                  <option>kg</option>
                </select>
              </div>
              <div class="col-md-2 form-group">
                <label>Price</label>
                <input type="text" name="regular_price[]" class="form-control" id="regular_price">
              </div>
              <div class="col-md-2 form-group">
                <label>Discount</label>
                <input type="text" name="discount[]" class="form-control" id="discount">
              </div>
              <div class="col-md-2 form-group">
                <label>Sale Price</label>
                <input type="text" name="sale_price[]" class="form-control" id="sale_price">
              </div>
              <div class="col-md-1 form-group">
                <input type="button" name="" id="add" value="+" class="btn btn-success">
              </div>
            </div>
          </div> -->
          <div class="col-md-12 form-group{{ $errors->has('excerpt') ? ' has-error' : '' }}">
              {!! Form::label('excerpt', 'Product Short Description') !!} - <p class="inline info"></p>
              {!! Form::textarea('excerpt', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('excerpt') }}</small>
          </div>
          
          <div class="col-md-12 summernote-main form-group{{ $errors->has('description') ? ' has-error' : '' }}">
            {!! Form::label('description', 'Description') !!}
            {!! Form::textarea('description', null, ['id' => 'summernote-main', 'class' => 'form-control']) !!}
            <small class="text-danger">{{ $errors->first('description') }}</small>
          </div>
          

          
		  
		  
     <!--     <div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }} currency-symbol-block">
            {!! Form::label('icon', 'Category Icon / Symbol') !!}
            <p class="inline info"> - Please select catgeory symbol or category image</p>
              <div class="input-group">
                {!! Form::text('icon', null, ['class' => 'form-control category-icon-picker']) !!}
                <span class="input-group-addon simple-input"><i class="glyphicon glyphicon-th-large"></i></span>
              </div>
            <small class="text-danger">{{ $errors->first('icon') }}</small>
          </div>
		  -->
		  
       
          <div class="col-md-12 form-group{{ $errors->has('image') ? ' has-error' : '' }} input-file-block">
            {!! Form::label('image', 'Product Image') !!} 
            {!! Form::file('image', ['class' => 'input-file', 'id'=>'image']) !!}
            <label for="image" class="btn btn-danger js-labelFile" data-toggle="tooltip" data-original-title="Category Image">
              <i class="icon fa fa-check"></i>
              <span class="js-fileName">Choose a File</span>
            </label>
            <p class="info">Choose custom image</p>
            <small class="text-danger">{{ $errors->first('image') }}</small>
          </div> 

          <h6 class="col-md-12"><p class="bg-primary text-white text-center p-4 font-weight-bold">Meta Info</p></h6>  

          <div class="col-md-6 form-group{{ $errors->has('seo_title') ? ' has-error' : '' }}">
              {!! Form::label('seo_title', 'Seo Title') !!} - <p class="inline info">Like electronics, clothing</p>
              {!! Form::text('seo_title', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('seo_title') }}</small>
          </div>
          <div class="col-md-6 form-group{{ $errors->has('seo_keywords') ? ' has-error' : '' }}">
              {!! Form::label('seo_keywords', 'Seo Keywords') !!} - <p class="inline info">Like electronics, clothing</p>
              {!! Form::text('seo_keywords', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('seo_keywords') }}</small>
          </div>
          <div class="col-md-12 form-group{{ $errors->has('seo_description') ? ' has-error' : '' }}">
              {!! Form::label('seo_description', 'Seo Description') !!} - <p class="inline info"></p>
              {!! Form::textarea('seo_description', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('seo_description') }}</small>
          </div>
          <div class="btn-group pull-right">
            <button type="reset" class="btn btn-info">Reset</button>
            <button type="submit" class="btn btn-success">Update</button>
          </div>
          <div class="clear-both"></div>
        </div>  
      </div>
    {!! Form::close() !!}
  </div>
@stop






