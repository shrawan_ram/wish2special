 @extends('backend.layout.master')

@section('title','Order')

@section('contant')
		<div class="content-main-block  mrg-t-40">
    <div class="admin-create-btn-block">
      <a  class="btn btn-danger btn-md">Order</a>
    </div>
    <div class="content-block box-body">
      <table id="full_detail_table" class="table table-hover table-responsive">
        <thead>
          <tr class="table-heading-row">
            <th>
              <div class="inline">
                
              </div>
            #</th>
            <!-- <th>Image</th> -->
            <th>Invoice</th>
            <th>Customer Name</th>
            <th>Trans. Type</th>
            <th>PaymentType</th>
            <th>Total Amount</th>
            <!-- <th>Status</th> -->
            <th>Date</th>
            <th>Detail</th>
          </tr>
        </thead>
          @if (isset($order))
          <tbody>
            @foreach ($order as $key => $item)
              <tr>
                <td>
                  <div class="inline">
                    
                    <label for="checkbox{{$item->id}}" class="material-checkbox"></label>
                  </div>
                  {{$key+1}}
                </td>
                
                <td>{{ sprintf("%s/%s/%04d", $setting->invoice_prefix, $item->financial_year, $item->invoice_no) }}</td>
                <td>{{$item->user ? $item->user->name : '-'}}</td>
                <td>{{$item->delivery_type ? $item->delivery_type : '-'}}</td>
                
                <td>{{$item->payment_mode ? $item->payment_mode : '-'}}</td>
                <td>{{$item->total_amount ? $item->total_amount : '-'}}</td>
                <!-- <td>
                  @php
                  $statusArr = [
                    "In-Queue" => "In-Queue",
                    "Preparing" => "Preparing",
                    "Cancelled" => "Cancelled",
                    "Delivered" => "Delivered",
                  ];
                  @endphp
                {{ Form::select("order_status", $statusArr, $item->status, ["onchange" => "statuschange(this)", "data-url" => route('order_status', $item->id)]) }}
                
               </td> -->
                <td>{{$item->updated_at->format('F d, Y h:i A')}}</td>
                <td><a href="{{ url(env('ADMIN_DIR').'/order/'.$item->id) }}">More</a></td>
              </tr>
            @endforeach
          </tbody>
        @endif  
      </table>
    </div>
  </div>
@stop