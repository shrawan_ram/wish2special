@extends('backend.layout.master')

@section('title','faqs')

@section('contant')

<div class="admin-form-main-block mrgn-t-40">
    <h4 class="admin-form-text"><a href="{{url('admin/faqs')}}" data-toggle="tooltip" data-original-title="Go back" class="btn-floating"><i class="material-icons">reply</i></a> Add Faq</h4> 
    {!! Form::open(['method' => 'POST', 'files' => true]) !!}
      <div class="row admin-form-block z-depth-1">
        <div class="col-md-12">

          <div class="form-group{{ $errors->has('question') ? ' has-error' : '' }}">
              {!! Form::label('queetion', 'Question') !!} - <p class="inline info">Like electronics, clothing</p>
              {!! Form::text('question', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('question') }}</small>
          </div> 
		  <!-- 
		  
		   
          <div class="form-group{{ $errors->has('answer') ? ' has-error' : '' }}">
              {!! Form::label('answer', 'Answer') !!} - <p class="inline info"></p>
              {!! Form::textarea('answer', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('answer') }}</small>
          </div> -->
          
          <div class="summernote-main form-group{{ $errors->has('description') ? ' has-error' : '' }}">
            {!! Form::label('answer', 'Answer') !!}
            {!! Form::textarea('answer', null, ['id' => 'summernote-main', 'class' => 'form-control']) !!}
            <small class="text-danger">{{ $errors->first('answer') }}</small>
          </div>
          

          
		  
		  
     <!--     <div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }} currency-symbol-block">
            {!! Form::label('icon', 'Category Icon / Symbol') !!}
            <p class="inline info"> - Please select catgeory symbol or category image</p>
              <div class="input-group">
                {!! Form::text('icon', null, ['class' => 'form-control category-icon-picker']) !!}
                <span class="input-group-addon simple-input"><i class="glyphicon glyphicon-th-large"></i></span>
              </div>
            <small class="text-danger">{{ $errors->first('icon') }}</small>
          </div>
		  -->
		  
       
          

          
          <div class="btn-group pull-right">
            <button type="reset" class="btn btn-info">Reset</button>
            <button type="submit" class="btn btn-success">Create</button>
          </div>
          <div class="clear-both"></div>
        </div>  
      </div>
    {!! Form::close() !!}
  </div>
@stop






			
		
	
