@extends('backend.layout.master')

@section('title','Order')

@section('contant')

  <!-- Page Wrapper -->
  <div class="content-main-block mrg-t-40">
   

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
       
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <div class="row mt-5">
          <div class="col-md-12">
            <div class="card mb-5">
              <div class="card-body" style="min-height: 380px;flex: 1 1 auto;padding: 1.25rem;">
               
                <h3>Order Information</h3>
                <div class="row mb-5">
                  <div class="col-md-3">
                    <b>Order ID</b>
                  </div>
                  <div class="col-md-3">
                    {{ sprintf('#%06d', $lists->id) }}
                  </div>
                  <div class="col-md-3">
                    <b>Invoice No.</b>
                  </div>
                  <div class="col-md-3">
                    {{ "{$setting->invoice_prefix}/{$lists->financial_year}/".sprintf('%04d', $lists->invoice_no) }}
                  </div>
                </div>
                <div class="row mb-5">
                  <div class="col-md-3">
                    <b>Created At</b>
                  </div>
                  <div class="col-md-3">
                    {{ $lists->created_at->format('F d, Y h:i A') }}
                  </div>
                  <div class="col-md-3">
                    <b>Last Updated At</b>
                  </div>
                  <div class="col-md-3">
                    {{ $lists->updated_at->format('F d, Y h:i A') }}
                  </div>
                </div>
                <div class="row mb-5">
                  <div class="col-md-3">
                    <b>Status</b>
                  </div>
                  <div class="col-md-3">
                    {{ $lists->status }}
                  </div>
                  <div class="col-md-3">
                    <b>Payment Status</b>
                  </div>
                  <div class="col-md-3">
                    {{ $lists->payment_status }}
                  </div>

                </div>
                <div class="row mb-5">
                  <div class="col-md-3">
                    <b>Coupon Code</b>
                  </div>
                  <div class="col-md-3">
                    {{ $lists->coupon_code ?? 'N/A' }}
                  </div>
                  <div class="col-md-3">
                    <b>Discount</b>
                  </div>
                  <div class="col-md-3">
                    {{ $lists->discount ?? 'N/A' }}
                  </div>
                  
                </div>
                <div class="row mb-5">
                  <div class="col-md-3">
                    <b>Total Amount</b>
                  </div>
                  <div class="col-md-3">
                    ₹{{ $lists->total_amount }}
                  </div>
                  <div class="col-md-3">
                    <b>Payment Mode</b>
                  </div>
                  <div class="col-md-3">
                    {{ $lists->payment_mode }}
                  </div>
                </div>
                <div class="row mb-5">
                  @if(!empty($lists->schedule_date))
                  <div class="col-md-3">
                    <b>Schedule Date & Time</b>
                  </div>
                  <div class="col-md-3">
                    {{ $lists->schedule_date ?? 'N/A' }}, {{ $lists->schedule_time ?? 'N/A' }}
                  </divs>
                  @endif
                  <div class="col-md-3">
                    <b>Notes</b>
                  </div>
                  <div class="col-md-3">
                    {{ $lists->notess ?? 'N/A' }}
                  </div>
                  <div class="col-md-3">
                    <b>Change Status</b>
                  </div>
                  <div class="col-md-3">
                    <span>
                      <div class="dropdown show">
                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          {{ $lists->status }}
                        </a>
                      
                        <div class="dropdown-menu status-dropdown" aria-labelledby="dropdownMenuLink">
                          
                          <a class="dropdown-item text-success" href="{{ url('admin-control/order/'.$lists->id.'?id='.$lists->id.'&status=In-Queue') }}">In-Queue</a>
                          <a class="dropdown-item" href="{{ url('admin-control/order/'.$lists->id.'?id='.$lists->id.'&status=Preparing') }}">Preparing</a>
                          <a class="dropdown-item text-warning" href="{{ url('admin-control/order/'.$lists->id.'?id='.$lists->id.'&status=Cancelled') }}">Cancelled</a>
                          <a class="dropdown-item text-warning" href="{{ url('admin-control/order/'.$lists->id.'?id='.$lists->id.'&status=Delivered') }}">Delivered</a>
                        </div>
                      </div>
                    </span>
                  </div>
                </div>
              
              </div>
            </div>

        </div>
        <div class="col-sm-12">
            <div class="card">
    			<div class="card-body" style="flex: 1 1 auto;padding: 1.25rem;">
    				<h3>Products</h3>
    				<div class="table-responsive">
    					<table class="table table-bordered table-striped table-sm">
    						<thead>
    							<tr>
    								<th>#</th>
    								<th>Item Description</th>
                    <th>Price</th>
    								<th>Weight</th>
    								<th>Qty</th>
    								<th>Jain Food</th>
    								<th>Subtotal</th>
    							</tr>
    						</thead>
    						<tbody>
    							@php
    							$total = 0;
    							@endphp
    							@foreach($lists->products as $i => $p)
    								@php
                 
    									$total += $p->price * $p->qty;
    
    								@endphp
    							<tr>
    								<td>{{ $i + 1 }}.</td>
    								<td></td>
    								<td>₹{{ $p->price }}</td>
                    <td></td>
    								<td>{{ $p->qty }}</td>
    								<td>{{ $p->jain_food }}</td>
    								<td>₹{{ $p->price * $p->qty }}</td>
    								
    							</tr>
    							@endforeach
    						</tbody>
    					</table>
    					<br>
    					<table class="table table-bordered table-striped table-sm">
    						<tbody>
    							<tr>
    								<td colspan="4">Total</td>
    								<td colspan="2">₹{{ $total }}</td>
    							</tr>
    							<tr>
    								<td colspan="4">Shipping</td>
    								<td colspan="2">{{ $lists->shipping_charge ? "₹".$lists->shipping_charge : "Free" }}</td>
    							</tr>
    							@php
    								if(!empty($lists->shipping_charge)) $total += $lists->shipping_charge;
    							@endphp
    							<tr>
    								<th class="bg-success" colspan="4">Grand Total</th>
    								<th class="bg-success" colspan="2">₹{{ $total }}</th>
    							</tr>
    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>
        </div>
        </div>
        
        
        
        <div class="row mt-5">
          <div class="col-sm-6">
            <div class="card mb-5">
              <div class="card-body" style="flex: 1 1 auto;padding: 1.25rem;">
                <h3>Billing Information</h3>
                <div class="row mb-5">
                  <div class="col-md-4">
                    <b>First Name</b>
                  </div>
                  <div class="col-md-8">
                    {{ $lists->billing_fname }}
                  </div>
                </div>
                <div class="row mb-5">
                  <div class="col-md-4">
                    <b>Last Name</b>
                  </div>
                  <div class="col-md-8">
                    {{ $lists->billing_lname }}
                  </div>
                </div>
                <div class="row mb-5">
                  <div class="col-md-4">
                    <b>Email ID</b>
                  </div>
                  <div class="col-md-8">
                    {{ $lists->billing_email }}
                  </div>
                </div>
                <div class="row mb-5">
                  <div class="col-md-4">
                    <b>Mobile No.</b>
                  </div>
                  <div class="col-md-8">
                    {{ $lists->billing_phone }}
                  </div>
                </div>
                <div class="row mb-5">
                  <div class="col-md-4">
                    <b>Address</b>
                  </div>
                  <div class="col-md-8">
                    {{ $lists->billing_addressid }}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="card">
              <div class="card-body" style="flex: 1 1 auto;padding: 1.25rem;">
                <h3>Shipping Information</h3>
                <div class="row mb-5">
                  <div class="col-md-4">
                    <b>First Name</b>
                  </div>
                  <div class="col-md-8">
                    {{ $lists->shipping_fname ?? $lists->billing_fname }}
                  </div>
                </div>
                <div class="row mb-5">
                  <div class="col-md-4">
                    <b>Last Name</b>
                  </div>
                  <div class="col-md-8">
                    {{ $lists->shipping_lname ?? $lists->billing_lname }}
                  </div>
                </div>
                <div class="row mb-5">
                  <div class="col-md-4">
                    <b>Email ID</b>
                  </div>
                  <div class="col-md-8">
                    {{ $lists->shipping_email ?? $lists->billing_email }}
                  </div>
                </div>
                <div class="row mb-5">
                  <div class="col-md-4">
                    <b>Mobile No.</b>
                  </div>
                  <div class="col-md-8">
                    {{ $lists->shipping_phone ?? $lists->billing_phone }}
                  </div>
                </div>
                <div class="row mb-5">
                  <div class="col-md-4">
                    <b>Address</b>
                  </div>
                  
                  <div class="col-md-8">
                        {{ $lists->billing_addressid }}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
         </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->

  </div>

  </div>
@stop