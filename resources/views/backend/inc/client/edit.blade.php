	@extends('backend.layout.master')

	@section('title','Client')

	@section('contant')


	<div class="content-wrapper mt-5">
        <h2 class="text-center py-5">Edit Client</h2>
		@if( $errors->any() )
		<div class="alert alert-danger" style="color: red;">
			@foreach($errors->all() as $error)
			<li>{!! $error !!}</li>
			@endforeach
		</div>
		@endif

		@if (\Session::has('success'))
		<div class="alert alert-success" style="color: green">
		{!! \Session::get('success') !!}</li>
	</div>
	@endif

	@if (\Session::has('danger'))
	<div class="alert alert-danger" style="color: red;">
	{!! \Session::get('danger') !!}</li>
</div>
@endif
<!--  -->
{{ Form::open(['method'=>'POST', 'files' => 'true', 'class' => 'user']) }}
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			{{ Form::label('name', 'Client Name')}}
			{{ Form::text('name', '', ['class' => 'form-control', 'id'=>'name', 'placeholder'=>'Client name','required'=>'required'])}}
		</div>
		<div class="form-group">
			{{Form::label('work', 'Client work')}}
			{{Form::text('work','', ['class'=>'form-control', 'id'=>'work', 'placeholder'=>'Client Work'])}}
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			{{Form::label('image', ' Choose image')}}
			<div class="">
				<!-- <input type="hidden" name="record[image]" value="" id="image"> -->
				{{ Form::file('image', ['class' => 'form-control']) }}
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			{{Form::label('excerpt', 'Enter Message')}}
			{{Form::textarea('excerpt', '', ['class' => 'form-control editor','id'=>'excerpt', 'placeholder'=>'Enter message'])}}
		</div>
	</div>
	
	<div class="text-right mt-4">
		{{ Form::submit('Update', ['class' => 'login-btn btn btn-orange']) }}
	</div>
	{{ Form::close() }}
</div>
</div>



</div>
@stop






