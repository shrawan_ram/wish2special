@php

$setting = App\Model\Setting::findOrFail(1);

@endphp
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="icon" type="image/png" sizes="20x20" href="{{url('imgs/'.$setting->fav_icon)}}">
	<title>App Name-@yield('title')</title>
	{{Html::style('admin/css/style.css')}}
	{{Html::style('admin/css/bootstrap.min.css')}}

</head>
<body style="background: #a1a1a1;">

	<div class="container">
		@yield('contant')
	</div>
</body>
</html>