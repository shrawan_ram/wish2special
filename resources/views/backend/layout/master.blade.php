@php
  $setting =  App\model\Setting::findOrFail(1);
@endphp
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Admin</title>
  <!-- favicon-icon -->
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">        
  <!-- flaticon css -->
  <link rel="stylesheet" type="text/css" href="{{url('admin/css/flaticon.css')}}"/>
  <link href="{{url('admin/css/datepicker.css')}}" rel="stylesheet" type="text/css"/> 
  <link rel="stylesheet" href="{{url('admin/css/admin.css')}}">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/summernote-bs4.css"/> <!-- summernote css -->
  <!-- Admin (main) Style Sheet -->
  <style>
      .discount_none{
          display:none;us
      }
      .mb-5, .my-5 {
          margin-bottom: 3rem !important;
      }
      .mt-5, .my-5 {
          margin-top: 3rem !important;
      }
      
  </style>
</head>
  <body class="hold-transition skin-blue">
<div class="wrapper">
  <!-- Main Header -->
  <header class="main-header">
    <!-- Logo -->
    <a href="{{url('admin-control/')}}" class="logo">
       <img src="{{ url('imgs/'.$setting->logo) }}" alt="{{ $setting->site_title }}">
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <a href="{{url('/')}}" class="visit-site-btn btn" target="_blank">Visit Site <i class="material-icons right">keyboard_arrow_right</i></a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown admin-nav">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="material-icons">account_circle</i></button>
            <ul class="dropdown-menu animated flipInX">
              <li><a href="{{url('admin/profile')}}">My Profile</a></li>
              <li>
                <a href="">
                    Logout
                </a>
                
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <i class="material-icons">account_circle</i>
        </div>
        <div class="pull-left info">
          <h4 class="user-name">admin</h4>
          <p>Admin</p>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <!-- Optionally, you can add icons to the links -->
        <li><a href="{{url('admin-control/')}}"><i class="material-icons">dashboard</i> <span>Dashboard</span></a></li>
        <li><a href="{{url('admin-control/about/edit/1')}}"><i class="material-icons">dashboard</i> <span>About Us</span></a></li> 
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">category</i> <span>City</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('admin-control/city/add') }}"><i class="material-icons">label_outline</i> Add City</a></li>
            <li><a href="{{ url('admin-control/city/') }}"><i class="material-icons">label_outline</i> All City</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">category</i> <span>Vendor</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('admin-control/vendor/add') }}"><i class="material-icons">label_outline</i> Add Vendor</a></li>
            <li><a href="{{ url('admin-control/vendor/') }}"><i class="material-icons">label_outline</i> All Vendor</a></li>
          </ul>
        </li>     
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">category</i> <span>Category</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('admin-control/category/add') }}"><i class="material-icons">label_outline</i> Add Category</a></li>
            <li><a href="{{ url('admin-control/category/') }}"><i class="material-icons">label_outline</i> All Category</a></li>
          </ul>
        </li> 
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">store</i> <span>Blog</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin-control/blog/add')}}"><i class="material-icons">label_outline</i> Add Blog</a></li>
            <li><a href="{{url('admin-control/blog/')}}"><i class="material-icons">label_outline</i> All Blog</a></li>
          </ul>
        </li>
        <!-- <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">store</i> <span>City</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin/city/create')}}"><i class="material-icons">label_outline</i> Add city</a></li>
            <li><a href=""><i class="material-icons">label_outline</i> All city</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">store</i> <span>State</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin/state/create')}}"><i class="material-icons">label_outline</i> Add state</a></li>
            <li><a href=""><i class="material-icons">label_outline</i> All state</a></li>
          </ul>
        </li> -->
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">image</i> <span>Product</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin-control/product/add')}}"><i class="material-icons">label_outline</i> Add Product</a></li>
            <li><a href="{{url('admin-control/product/')}}"><i class="material-icons">label_outline</i>All Product</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">image</i> <span>Product Detail</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin-control/product-detail/add')}}"><i class="material-icons">label_outline</i> Add Product</a></li>
            <li><a href="{{url('admin-control/product-detail/')}}"><i class="material-icons">label_outline</i>All Product</a></li>
          </ul>
        </li>
        <!--<li class="treeview">-->
        <!--  <a href="#" class="">-->
        <!--    <i class="material-icons">image</i> <span>Slider</span>-->
        <!--    <span class="pull-right-container">-->
        <!--      <i class="fa fa-angle-right pull-right"></i>-->
        <!--    </span>-->
        <!--  </a>-->
        <!--  <ul class="treeview-menu">-->
        <!--    <li><a href="{{url('admin-control/slider/add')}}"><i class="material-icons">label_outline</i> Add Slider</a></li>-->
        <!--    <li><a href="{{url('admin-control/slider/')}}"><i class="material-icons">label_outline</i>All Slider</a></li>-->
        <!--  </ul>-->
        <!--</li>-->
       
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">card_giftcard</i> <span>Faqs</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin-control/faqs/add')}}"><i class="material-icons">label_outline</i>Add Faq </a></li>
            <li><a href="{{url('admin-control/faqs/')}}"><i class="material-icons">label_outline</i>All Faq </a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">card_giftcard</i> <span>Unit</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin-control/unit/add')}}"><i class="material-icons">label_outline</i>Add Unit </a></li>
            <li><a href="{{url('admin-control/unit/')}}"><i class="material-icons">label_outline</i>All Unit </a></li>
          </ul>
        </li><!--<li class="treeview">-->
        <!--  <a href="#" class="">-->
        <!--    <i class="material-icons">card_giftcard</i> <span>Offer</span>-->
        <!--    <span class="pull-right-container">-->
        <!--      <i class="fa fa-angle-right pull-right"></i>-->
        <!--    </span>-->
        <!--  </a>-->
        <!--  <ul class="treeview-menu">-->
        <!--    <li><a href="{{url('admin-control/offer/add')}}"><i class="material-icons">label_outline</i>Add Offer </a></li>-->
        <!--    <li><a href="{{url('admin-control/offer/')}}"><i class="material-icons">label_outline</i>All Offer </a></li>-->
        <!--  </ul>-->
        <!--</li> -->
        <li><a href="{{url('admin-control/order')}}"><i class="material-icons">dashboard</i> <span>Order</span></a></li>
        <li><a href="{{url('admin-control/user')}}"><i class="material-icons">dashboard</i> <span>User</span></a></li>
        <li><a href="{{url('admin-control/send-mail')}}"><i class="material-icons">dashboard</i> <span>Send Mail</span></a></li>
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">category</i> <span>Pages</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('admin-control/privacy/edit/1') }}"><i class="material-icons">label_outline</i> Privacy</a></li>
            <li><a href="{{ url('admin-control/term/edit/2') }}"><i class="material-icons">label_outline</i> Term</a></li>
            <li><a href="{{ url('admin-control/security/edit/3') }}"><i class="material-icons">label_outline</i> Security</a></li>
          </ul>
        </li>
     
     <!-- <li><a href="{{url('admin/inquiry')}}"><i class="material-icons">dashboard</i> <span>Inquiry</span></a></li>  
     <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">people</i> <span>User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin/user/create')}}"><i class="material-icons">label_outline</i>Add User</a></li>
            <li><a href=""><i class="material-icons">label_outline</i> All User</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">pages</i> <span>Pages</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin/pages/create')}}"><i class="material-icons">label_outline</i> Add Pages</a></li>
            <li><a href=""><i class="material-icons">label_outline</i>All Pages</a></li>
          </ul>
        </li>  -->
    
    
    <!--
        <li><a class="('slider') }}" href="{{url('admin/slider')}}">
    <i class="material-icons">image</i> Home Banner</a></li>
    -->
    
    
        <li class="treeview">
          <a href="#" class="">
            <i class="material-icons">build</i> <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('admin-control/setting/edit/1')}}"><i class="material-icons">label_outline</i> General Settings</a></li>
            <li><a href=""><i class="material-icons">label_outline</i>Social</a></li>
            {{-- <li><a href=""><i class="material-icons">label_outline</i>Footer</a></li> --}}
          </ul>
        </li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <!-- Main content -->
    <section class="content container-fluid">
      @if (Session::has('added'))
        <div id="sessionModal" class="sessionmodal rgba-green-strong z-depth-2">
          <i class="fa fa-check-circle"></i> <p>{{session('added')}}</p>
        </div>
      @elseif (Session::has('updated'))
        <div id="sessionModal" class="sessionmodal rgba-cyan-strong z-depth-2">
          <i class="fa fa-check-circle"></i> <p>{{session('updated')}}</p>
        </div>
      @elseif (Session::has('deleted'))
        <div id="sessionModal" class="sessionmodal rgba-red-strong z-depth-2">
          <i class="fa fa-window-close"></i> <p>{{session('deleted')}}</p>
        </div>
      @endif
      @yield('contant')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
</div>
<!-- ./wrapper -->
<!-- Admin Js -->
<script src="{{url('admin/js/jquery-3.3.1.min.js')}}" type="text/javascript"></script>
<script src="{{url('admin/js/admin.js')}}" type="text/javascript"></script>
<script src="{{url('admin/js/summernote-bs4.min.js')}}"></script>
<!-- summernote js -->
<script src="{{url('admin/js/datatables.min.js')}}" type="text/javascript"></script>
    <!-- Datepicker Library -->
<script src="{{url('admin/js/datepicker.js')}}" type="text/javascript"></script>
<script src="{{url('admin/js/utils.js')}}" type="text/javascript"></script>

<!-- <script>

  function statuschange(self) {
    let status = self.value,
    
        url = $(self).data('url');

    window.location = url+"/?field=status&status="+status;
}
</script> -->
<script>
$(document).ready(function(){
    $("select").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".discount_none").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else{
                $(".discount_none").hide();
            }
        });
    }).change();
});
</script>
<script>
   var salePrice = 0;
    var rp = 0;

    $('#discount').on('keyup', function() {
        const ds = $(this).val();
        // if (rp == 0 && rp == '') {
        //     $("#regular_price").focus();
        //     $(this).val(0);
        // }
        if (ds == '') $(this).val(0).select();
        rp = $("#regular_price").val();
        salePrice = rp - (rp * ds / 100);
        $("#sale_price").val(salePrice);
    });
    $('#sale_price').on('keyup', function() {
        const sp = $(this).val();
        // if (rp == 0 && rp == '') $("#regular_price").focus();
        if (sp == '') $(this).val(0).select();
        rp = $("#regular_price").val();
        salePrice = (rp - sp) * 100 / rp;
        $("#discount").val(salePrice);
    });
</script>
<script>
   var salePrice = 0;
    var rp = 0;

    $('#flat').on('keyup', function() {
        const ds = $(this).val();
        // if (rp == 0 && rp == '') {
        //     $("#regular_price").focus();
        //     $(this).val(0);
        // }
        if (ds == '') $(this).val(0).select();
        rp = $("#regular_price").val();
        salePrice = rp - ds;
        $("#sale_price").val(salePrice);
    });
    $('#sale_price').on('keyup', function() {
        const sp = $(this).val();
        // if (rp == 0 && rp == '') $("#regular_price").focus();
        if (sp == '') $(this).val(0).select();
        rp = $("#regular_price").val();
        salePrice = rp - ds;
        $("#flat").val(salePrice);
    });
</script>
<script>
  $(function () {
    $('#flash-overlay-modal').modal();
    $('.alert').addClass('active');
    $('.alert').addClass('z-depth-1');
    setTimeout(function(){
      $('.alert:not(.alert-important)').removeClass('active');
    }, 6000);  
    $( '.date-picker' ).datepicker({
        format : "yyyy-mm-dd",
        startDate: '+1d',
       autoclose: true,
      });
    // DataTables
    $('#movies_table').DataTable({
      responsive: true,
      "sDom": "<'row'><'row'<'col-md-4'l><'col-md-4'B><'col-md-4'f>r>t<'row'<'col-sm-12'p>>",
      "language": {
        "paginate": {
          "previous": '<i class="material-icons paginate-btns">keyboard_arrow_left</i>',
          "next": '<i class="material-icons paginate-btns">keyboard_arrow_right</i>'
          }
      },
      buttons: [
        {
          extend: 'print',
          exportOptions: {
              columns: ':visible'
          }
        },
        'csvHtml5',
        'excelHtml5',
        'colvis',
      ]
    });

    $('#full_detail_table').DataTable({
      "sDom": "<'row'><'row'<'col-md-4'l><'col-md-4'B><'col-md-4'f>r>t<'row'<'col-sm-12'p>>",
      "language": {
      "paginate": {
        "previous": '<i class="material-icons paginate-btns">keyboard_arrow_left</i>',
        "next": '<i class="material-icons paginate-btns">keyboard_arrow_right</i>'
        }
      },
      buttons: [
        {
          extend: 'print',
          exportOptions: {
              columns: ':visible'
          }
        },
        'csvHtml5',
        'excelHtml5',
        'colvis',
      ]
    });

    $('#summernote-main').summernote({
      height: 100,
    });

    $(".js-select2").select2({
        placeholder: "Pick states",
        theme: "material"
    });
    
    $(".select2-selection__arrow")
        .addClass("material-icons")
        .html("arrow_drop_down");
  });
</script>
<script>
  $(document).on('click','.btn-remove',function () {
        let form=$(this).closest('form');
        // alert($('.check:checked').length);
        if (form.find('.check:checked').length) {

           if (confirm('Are you sure you want to delete this item?')) {
            form.trigger('submit');
           }
       }else{
        alert('please select atleast one record to delete');
       }
    });
</script>

<script>
  $(document).ready(function() {
    var i=1; 
    $('#add').click(function() {
        i++;
        $('#dynamic_field').append('<div id="row'+i+'"><div class="col-lg-2 form-group"><label>Volume</label><input type="text" name="volume[]" class="form-control"></div><div class="col-lg-3 form-group"><label>Unit</label><select class="form-control select2 " name="unit[]"><option>Select Unit</option><option>gm</option><option>kg</option></select></div><div class="col-lg-2 form-group"><label>Price</label><input type="text" name="regular_price[]" class="form-control" id="regular_price"></div><div class="col-lg-2 form-group"><label>Discount</label><input type="text" name="discount[]" class="form-control" id="discount"></div><div class="col-lg-2 form-group"><label>Sale Price</label><input type="text" name="sale_price[]" class="form-control" id="sale_price"></div><div class="col-lg-1 form-group"><button type="button" class="btn_remove btn btn-danger" name="remove" id="'+ i +'">-</button></div></div>')

    });
    $(document).on('click', '.btn_remove', function() {
        var button_id = $(this).attr("id");
        $('#row' + button_id + '').remove();
    });
});
</script>


@yield('custom-script')
</body>
</html>

