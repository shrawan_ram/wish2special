@extends('frontend.layout.master')

@section('title', 'Change Password')

@section('contant')

		<main>
     <div id="wrapper">
  <!-- banner 
    ============================================= -->
  
  <div class="category_background" style="background-attachment: fixed; background-size: cover; color:#fff;">
        <div class="container">

        <div class="banner-content">
            <div class="container" >
                <h1 class="text-center text-white">Change Password</h1>
                <ol class="breadcrumb text-center">
                  <li><a href="{{url('/')}}">Home</a></li>
                  <li><a href="{{url('/account')}}">Profile</a></li>
                  <li>Change Password</li>
                </ol>
            </div>
          </div>
      </div>
    </div>
        <!-- End Breadcrumb Part -->
        <section class="home-icon login-register bg-skeen">
            <div class="icon-default icon-skeen">
                <img src="{{url('imgs/scroll-arrow.png')}}" alt="">
            </div>
            <div class="container">
                <div class="row">
                    @include('frontend.profile.sidebar')
                    <div class="col-sm-8">
						<div class="panel mb-5">
							<div class="panel-body" style="min-height: 380px">
								<h3 class="mb-15">Change Password</h3>
								{{ Form::open() }}
									@if (\Session::has('success'))
						                <div class="alert alert-success toast-msg" style="color: green">
						                    {!! \Session::get('success') !!}</li>
						                </div>
						            @endif
						            @if (\Session::has('danger'))
						                <div class="alert alert-danger toast-msg" style="color: red;">
						                    {!! \Session::get('danger') !!}</li>
						                </div>
						            @endif
									@if($message = Session::get('error'))
									   <div class="alert alert-danger alert-block">
									     <button type="button" class="close" data-dismiss="alert">x</button>
									     {{$message}}
									   </div>
								  	@endif
								  @if(count($errors->all()))
								    <div class="alert alert-danger">
								      <ul>
								        @foreach($errors->all() as $error)
								          <li>{{$error}}</li>
								        @endforeach
								      </ul>
								    </div>
								@endif
								<div class="row">
									<div class="col-xs-12">
										{{ Form::label('current_password') }}
										{{ Form::password('current_password', ['class'=>'form-control', 'placeholder' => 'Current Password', 'required' => 'required', 'autocomplete' => 'new-password']) }}
									</div>
									<div class="col-xs-12">
										{{ Form::label('new_password') }}
										{{ Form::password('new_password', ['class'=>'form-control', 'placeholder' => 'New Password', 'required' => 'required', 'autocomplete' => 'new-password']) }}
									</div>
									<div class="col-xs-12">
										{{ Form::label('confirm_password') }}
										{{ Form::password('confirm_password', ['class'=>'form-control', 'placeholder' => 'Confirm Password', 'required' => 'required', 'autocomplete' => 'new-password']) }}
									</div>
									<div class="col-xs-12">
										{{ Form::submit('Change Password', ['class' => 'btn-main btn add_cart_btn']) }}
									</div>
								</div>
								{{ Form::close() }}
							</div>
						</div>

                    </div>
                </div>
            </div>
        </section>

		<!-- Rate Modal -->
		<div id="reviewModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Rate &amp; Review</h4>
		      </div>
		      <div class="modal-body">
		        {{ Form::open(['url' => route('save_rating'), 'id' => 'productRatingForm']) }}
				<input type="hidden" name="rate[menu_id]" id="review_pid" value="">
				<div class="rating-star">
					@for($i = 1; $i <= 5; $i++)
					<label for="rating_{{ $i }}"><i class="fas fa-star"></i> </label>
					<input type="radio" name="rate[rating]" value="{{ $i }}" id="rating_{{ $i }}" @if($i == 1) checked @endif>
					@endfor
				</div>
				<div class="form-group">
					{{ Form::label('review') }}
					{{ Form::textarea('rate[review]', '', ['id' => 'review', 'placeholder' => 'Write review here', 'required']) }}
				</div>
				<button type="submit" class="btn btn-default">Submit</button>
				{{ Form::close() }}
		      </div>
		    </div>
		  </div>
		</div>
    </div>
</main>
@stop
