@extends('frontend.layout.master')

@section('title', 'Order Details')

@section('contant')

<main>
     <div id="wrapper">
  <!-- banner 
    ============================================= -->
  
  <div class="category_background" style="background-attachment: fixed; background-size: cover; color:#fff;">
        <div class="container">

        <div class="banner-content">
            <div class="container" >
                <h1 class="text-center text-white">Order Information</h1>
                <ol class="breadcrumb text-center">
                  <li><a href="{{url('/')}}">Home</a></li>
                  <li><a href="{{url('/account')}}">Profile</a></li>
                  <li>Order Info</li>
                </ol>
            </div>
          </div>
      </div>
    </div>
        <section class="home-icon login-register bg-skeen">
            <div class="icon-default icon-skeen">
                <img src="{{url('imgs/scroll-arrow.png')}}" alt="">
            </div>
            <div class="container">
                <div class="row">
                    @include('frontend.profile.sidebar')
                    <div class="col-sm-8">
						<div class="panel mb-5">
							<div class="panel-body" style="min-height: 427px">
								<a href="{{ route('my_orders') }}" class="pull-right" title="Edit" data-toggle="tooltip"><i class="fa fa-arrow-left icomoon"></i> Back</a>
								<h3>Order Information</h3>
								<div class="row mb-5">
									<div class="col-xs-6 col-md-3">
										<strong>Order ID</strong>
									</div>
									<div class="col-xs-6 col-md-3">
										{{ sprintf('#%06d', $order->id) }}
									</div>
									<div class="col-xs-6 col-md-3">
										<strong>Invoice No.</strong>
									</div>
									<div class="col-xs-6 col-md-3">
										{{ "{$setting->invoice_prefix}/{$order->financial_year}/".sprintf('%04d', $order->invoice_no) }}
									</div>
								</div>
								<div class="row mb-5">
									<div class="col-xs-6 col-md-3">
										<strong>Date & Time</strong>
									</div>
									<div class="col-xs-6 col-md-3">
										{{ $order->created_at->format('M d, Y h:i A') }}
									</div>
                                    <div class="col-xs-6 col-md-3">
                                        <strong>Schedule Date & Time</strong>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        {{ $order->schedule_date }} {{ $order->schedule_time ?? 'N/A' }}
                                    </div>
								</div>
								<div class="row mb-5">
									<div class="col-xs-6 col-md-3">
										<strong>Status</strong>
									</div>
									<div class="col-xs-6 col-md-3">
										{{ $order->status }}
									</div>
									<div class="col-xs-6 col-md-3">
										<strong>Payment Status</strong>
									</div>
									<div class="col-xs-6 col-md-3">
										{{ $order->payment_status }}
									</div>
								</div>
								<div class="row mb-5">
									<div class="col-xs-6 col-md-3">
										<strong>Total Amount</strong>
									</div>
									<div class="col-xs-6 col-md-3">
										₹{{ $order->total_amount }}
									</div>
									<div class="col-xs-6 col-md-3">
										<strong>Payment Mode</strong>
									</div>
									<div class="col-xs-6 col-md-3">
										{{ $order->payment_mode }}
									</div>
								</div>
								<div class="row mb-5">
                                    <div class="col-xs-6 col-md-3">
                                        <strong>Notes</strong>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        {{ $order->notes ?? 'N/A' }}
                                    </div>
                                </div>
							</div>
						</div>

                    </div>
                </div>
				<div class="panel">
					<div class="panel-body mb-5">
						<div class="table-responsive">
							<table class="table table-striped table-responsive table-cart">
						        <thead>
						            <tr>
						                <th>PRODUCT</th>
						                <th class="text-center">PRICE</th>
						                <th class="text-center">WEIGHT</th>
						                <th class="text-center">QUANTITY</th>
						                <th class="text-center">JAIN FOOD</th>
						                <th class="text-center">TOTAL</th>
						                @if($order->status == 'Delivered')
						                <th class="text-center">RATE</th>
						                @endif
						            </tr>
						        </thead>
						        <tbody>
						            @php
									$total = 0;
									@endphp
									@foreach($order->products as $i => $p)
										@php

											$total += $p->price * $p->qty;

											$rating = \App\model\MenuReview::where('u_id', auth()->user()->id)->where('menu_id', $p->menu->id)->first();
										@endphp
						            <tr>
						                <td style="padding-left: 20px; ">
						                    <div class="product-cart">
						                        <img src="{{ url('imgs/product/'.$p->menu->product->image) }}" alt="" style="max-height: 100px; border-radius: 8px;"><span>{{ $p->menu->product->title }}</span>
						                    </div>
						                    <!-- <div class="product-cart-title">
						                        
						                    </div> -->
						                </td>
						                <td class="text-center">
						                    <strong>₹ {{ $p->price }}</strong>
						                </td>
						                <td class="text-center">
						                    {{ $p->menu->weight }}
						                </td>
						                <td class="text-center">
						                    {{ $p->qty }}
						                </td>
						                <td class="text-center">
					                        {{ $p->jain_food }}
					                    </td>
						                <td class="text-center">
						                    ₹ {{ $p->price * $p->qty }}
						                </td>
						                @if($order->status == 'Delivered')
										<td class="text-center">
											@if(!empty($rating->id))
												@for($star = 1; $star <= $rating->rating; $star++)
													<i class="fas fa-star yellow_bg"></i>
												@endfor
												@for($star = $rating->rating + 1; $star <= 5; $star++)
													<i class="fas fa-star"></i>
												@endfor
											@else
												<button class="btn btn-xs btn-primary rate_menu_btn" data-id="{{ $p->menu->id }}">Rate Menu</button>
											@endif
										</td>
										@endif
						            </tr>
						            @endforeach
						        </tbody>
						    </table>    							
						</div>
					</div>
				</div>
				<div class="panel">
					<div class="panel-body">
						<div class="cart-total wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" style="float: left; border-radius: 4px; margin: 0; padding: 0px; max-width: 100%;background: #f5f5f5;">
						    <div class="product-cart-total">
						        <small style="font-size: 16px;">Total</small>
						        <span>₹{{ $total }}</span>
						    </div>
						    <div class="product-cart-total">
						        <small style="font-size: 16px;">Shipping</small>
						        <span>{{ $order->shipping_charge ? "₹".$order->shipping_charge : "Free" }}</span>
						    </div>
						    @if(!empty($order->discount))
						    <div class="product-cart-total">
						        <small style="font-size: 16px;">Discount</small>
						        <span>{{ $order->discount ? "₹".$order->discount : "Free" }}</span>
						    </div>
						    @endif	
								@php
									if(!empty($order->shipping_charge)) $total += $order->shipping_charge;
									if(!empty($order->discount)) $total -= $order->discount;
								@endphp
						    <div class="grand-total" style="margin: 19px 0 19px;">
						        <h5>Grand Total <span>₹{{ $total }}</span></h5>
						    </div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="panel mb-5">
							<div class="panel-body">
								<h3>Billing Information</h3>
								<div class="row mb-5">
									<div class="col-xs-4">
										<strong>First Name</strong>
									</div>
									<div class="col-xs-8">
										{{ $order->billing_fname }}
									</div>
								</div>
								<div class="row mb-5">
									<div class="col-xs-4">
										<strong>Last Name</strong>
									</div>
									<div class="col-xs-8">
										{{ $order->billing_lname }}
									</div>
								</div>
								@if(!empty($order->billing_company_name))
								<div class="row mb-5">
									<div class="col-xs-4">
										<strong>Company Name</strong>
									</div>
									<div class="col-xs-8">
										{{ $order->billing_company_name }}
									</div>
								</div>
								@endif
								<div class="row mb-5">
									<div class="col-xs-4">
										<strong>Email ID</strong>
									</div>
									<div class="col-xs-8">
										{{ $order->billing_email }}
									</div>
								</div>
								<div class="row mb-5">
									<div class="col-xs-4">
										<strong>Mobile No.</strong>
									</div>
									<div class="col-xs-8">
										{{ $order->billing_phone }}
									</div>
								</div>
								<div class="row mb-5">
									<div class="col-xs-4">
										<strong>Address</strong>
									</div>
									<div class="col-xs-8">
										{{ $order->billing_addressid }}
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="panel">
							<div class="panel-body">
								<h3>Shipping Information</h3>
								<div class="row mb-5">
									<div class="col-xs-4">
										<strong>First Name</strong>
									</div>
									<div class="col-xs-8">
										{{ $order->shipping_fname ?? $order->billing_fname }}
									</div>
								</div>
								<div class="row mb-5">
									<div class="col-xs-4">
										<strong>Last Name</strong>
									</div>
									<div class="col-xs-8">
										{{ $order->shipping_lname ?? $order->billing_lname }}
									</div>
								</div>
								@if(!empty($order->shipping_company_name ?? $order->billing_company_name))
								<div class="row mb-5">
									<div class="col-xs-4">
										<strong>Company Name</strong>
									</div>
									<div class="col-xs-8">
										{{ $order->shipping_company_name ?? $order->billing_company_name }}
									</div>
								</div>
								@endif
								<div class="row mb-5">
									<div class="col-xs-4">
										<strong>Email ID</strong>
									</div>
									<div class="col-xs-8">
										{{ $order->shipping_email ?? $order->billing_email }}
									</div>
								</div>
								<div class="row mb-5">
									<div class="col-xs-4">
										<strong>Mobile No.</strong>
									</div>
									<div class="col-xs-8">
										{{ $order->shipping_phone ?? $order->billing_phone }}
									</div>
								</div>
								<div class="row mb-5">
									<div class="col-xs-4">
										<strong>Address</strong>
									</div>
									<div class="col-xs-8">
									    {{ $order->billing_addressid }}	
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </section>

		<!-- Rate Modal -->
		<div id="reviewModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Rate &amp; Review</h4>
		      </div>
		      <div class="modal-body">
		        {{ Form::open(['url' => route('save_rating'), 'id' => 'productRatingForm']) }}
				<input type="hidden" name="rate[menu_id]" id="review_pid" value="">
				<div class="rating-star">
					@for($i = 1; $i <= 5; $i++)
					<label for="rating_{{ $i }}"><i class="fa fa-star"></i> </label>
					<input type="radio" name="rate[rating]" value="{{ $i }}" id="rating_{{ $i }}" @if($i == 1) checked @endif>
					@endfor
				</div>
				<div class="form-group">
					{{ Form::label('review') }}
					{{ Form::textarea('rate[review]', '', ['id' => 'review', 'placeholder' => 'Write review here', 'required']) }}
				</div>
				<button type="submit" class="btn btn-default">Submit</button>
				{{ Form::close() }}
		      </div>
		    </div>
		  </div>
		</div>
    </div>
</main>
@stop
