<div class="col-sm-4">
    <div class="panel panel-default">
        <div class="panel-heading text-center">
            <h4 class="mt-15">{{ ucwords(auth()->user()->name) }}</h4>
            <p>({{ auth()->user()->login }})</p>
        </div>
        <ul class="list-group">
            <li class="list-group-item">
                <a href="{{ route('profile') }}"><i class="fa fa-user" style="background: none;"></i> Profile</a>
            </li>
            <li class="list-group-item">
                <a href="{{ route('edit_profile') }}"><i class="fa fa-pencil"></i> Edit Profile</a>
            </li>
            <li class="list-group-item">
                <a href="{{ route('my_orders') }}"><i class="fa fa-shopping-cart"></i> My Orders</a>
            </li>
            <!-- <li class="list-group-item">
                <a href="{{ route('wishlist') }}"><i class="fa fa-heart"></i> My Wishlist</a>
            </li> -->
            <li class="list-group-item">
                <a href="{{ route('address') }}"><i class="fa fa-map-marker" ></i> My Address</a>
            </li>
            <li class="list-group-item">
                <a href="{{ route('change_password') }}"><i class="fa fa-lock"></i> Change Password</a>
            </li>
            <li class="list-group-item">
                <a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> Logout</a>
            </li>
        </ul>
    </div>
</div>
