@extends('frontend.layout.master')

@section('title', 'Edit Profile')

@section('contant')

		<main>
      <div id="wrapper">
  <!-- banner 
    ============================================= -->
  
  <div class="category_background" style="background-attachment: fixed; background-size: cover; color:#fff;">
        <div class="container">

        <div class="banner-content">
            <div class="container" >
                <h1 class="text-center text-white">Edit Profile</h1>
                <ol class="breadcrumb text-center">
                  <li><a href="{{url('/')}}">Home</a></li>
                  <li><a href="{{url('/account')}}">Profile</a></li>
                  <li>Edit Profile</li>
                </ol>
            </div>
          </div>
      </div>
    </div>
        <!-- End Breadcrumb Part -->
        <section class="home-icon login-register bg-skeen">
            <div class="icon-default icon-skeen">
                <img src="{{url('imgs/scroll-arrow.png')}}" alt="">
            </div>
            <div class="container">
                <div class="row">
                    @include('frontend.profile.sidebar')
                    <div class="col-sm-8">
						<div class="panel">
							<div class="panel-body">
								<a href="{{ route('profile') }}" class="pull-right" title="Edit" data-toggle="tooltip"><i class="fa fa-user" style="background: none"></i> View Profile</a>
								<h3>Edit Profile</h3>
								{{ Form::open() }}
								@if (\Session::has('success'))
					                <div class="alert alert-success toast-msg" style="color: green">
					                    {!! \Session::get('success') !!}</li>
					                </div>
					            @endif

					            @if (\Session::has('danger'))
					                <div class="alert alert-danger toast-msg" style="color: red;">
					                    {!! \Session::get('danger') !!}</li>
					                </div>
					            @endif

								@if($message = Session::get('error'))
								   <div class="alert alert-danger alert-block">
								     <button type="button" class="close" data-dismiss="alert">x</button>
								     {{$message}}
								   </div>
								  @endif
								  @if(count($errors->all()))
								    <div class="alert alert-danger">
								      <ul>
								        @foreach($errors->all() as $error)
								          <li>{{$error}}</li>
								        @endforeach
								      </ul>
								    </div>
								@endif
								<div class="row">
									<div class="col-xs-6">
										{{ Form::label('fname', 'First Name *') }}
										{{ Form::text('record[fname]', $user->fname, ['class' => 'form-control', 'placeholder' => 'First Name', 'required', 'id' => 'fname']) }}
									</div>
									<div class="col-xs-6">
										{{ Form::label('lname', 'Last Name') }}
										{{ Form::text('record[lname]', $user->lname, ['class' => 'form-control', 'placeholder' => 'Last Name', 'id' => 'lname']) }}
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										{{ Form::label('name', 'Display Name *') }}
										{{ Form::text('record[name]', $user->name, ['class' => 'form-control', 'placeholder' => 'Name', 'required', 'id' => 'name']) }}
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										{{ Form::label('email', 'Email ID *') }}
										{{ Form::email('record[email]', $user->email, ['class' => 'form-control', 'placeholder' => 'Email ID', 'required', 'id' => 'email']) }}
									</div>
								</div>
								<div class="form-group">
									{{ Form::submit('Update', ['class' => 'btn-main btn btn add_cart_btn']) }}
								</div>
								{{ Form::close() }}
							</div>
						</div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@stop
