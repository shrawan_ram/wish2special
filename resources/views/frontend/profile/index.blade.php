@extends('frontend.layout.master')

@section('title', 'Profile')

@section('contant')

		<main>
     <div id="wrapper">
  <!-- banner 
    ============================================= -->
  
  <div class="category_background" style="background-attachment: fixed; background-size: cover; color:#fff;">
        <div class="container">

        <div class="banner-content">
            <div class="container" >
                <h1 class="text-center text-white">Profile</h1>
                <ol class="breadcrumb text-center">
                  <li><a href="{{url('/')}}">Home</a></li>
                  <li>Profile</li>
                </ol>
            </div>
          </div>
      </div>
    </div>
        <!-- End Breadcrumb Part -->
        <section class="home-icon login-register bg-skeen">
            <div class="icon-default icon-skeen">
                <img src="{{url('imgs/scroll-arrow.png')}}" alt="">
            </div>
            <div class="container">
                <div class="row">
                    @include('frontend.profile.sidebar')
                    <div class="col-sm-8">
						<div class="panel">
							<div class="panel-body">
								<a href="{{ route('edit_profile') }}" class="pull-right" title="Edit" data-toggle="tooltip"><i class="fa fa-pencil"></i> Edit</a>
								<h3>Personal Information</h3>
								<div class="row mb-5">
									<div class="col-xs-4">
										<strong>First Name</strong>
									</div>
									<div class="col-xs-8">
										{{ auth()->user()->fname }}
									</div>
								</div>
								<div class="row mb-5">
									<div class="col-xs-4">
										<strong>Last Name</strong>
									</div>
									<div class="col-xs-8">
										{{ auth()->user()->lname }}
									</div>
								</div>
								<div class="row mb-5">
									<div class="col-xs-4">
										<strong>Display Name</strong>
									</div>
									<div class="col-xs-8">
										{{ auth()->user()->name }}
									</div>
								</div>
								<div class="row mb-5">
									<div class="col-xs-4">
										<strong>Email ID</strong>
									</div>
									<div class="col-xs-8">
										{{ auth()->user()->email }}
									</div>
								</div>
								<div class="row mb-5">
									<div class="col-xs-4">
										<strong>Mobile No.</strong>
									</div>
									<div class="col-xs-8">
										{{ auth()->user()->mobile }}
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@stop
