@extends('frontend.layout.master')

@section('title', 'My Address')

@section('contant')

<main>
   <div id="wrapper">
  <!-- banner 
    ============================================= -->
  
  <div class="category_background" style="background-attachment: fixed; background-size: cover; color:#fff;">
        <div class="container">

        <div class="banner-content">
            <div class="container" >
                <h1 class="text-center text-white">My Address</h1>
                <ol class="breadcrumb text-center">
                  <li><a href="{{url('/')}}">Home</a></li>
                  <li><a href="{{url('/account')}}">Profile</a></li>
                  <li>My Address</li>
                </ol>
            </div>
          </div>
      </div>
    </div>
        <!-- End Breadcrumb Part -->
        <section class="home-icon login-register bg-skeen">
            <div class="icon-default icon-skeen">
                <img src="{{ url('imgs/scroll-arrow.png') }}" alt="">
            </div>
            <div class="container">
                <div class="row">
                    @include('frontend.profile.sidebar')
                    <div class="col-sm-8">
						<div class="panel">
							<div class="panel-body">
								<a href="{{ route('add_address') }}" class="pull-right" title="Edit" data-toggle="tooltip"><i class="fa fa-map-marker" style="background: none"></i> Add Address</a>
								<h3>My Address</h3>
								@foreach($address as $list)
								<div class="panel" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
								    <div class="panel-body">
                                        <div class="checkout-log" style="margin: 0;">
                                            <div class="row">
                                                <div class="col-xs-1 col-md-1 checkout-log-add">
                                                    <div style="padding: 0 15px;">
                                                        <i class="fa fa-map-marker"></i>
                                                    </div>
                                                </div>
                                                <div class="col-xs-10 col-md-8 checkout-log-add" style="padding: 10px 10px;">
                                                    <div>{{ $list->address }}, {{ $list->city }}, {{ $list->state }}, {{ $list->pincode }}, {{ $list->country }}</div>
                                                </div>
                                                <div class="col-xs-12 col-md-6 checkout-log-add text-center">
                                                    <a href="{{ url('account/edit_address/'.$list->id) }}" class=""><div class="form-control alert-success btn add_cart_btn"><i class="fa fa-pencil"></i> Edit Address</div></a>
                                                </div>
                                                <div class="col-xs-12 col-md-6 checkout-log-add text-center">
                                                    <a href="{{ url('account/delete_address/'.$list->id) }}"><div class="form-control alert-danger btn add_cart_btn"><i class="fa fa-trash"></i> Delete Address</div></a>
                                                </div>
                                            </div>
                                        </div>
								    </div>
								</div>
								@endforeach
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@stop
