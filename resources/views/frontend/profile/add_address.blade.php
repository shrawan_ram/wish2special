@extends('frontend.layout.master')

@section('title', 'Add Address')

@section('contant')

		<main>
     <div id="wrapper">
  <!-- banner 
    ============================================= -->
  
  <div class="category_background" style="background-attachment: fixed; background-size: cover; color:#fff;">
        <div class="container">

        <div class="banner-content">
            <div class="container" >
                <h1 class="text-center text-white">Add Address</h1>
                <ol class="breadcrumb text-center">
                  <li><a href="{{url('/')}}">Home</a></li>
                  <li><a href="{{url('/account')}}">Profile</a></li>
                  <li>Add Address</li>
                </ol>
            </div>
          </div>
      </div>
    </div>
        <!-- End Breadcrumb Part -->
        <section class="home-icon login-register bg-skeen">
            <div class="icon-default icon-skeen">
                <img src="{{url('imgs/scroll-arrow.png')}}" alt="">
            </div>
            <div class="container">
                <div class="row">
                    @include('frontend.profile.sidebar')
                    <div class="col-sm-8">
						<div class="panel">
							<div class="panel-body">
								<a href="{{ route('address') }}" class="pull-right" title="Edit" data-toggle="tooltip"><i class="fa fa-map-marker" style="background: none"></i> View Address</a>
								<h3>Add Address</h3>
								{{ Form::open() }}
								@if (\Session::has('success'))
					                <div class="alert alert-success toast-msg" style="color: green">
					                    {!! \Session::get('success') !!}</li>
					                </div>
					            @endif

					            @if (\Session::has('danger'))
					                <div class="alert alert-danger toast-msg" style="color: red;">
					                    {!! \Session::get('danger') !!}</li>
					                </div>
					            @endif

								@if($message = Session::get('error'))
								   <div class="alert alert-danger alert-block">
								     <button type="button" class="close" data-dismiss="alert">x</button>
								     {{$message}}
								   </div>
								  @endif
								  @if(count($errors->all()))
								    <div class="alert alert-danger">
								      <ul>
								        @foreach($errors->all() as $error)
								          <li>{{$error}}</li>
								        @endforeach
								      </ul>
								    </div>
								@endif
								<div class="row">
									<div class="col-xs-6">
										{{ Form::label('country', 'Country Name') }}
										{{ Form::text('country', '', ['class' => ' form-control', 'placeholder' => 'Country Name', 'required', 'id' => 'country']) }}
									</div>
									<div class="col-xs-6">
										{{ Form::label('state', 'State Name') }}
										{{Form::select('state', $states,'0', ['class' => 'select-dropbox form-control', 'id' => 'state', 'required'])}}
									</div>
								</div>
								<div class="row">
									<div class="col-xs-6">
										{{ Form::label('city', 'City Name') }}
										{{Form::select('city', $cities,'0', ['class' => 'select-dropbox form-control', 'id' => 'city', 'required'])}}
									</div>
									<div class="col-xs-6">
										{{ Form::label('pincode', 'Pincode') }}
										{{ Form::number('pincode', '', ['class' => ' form-control', 'placeholder' => 'Pincode', 'required', 'id' => 'pincode']) }}
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										{{ Form::label('address', 'Address') }}
										{{ Form::text('address', '', ['class' => ' form-control', 'placeholder' => 'Address', 'required', 'id' => 'address']) }}
									</div>
								</div>
								<div class="form-group">
									{{ Form::submit('Add', ['class' => 'btn-main btn add_cart_btn']) }}
								</div>
								{{ Form::close() }}
							</div>
						</div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@stop
