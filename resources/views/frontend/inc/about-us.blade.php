@extends('frontend.layout.master')

@section('title','')

@section('contant')
  <!--<section class="banner about dark" >-->
  <!--  <div id="service-parallax">-->
  <!--    <div class="bcg background1"-->
  <!--              data-center="background-position: 50% 0px;"-->
  <!--              data-bottom-top="background-position: 50% 100px;"-->
  <!--              data-top-bottom="background-position: 50% -100px;"-->
  <!--              data-anchor-target="#service-parallax"-->
  <!--            >-->
  <!--      <div class="bg-transparent">-->
  <!--        <div class="banner-content">-->
  <!--          <div class="container" >-->
  <!--            <div class="slider-content"> <img src="{{ url('imgs/'.$setting->logo) }}" width="40px">-->
  <!--              <h1>About us</h1>-->
  <!--              <ol class="breadcrumb">-->
  <!--                <li><a href="{{url('/')}}">Home</a></li>-->
  <!--                <li>About Us</li>-->
  <!--              </ol>-->
  <!--            </div>-->
  <!--          </div>-->
  <!--        </div>-->
  <!--      </div>-->
  <!--    </div>-->
  <!--  </div>-->
  <!--</section>-->
  <div class="category_background" style="background-attachment: fixed; background-size: cover; color:#fff;">
        <div class="container">

        <div class="banner-content">
            <div class="container" >
                <h1 class="text-center text-white">About us</h1>
                <ol class="breadcrumb text-center">
                  <li><a href="{{url('/')}}">Home</a></li>
                  <li>About Us</li>
                </ol>
            </div>
          </div>
      </div>
    </div>
<div id="content">
    <section id="intro01" class="padding-100 intro2_01">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="stk-po-t">
              <img class="img-responsive" src="{{url('imgs/home2/'.$about->image)}}" alt="">
            </div> 
           </div>
          <div class="col-md-5 text-center intro_message mt40"> 
            <!-- Head Title -->
            <div class="head_title">
              <i class="icon-intro"></i>
                <h1>{{ $about->title }}</h1>
                <span class="welcome">About Us</span>
            </div>
            <!-- End# Head Title -->
            <p>{!! $about->description !!}</p>
          </div>
          <!-- End intro center -->
        </div>
      </div>
    </section>
    <!-- End intro -->
@stop