@extends('frontend.layout.master')

@section('title','Blog')

@section('contant')
<div class="container">
<div class="theme-page padding-bottom-66">
	<div class="row gray full-width page-header vertical-align-table">
		<div class="row full-width padding-top-bottom-50 vertical-align-cell">
			<div class="row">
				<div class="page-header-left">
					<h1 class="heading-cptl">{{ $slug->title }}</h1>
				</div>
				<div class="page-header-right">
					<div class="bread-crumb-container">
						<label>You Are Here:</label>
						<ul class="bread-crumb">
							<li>
								<a title="Our Projects" href="index56e1.html?page=projects">
									OUR PROJECTS
								</a>
							</li>
							<li class="separator">
								&#47;
							</li>
							<li>
								{{ $slug->title }}
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix">
		<div class="row margin-top-70">
			<div class="column column-1-2">
				<a href="{{url('imgs/samples/750x500/image_01.jpg')}}" class="prettyPhoto re-preload" title="Interior Renovation">
					@if($slug->image!='')
					<img src="{{url('imgs/project/'.$slug->image)}}" alt="">
					@else
					<img src="{{url('img/unavailable-image-300x225.jpg')}}" alt="">
					@endif
				</a>
				<div class="row margin-top-30">
					<div class="column column-1-2">
						<a href="{{url('imgs/samples/750x500/image_06.jpg')}}" class="prettyPhoto re-preload" title="Interior Renovation">
							<img src="{{url('imgs/samples/570x380/image_06.jpg')}}" alt='img'>
						</a>
					</div>
					<div class="column column-1-2">
						<a href="{{url('imgs/samples/750x500/image_02.jpg')}}" class="prettyPhoto re-preload" title="Interior Renovation">
							<img src="{{url('imgs/samples/570x380/image_02.jpg')}}" alt='img'>
						</a>
					</div>
				</div>
				<div class="row margin-top-30">
					<a href="{{url('imgs/samples/750x500/image_07.jpg')}}" class="prettyPhoto re-preload" title="Interior Renovation">
					<img src="{{url('imgs/samples/570x380/image_07.jpg')}}" alt='img'>
				</a>
				</div>
			</div>
			<div class="column column-1-2">
				<h3 class="box-header">BRIEF DESCRIPTION</h3>
				<p>{!! $slug->description !!}</p>
			</div>
		</div>
	</div>
</div>
</div>
@stop