@extends('frontend.layout.master')

@section('title','')

@section('contant')
<!-- banner ============================================= -->
  <!--<section class="banner dark" >-->
  <!--  <div id="blog-parallax">-->
  <!--    <div class="bcg background2"-->
  <!--              data-center="background-position: 50% 0px;"-->
  <!--              data-bottom-top="background-position: 50% 100px;"-->
  <!--              data-top-bottom="background-position: 50% -100px;"-->
  <!--              data-anchor-target="#blog-parallax"-->
  <!--            >-->
  <!--      <div class="bg-transparent">-->
  <!--        <div class="banner-content">-->
  <!--          <div class="container">-->
  <!--            <div class="slider-content"> <img src="{{ url('imgs/'.$setting->logo) }}" width="40px">-->
  <!--              <h1>Blog</h1>-->
  <!--              <ol class="breadcrumb">-->
  <!--                <li><a href="{{url('/')}}">Home</a></li>-->
  <!--                <li>Blog</li>-->
  <!--              </ol>-->
  <!--            </div>-->
  <!--          </div>-->
  <!--        </div>-->
          <!-- End Banner content -->
  <!--      </div>-->
        <!-- End bg trnsparent -->
  <!--    </div>-->
  <!--  </div>-->
    <!-- Service parallax -->
  <!--</section>-->
<!-- End Banner -->
<!-- Header ============================================= -->
 
  <div class="category_background" style="background-attachment: fixed; background-size: cover; color:#fff;">
        <div class="container">

        <div class="banner-content">
            <div class="container" >
                <h1 class="text-center text-white">Blog</h1>
                <ol class="breadcrumb text-center">
                  <li><a href="{{url('/')}}">Home</a></li>
                  <li>Blog</li>
                </ol>
            </div>
          </div>
      </div>
    </div>
<!-- End header -->
<div class="container">
  <!-- <section class="row my-5" >
    @include('frontend.template.blog-sidebar')

    <div  class="col-12 col-md-8 col-sm-8 col-lg-8 col-xs-12 w3l-services1">
        <div class="blog-right-section">
          @foreach( $blog as $list )
              <div class="blog-card blog-shw">
                  <div class="blog-right-listing wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInDown;">
                      <div class="feature-img">
                        @if($list->image!='')
                        <img src="{{ url('imgs/blogs/'.$list->image) }}" alt="{{ $list->title }}">
                        @else                                            
                        <img src="{{ url('imgs/unavailable-image-300x225.jpg') }}" alt="{{ $list->title }}" style="border-radius: 15px;">
                        @endif
                          <div class="date-feature">
                              {{ $list->created_at->format('d') }}
                              <br>
                              <small>{{ $list->created_at->format('M') }}</small>
                          </div>
                      </div>
                      <div class="feature-info">
                          <span><i class="icon-user-1"></i>{{ $setting->sitename }}</span>
                          <span><i class="icon-comment-5"></i> 0 Comments</span>
                          <span class="share-tag-span" style="margin-right: 0;">
                              <span class="hide-tag">
                                  SHARE :
                              </span>
                              
                              <span class="social-facebook">
                                  <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('bloginfo',$list->slug) }}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i>
                  </a>
                              </span>
                              <span class="social-tweeter" style="">
                                  <a href="https://twitter.com/home?status={{ route('bloginfo',$list->slug) }}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i>
                  </a>
                              </span>
                              <span class="social-linked" style="margin-right: 0;">
                                  <a href="https://www.linkedin.com/shareArticle?mini=true&url={{ route('bloginfo',$list->slug) }}" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i>
                  </a>
                              </span>
                          </span>
                          
                          <h5>{{ $list->title }}</h5>
                          <p>{{ $list->excerpt }}</p>
                          <a href="{{url('blog/info/'.$list->slug)}}" class="btn-default read-more">Read More <i class="icon-arrow-right2"></i></a>
                      </div>
                  </div>
              </div>  
              @endforeach                                             
              <div class="gallery-pagination">
                  <div class="gallery-pagination-inner">
                   
                      
                  </div>
              </div>
          </div>
    </div>
  </section> -->
  @if(!empty($blog))
  <section class="blog text-center padding-100">
      <div class="container">
        <div class="row">
          <div class="blog-content dark">
            <!-- blog item-->
            @foreach( $blog as $list )
            <div class="blog-item col-md-4 col-sm-6 col-xs-12">
              <figure>
                @if($list->image!='')
                <img class="img-responsive" src="{{ url('imgs/blogs/'.$list->image) }}" alt="{{ $list->title }}" style="height: 360px">
                @else
                <img class="img-responsive" src="{{ url('imgs/unavailable-image-300x225.jpg') }}" alt="{{ $list->title }}" style="height: 360px">
                @endif
                <figcaption class="text-center">
                  <div class="fig_container"> <i class="fa fa-picture-o"></i>
                    <h3><a href="{{url('blog/info/'.$list->slug)}}">{{ $list->title }}</a></h3>
                    <!-- <p>Event</p> -->
                    <div class="fig_content"> <a href="{{url('blog/info/'.$list->slug)}}">{{ $list->excerpt }}</a> </div>
                  </div>
                  <span class="btn btn-gold primary-bg text-white" style="font-size: 13px; border-radius: 8px;">{{ $list->created_at->format('d M Y') }}</span> </figcaption>
              </figure>
            </div>
            @endforeach 
            <!-- End blog item-->
          </div>
          <!-- End News Content -->
          <!-- Pagination -->
          {{$blog->links()}}
          <!-- <div class="clearfix"></div>
          <div class="col-md-12">
            <ul class="majesty_pagination">
              <li class="next"><a href="#">NEXT</a></li>
              <li class="active"><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li class="previous"><a href="#">PREVIOUS</a></li>
            </ul>
          </div> -->
          <!-- End Pagination -->
        </div>
      </div>
    </section>
    @else
      No Record's Found.
    @endif
</div>
@stop