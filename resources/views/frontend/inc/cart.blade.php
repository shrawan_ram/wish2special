@extends('frontend.layout.master')

@section('title','Cart')

@section('contant')
<!-- banner ============================================= -->
  <!--<section class="banner dark" >-->
  <!--  <div id="blog-parallax">-->
  <!--    <div class="bcg background2"-->
  <!--              data-center="background-position: 50% 0px;"-->
  <!--              data-bottom-top="background-position: 50% 100px;"-->
  <!--              data-top-bottom="background-position: 50% -100px;"-->
  <!--              data-anchor-target="#blog-parallax"-->
  <!--            >-->
  <!--      <div class="bg-transparent">-->
  <!--        <div class="banner-content">-->
  <!--          <div class="container">-->
  <!--            <div class="slider-content" style="padding-top: 100px;"> <img src="{{ url('imgs/'.$setting->logo) }}" width="40px">-->
  <!--              <h1>Cart</h1>-->
               
  <!--              <ol class="breadcrumb">-->
  <!--                <li><a href="{{url('/')}}">Home</a></li>-->
  <!--                <li>Cart</li>-->
  <!--              </ol>-->
  <!--            </div>-->
  <!--          </div>-->
  <!--        </div>-->
          <!-- End Banner content -->
  <!--      </div>-->
        <!-- End bg trnsparent -->
  <!--    </div>-->
  <!--  </div>-->
    <!-- Service parallax -->
  <!--</section>-->
  <div class="category_background" style="background-attachment: fixed; background-size: cover; color:#fff;">
        <div class="container">

        <div class="banner-content">
            <div class="container" >
                <h1 class="text-center text-white">Cart</h1>
                <ol class="breadcrumb text-center">
                  <li><a href="{{url('/')}}">Home</a></li>
                  <li>Cart</li>
                </ol>
            </div>
          </div>
      </div>
    </div>
    <div class="container">
      
        <div class="shop-cart-list wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
            <form action="{{ url('cart/update') }}" method="post" id="cartResponse">
                @include('frontend.template.cart', compact('cartProducts'))
            </form>
        </div>
        @if(!empty($cartProducts))
                <div id="cartFooter">
                    @include('frontend.template.cart_footer', compact('totalPrice', 'setting', 'cartProducts'))
                </div>
                @endif
    </div>
@stop