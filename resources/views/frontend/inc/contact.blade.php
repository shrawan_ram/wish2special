@extends('frontend.layout.master')

@section('title','Contact')

@section('contant')
<div id="wrapper">
  <!-- banner 
    ============================================= -->
  <!--<section class="banner dark">-->
  <!--  <div id="contact-parallax">-->
  <!--    <div class="bcg background8 skrollable skrollable-between" data-center="background-position: 50% 0px;" data-bottom-top="background-position: 50% 100px;" data-top-bottom="background-position: 50% -100px;" data-anchor-target="#contact-parallax" style="background-position: 50% -9.58409px;">-->
  <!--      <div class="bg-transparent">-->
  <!--        <div class="banner-content">-->
  <!--          <div class="container">-->
  <!--            <div class="slider-content" style="padding-top: 146.5px;"> <img src="{{ url('imgs/'.$setting->logo) }}" width="40px">-->
  <!--              <h1>CONTACT US</h1>-->
  <!--              <ol class="breadcrumb">-->
  <!--                <li><a href="{{url('/')}}">Home</a></li>-->
  <!--                <li>Contact Us</li>-->
  <!--              </ol>-->
  <!--            </div>-->
  <!--          </div>-->
  <!--        </div>-->
          <!-- End Banner content -->
  <!--      </div>-->
        <!-- End bg trnsparent -->
  <!--    </div>-->
  <!--  </div>-->
    <!-- Service parallax -->
  <!--</section>-->
 <div class="category_background" style="background-attachment: fixed; background-size: cover; color:#fff;">
        <div class="container">

        <div class="banner-content">
            <div class="container" >
                <h1 class="text-center text-white">Contact Us</h1>
                <ol class="breadcrumb text-center">
                  <li><a href="{{url('/')}}">Home</a></li>
                  <li>Contact Us</li>
                </ol>
            </div>
          </div>
      </div>
    </div>
<!-- End header -->
  <div id="content">
    <!-- Our Contact
    ============================================= -->
    <section class="contact text-center padding-100">
      <div class="container">
        <div class="row"> 
          <!-- Head Title -->
            <div class="head_title">
                <i class="icon-intro"></i>
                <h1>Send Message</h1>
                <span class="welcome">Keep in Touch</span>
            </div>
            <!-- End# Head Title -->
          <!-- Contact form -->
          <div class="contact-form">
            {{ Form::open(['id'=>'contact_form', 'data-url'=>route('contact-route') ,'class'=>'contact-form mb-5']) }}
              <!-- Form Group -->
              <div class="form-group">
                <div class="row">
                  <div class="col-md-4 col-sm-4 col-sx-12">
                    <!-- Element -->
                    <div class="element">
                      <input type="text" id="user_name" name="name" class="form-control text" placeholder="Name *" required="required">
                    </div>
                    <!-- End Element -->
                  </div>
                  <div class="col-md-4 col-sm-4 col-sx-12">
                    <!-- Element -->
                    <div class="element">
                      <input type="text" id="user_email" name="email" class="form-control text" placeholder="E-mail *" required="required">
                    </div>
                    <!-- End Element -->
                  </div>
                  <div class="col-md-4 col-sm-4 col-sx-12">
                    <!-- Element -->
                    <div class="element">
                      <input type="text" id="user_mobile" name="mobile" class="form-control text" placeholder="Mobile *" required="required">
                    </div>
                    <!-- End Element -->
                  </div>
                </div>
              </div>
              <!-- End form group -->
              <div class="row">
                <div class="col-md-12">
                  <!-- Form Group -->
                  <div class="form-group">
                    <!-- Element -->
                    <div class="element">
                      <div>
                        <!-- Element -->
                        <div class="element">
                          <textarea name="message" id="user_message" class="text textarea" placeholder="Message *" required="required"></textarea>
                        </div>
                        <!-- End Element -->
                      </div>
                    </div>
                    <!-- End Element -->
                  </div>
                  <!-- End form Group -->
                </div>
              </div>
              <!-- Element -->
              <div class="element">
                <button type="submit" id="submit-form" value="Send" class="btn btn-black">Send</button>
              </div>
              <div id="contact_message">
                 
            </div>
              <!-- End Element -->
            {{ Form::close() }}
            
          </div>
          <!-- End contact form -->
        </div>
      </div>
    </section>
    <!-- End contact -->
    <!-- Address content
    ============================================= -->
    <section class="address-content dark">
      <!--  BG parallax -->
      <div id="address-content">
        <div class="bcg skrollable skrollable-before" data-center="background-position: 50% 0px;" data-bottom-top="background-position: 50% 100px;" data-top-bottom="background-position: 50% -100px;" data-anchor-target="#address-content" style="background-image: url(&quot;imgs/banner/certification.jpg&quot;); background-position: 50% 100px;">
          <!-- BG transparent -->
          <div class="bg-transparent padding-100">
            <div class="container">
              <div class="row">
                <!-- Adress -->
                <div class="col-md-4 adress">
                  <!-- Icon -->
                  <div class="col-md-3 icon "> <i class="fa fa-road"></i> </div>
                  <!-- End Icon -->
                  <!-- Content Item -->
                  <div class="col-md-9 content-item">
                    <h3>Adress</h3>
                    <p>{{ $setting->address }}</p>
                  </div>
                  <!-- End content Item -->
                </div>
                <!--End Adress -->
                <!-- Phone -->
                <div class="col-md-4 Phone">
                  <!-- Icon -->
                  <div class="col-md-3 icon"> <i class="fa fa-phone"></i> </div>
                  <!-- End Icon -->
                  <!-- Content Item -->
                  <div class="col-md-9 content-item">
                    <h3>PHONE</h3>
                    <p><a href="tel:{{ $setting->mobile }}">{{ $setting->mobile }}</a></p>
                  </div>
                  <!-- End content Item -->
                </div>
                <!--End Phone -->
                <!-- Email -->
                <div class="col-md-4 email">
                  <!-- Icon -->
                  <div class="col-md-3 icon"> <i class="fa fa-envelope"></i> </div>
                  <!-- End Icon -->
                  <!-- Content Item -->
                  <div class="col-md-9 content-item">
                    <h3>E-MAIL</h3>
                    <p><a href="mailto:{{ $setting->email }}">{{ $setting->email }}</a></p>
                  </div>
                  <!-- End content Item -->
                </div>
                <!--End Email -->
              </div>
            </div>
          </div>
        </div>
        <!-- End BG transparent -->
      </div>
      <!-- End BG parallax -->
    </section>

    <!-- End address content -->
    <!-- Map 
    ============================================= -->
    
    <!-- End map -->
  </div>
  <!-- end of #content -->

  
  
@stop