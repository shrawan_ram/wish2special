@extends('frontend.layout.master')

@section('title', 'Checkout')

@section('contant')
    <div id="wrapper">
  <!-- banner 
    ============================================= -->
  <!--<section class="banner dark">-->
  <!--  <div id="contact-parallax">-->
  <!--    <div class="bcg background8 skrollable skrollable-between" data-center="background-position: 50% 0px;" data-bottom-top="background-position: 50% 100px;" data-top-bottom="background-position: 50% -100px;" data-anchor-target="#contact-parallax" style="background-position: 50% -9.58409px;">-->
  <!--      <div class="bg-transparent">-->
  <!--        <div class="banner-content">-->
  <!--          <div class="container">-->
  <!--            <div class="slider-content" style="padding-top: 146.5px;"> <img src="{{ url('imgs/'.$setting->logo) }}" width="40px">-->
  <!--              <h1>Checkout as Guest</h1>-->
  <!--              <ol class="breadcrumb">-->
  <!--                <li><a href="{{url('/')}}">Home</a></li>-->
  <!--                <li><a >Checkout</a></li>-->
  <!--              </ol>-->
  <!--            </div>-->
  <!--          </div>-->
  <!--        </div>-->
          <!-- End Banner content -->
  <!--      </div>-->
        <!-- End bg trnsparent -->
  <!--    </div>-->
  <!--  </div>-->
    <!-- Service parallax -->
  <!--</section>-->
  <!-- End Banner -->
  <div class="category_background" style="background-attachment: fixed; background-size: cover; color:#fff;">
        <div class="container">

        <div class="banner-content">
            <div class="container" >
                <h1 class="text-center text-white">Checkout as Guest</h1>
                <ol class="breadcrumb text-center">
                  <li><a href="{{url('/')}}">Home</a></li>
                  <li>Checkout</li>
                </ol>
            </div>
          </div>
      </div>
    </div>
        <!-- End Breadcrumb Part -->
		@if(!empty($cartProducts))
		{{ Form::open(['class' => 'form-checkout', 'id' => 'guestForm']) }}
        <section class="home-icon shop-cart bg-skeen">
            <div class="icon-default icon-skeen">
                <img src="{{url('imgs/scroll-arrow.png')}}" alt="">
            </div>
            <div class="container">
                <!-- <div class="checkout-wrap wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                    <ul class="checkout-bar">
                        <li class="done-proceed">Shopping Cart</li>
                        <li class="active">Checkout as Guest</li>
                        <li>Order Complete</li>
                    </ul>
                </div> -->
                <div class="row">
                    <div class="col-md-7 col-sm-7 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="shop-checkout-left mb-5" style="padding: 15px 30px 10px;">
                            <span class="check-span"><i class="fa fa-user"></i> <span class="check-span1">Guest</span></span>
                            <span class="check-span2">
                                <a href="#" data-toggle="modal" data-target="#user_login">Login</a>
                            </span>
                        </div>
                    
                        <div class="shop-checkout-left">
                            <!-- <h6>Returning customer? Click here to <a href="login_register.html">login</a></h6> -->
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <h5>Billing Details</h5>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <input type="text" name="order[billing_fname]" placeholder="First Name *" value="" class="form-control" required>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <input type="text" name="order[billing_lname]" value="" placeholder="Last Name" class="form-control">
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="email" name="order[billing_email]" value="" placeholder="Email *" class="form-control" required>
                                    </div>
                                    <!-- <div class="col-md-6 col-sm-12 col-xs-12">
                                        <input type="text" name="order[billing_phone]" value="" placeholder="Phone *" required>
                                    </div> -->
                                    <div class="col-xs-12">
                                        <input type="text" name="order[billing_phone]" placeholder="Mobile Number" id="guest_mobile" class="form-control" required>
                                        <button type="button" data-target="#guest_mobile" class="btn btn-xs add_cart_btn form-btn send_otp_btn ">Send OTP</button>
                                    </div>
                                    <div class="col-xs-12">
                                        <input type="text" name="otp" placeholder="OTP Code" id="otp_sec" class="form-control" required>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <textarea name="billing[address]" placeholder="Address" class="form-control" required></textarea>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <select name="billing[country]" class="select-dropbox form-control" required>
                                            <option value="">Select Country *</option>
                                            <option selected>India</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <select name="billing[state]" class="select-dropbox form-control" required>
                                            <option value="">Select State *</option>
                                            <option selected>Rajasthan</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <select name="billing[city]" class="select-dropbox form-control" required>
                                            <option value="">Select City *</option>
                                            <option selected>Jodhpur</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <input type="text" name="billing[pincode]" placeholder="Pincode" class="form-control" required>
                                    </div>
                                    <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                                        <h5>Shipping Address</h5>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label class="custom-checkbox">
                                            <input type="checkbox" name="order[is_other_shipping]" value="1" id="shipDifferentAddress" class="form-control">
                                            <span>Ship to a different address?</span>
										</label>
                                    </div> -->
									<!-- <div id="shippingAddress" class="hidden">
										<div class="col-md-6 col-sm-12 col-xs-12">
	                                        <input type="text" name="order[shipping_fname]" placeholder="First Name *" class="require form-control">
	                                    </div>
	                                    <div class="col-md-6 col-sm-12 col-xs-12">
	                                        <input type="text" name="order[shipping_lname]" placeholder="Last Name" class="form-control">
	                                    </div>
	                                    <div class="col-md-12 col-sm-12 col-xs-12">
	                                        <input type="text" name="order[shipping_company_name]" placeholder="Company Name" class="form-control">
	                                    </div>
	                                    <div class="col-md-6 col-sm-12 col-xs-12">
	                                        <input type="email" name="order[shipping_email]" placeholder="Email *" class="require form-control">
	                                    </div>
	                                    <div class="col-md-6 col-sm-12 col-xs-12">
	                                        <input type="text" name="order[shipping_phone]" placeholder="Phone *" class="require form-control">
	                                    </div>

	                                    <div class="col-md-12 col-sm-12 col-xs-12">
	                                        <textarea name="shipping[address]" placeholder="Address *" class="require form-control"></textarea>
	                                    </div>
	                                    <div class="col-md-6 col-sm-12 col-xs-12">
	                                        <select name="shipping[country]" class="select-dropbox require form-control">
	                                            <option value="">Select Country *</option>
	                                            <option>India</option>
	                                        </select>
	                                    </div>
	                                    <div class="col-md-6 col-sm-12 col-xs-12">
	                                        <select name="shipping[state]" class="select-dropbox require form-control">
	                                            <option value="">Select State *</option>
	                                            <option>Rajasthan</option>
	                                        </select>
	                                    </div>
	                                    <div class="col-md-6 col-sm-12 col-xs-12">
	                                        <select name="shipping[city]" class="select-dropbox require form-control">
	                                            <option value="">Select City *</option>
	                                            <option>Jodhpur</option>
	                                        </select>
	                                    </div>
	                                    <div class="col-md-6 col-sm-12 col-xs-12">
	                                        <input type="text" name="shipping[pincode]" placeholder="Pincode *" class="require form-control" >
	                                    </div>
									</div> -->
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <textarea name="order[notes]" placeholder="Order Notes" class="form-control"></textarea>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="shop-checkout-right">
                            <div class="shop-checkout-box">
                                <h5>YOUR ORDER</h5>
                                <div class="shop-checkout-title">
                                    <h6>PRODUCT <span>TOTAL</span></h6>
                                </div>
                                <div class="shop-checkout-row">
									@foreach( $cartProducts as $p )
                                    <p><span>{{ $p->product->title ? $p->product->title : '' }}</span> <small style="width: 50px; text-align: right;">₹{{ $p->sale_price * $p->qty }}</small> <small style="margin-right: 50px;">x{{ $p->qty }}</small></p>
                                    @endforeach
                                </div>
                                <div class="checkout-total">
                                    <h6>CART SUBTOTAL <small>₹{{ $totalPrice }}</small></h6>
                                </div>
                                <div class="checkout-total">
                                    <h6>SHIPPING <small>{{ !empty($setting->shipping_charge) ? '₹'.number_format( $setting->shipping_charge, 2 ) : 'Free Shipping' }}</small></h6>
                                </div>
								@php
								$totalAmt = !empty($setting->shipping_charge) ? $totalPrice + $setting->shipping_charge : $totalPrice;
								@endphp
                                <div class="checkout-total">
                                    <h6>ORDER TOTAL <small class="price-big">₹{{ number_format($totalAmt) }}</small></h6>
                                </div>
								<input type="hidden" name="order[total_amount]" value="{{ $totalAmt }}">
                            </div>
                            <div class="shop-checkout-box">
                                <h5>PAYMENT METHODS</h5>
                                
                                <!-- <div class="payment-mode form-group">
                                    <input type="checkbox" name="" value="" class="paymentMethod m-0" onclick="$('.details').slideToggle(function(){$('#show').html($('.shipping-details').is(':visible'));});" style="border-color:red; margin-top: -3px;">
                                    <span>&nbsp;Take away</span>
                                  </div>
                                  <div class="row details" style="display: none;">
                                    <div class="col-sm-6">
        								<input type="text" min="<?php echo date('m/d/Y') ?>" name="order[schedule_date]" placeholder="Pick a Date" class="date-pick">
        							</div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
        								<select name="order[schedule_time]" class="select-dropbox">
                                            <option value="">Select Time</option>
        									@for($hr = $setting->opening_hour; $hr <= $setting->closing_hour; $hr++)
        										@php
        											$min = $hr == $setting->opening_hour ? $setting->opening_minute : 0;
        											$max = $hr == $setting->closing_hour ? $setting->closing_minute : 45;
        											$hour = $hr > 12 ? $hr - 12 : $hr;
        											$apm  = $hr > 12 ? "PM" : "AM";
        										@endphp
        										@while($min <= $max)
        	                                    	<option value="{{ sprintf("%02d:%02d %s", $hour, $min, $apm) }}">{{ sprintf("%02d:%02d %s", $hour, $min, $apm) }}</option>
        											@php
        												$min += 15;
        											@endphp
        										@endwhile
        									@endfor
                                        </select>
                                    </div>
                                  </div> -->
                                
                                
                                <!--<div class="payment-mode">-->
                                <!--    <label class="custom-radio-btn">-->
                                <!--        <input type="radio" name="order[payment_mode]" value="COT" class="paymentMethod">-->
                                <!--        <span>COT - Cash on Table</span>-->
                                <!--    </label>-->
                                <!--</div>-->
                                
                                <div class="payment-mode">
                                    <label class="custom-radio-btn">
                                        <input type="radio" name="order[payment_mode]" value="COD" checked class="paymentMethod">
                                        <span>COD - Cash on Delivery</span>
                                    </label>
                                </div>
                                <div class="payment-mode">
									<a href="#" class="pull-right" target="_blank"><i class="icon-info-circled-2"></i> What is RazorPay?</a>
                                    <label class="custom-radio-btn">
                                        <input type="radio" name="order[payment_mode]" value="RazorPay" class="paymentMethod">
                                        <span>RPay - RazorPay</span>
                                    </label>
                                    <input type="hidden" name="order[txn_id]" value="" id="paymentID">
                                </div>
                                <div class="checkout-terms">
                                    <label class="custom-checkbox">
                                        <input type="checkbox" name="checkbox" checked>
                                        <span>I’ve read and accept the terms &amp; conditions *</span>
                                    </label>
                                </div>  
                                <div class="checkout-button">
                                    <button type="submit" class="button-default btn-large btn-primary-gold">PROCEED TO PAYMENT</button>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		{{ Form::close() }}
		@else
			<div>
				Cart is empty.
			</div>
		@endif
    </div>
@stop

@section('footer-script')
<script>
    $('#rzp-footer-form').submit(function (e) {
        var button = $(this).find('button');
        var parent = $(this);
        button.attr('disabled', 'true').html('Please Wait...');
        $.ajax({
            method: 'get',
            url: this.action,
            data: $(this).serialize(),
            complete: function (r) {
                console.log('complete');
                //console.log(r);
            }
        })
        return false;
    })
</script>

<script>
    function padStart(str) {
        return ('0' + str).slice(-2)
    }

    function demoSuccessHandler(transaction) {
        let form = $('#guestForm');
        // You can write success code here. If you want to store some data in database.
        $("#paymentDetail").removeAttr('style');
        if(transaction) {
            $('#paymentID').val(transaction.razorpay_payment_id);
        }

        $.ajax({
            method: 'post',
            url: "{!! route('dopayment1') !!}",
            data: form.serialize(),
            //contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                if(r.success)
                    {
                        window.location = baseUrl+"guest/thank_you/?order="+r.id;
                    }
                else{
                    $("#otp_sec").focus();
                    $("#otp_sec").css({'border-color':'red'});
                }
            }
        })
    }

    var options = {
        key: "{{ env('RAZORPAY_KEY') }}",
        amount: {{ !empty( $totalAmt ) ? round($totalAmt * 100) : 0 }},
        name: 'City Food',
        description: 'A Food Restaurant',
        image: '{{ url("imgs/logo.png") }}',
        handler: demoSuccessHandler
    }

    window.r = new Razorpay(options);

    $(document).on('submit', '#guestForm', function(e) {
        e.preventDefault();
        is_valid = $(this).is_valid();
        if(is_valid) {
            let payMethod = $('.paymentMethod:checked').val();

            if(payMethod == "RazorPay") {
                r.open();
            } else {
                demoSuccessHandler(false);
            }
        }
    });
</script>
@endsection
