@extends('frontend.layout.master')

@section('title','Single Blog')

@section('contant')
<div id="wrapper">
<!--<section class="banner dark">-->
<!--    <div id="contact-parallax">-->
<!--      <div class="bcg background8 skrollable skrollable-between" data-center="background-position: 50% 0px;" data-bottom-top="background-position: 50% 100px;" data-top-bottom="background-position: 50% -100px;" data-anchor-target="#contact-parallax" style="background-position: 50% -9.58409px;">-->
<!--        <div class="bg-transparent">-->
<!--          <div class="banner-content">-->
<!--            <div class="container">-->
<!--              <div class="slider-content" style="padding-top: 146.5px;"> <img src="{{ url('imgs/'.$setting->logo) }}" width="40px">-->
<!--                <h1>Menu</h1>-->
<!--                <ol class="breadcrumb">-->
<!--                  <li><a href="{{url('/')}}">Home</a></li>-->
<!--                  <li>Menu</li>-->
<!--                </ol>-->
<!--              </div>-->
<!--            </div>-->
<!--          </div>-->
          <!-- End Banner content -->
<!--        </div>-->
        <!-- End bg trnsparent -->
<!--      </div>-->
<!--    </div>-->
    <!-- Service parallax -->
<!--  </section>-->
  <!-- End Banner -->
  <div class="category_background" style="background-attachment: fixed; background-size: cover; color:#fff;">
        <div class="container">

        <div class="banner-content">
            <div class="container" >
                <h1 class="text-center text-white">Menu</h1>
                <ol class="breadcrumb text-center">
                  <li><a href="{{url('/')}}">Home</a></li>
                  <li>menu</li>
                </ol>
            </div>
          </div>
      </div>
    </div>
<section>
      <div class="menu-tabs">
      
      <!-- Menu Items
                    ============================================= -->
      <div class="container">
        <!-- Menu Items Masonary Content -->
          
        <div id="menu-items" style="position: relative;margin-top: 50px;">
          @foreach($menu as $key => $item)
          @if($item->p_count)
          <article class="menu-item-list col-md-6 col-lg-3 col-sm-6 pf-{{$item->category->slug}}" style="position: absolute; left: 0px; top: 100px;">
            <!-- Card -->
          <div class="card">

            <div class="view zoom overlay">
              <img class="img-fluid w-100" src="{{ url('imgs/product/'.$item->image) }}" alt="{{ $item->title }}" style="height: 165px;">
                 <a href="#!">
                <div class="mask">
                  <img class="img-fluid w-100" src="{{ url('imgs/product/'.$item->image) }}" alt="{{ $item->title }}" style="height: 165px;">
                  <div class="mask rgba-black-slight"></div>
                </div>
              </a>
            </div>

            <div class="card-body text-center" style="padding-bottom: 20px">

              <h5>{{ $item->title }}</h5>
              <p class="small text-muted text-uppercase">{{ $item->category ? $item->category->title : '' }}</p>
              
              <hr class="mt-0">
              <div class="row">
                 <h6 class="col-6 float-left">
                     <!-- @php
                     $weight         = $item->weight;
                     $weight_arr     = explode (",", $weight);

                     $unit           = $item->unit;
                     $unit_arr       = explode (",", $unit);

                     $sale_price     = $item->sale_price;
                     $sale_price_arr = explode (",", $sale_price);

                     @endphp -->

                     
                     
                     @php
                     $parentArr = [];
                     
                     @endphp
                  <span class="my_setup">
                          <select class="weight-select select2">
                      @foreach($item->p as $pd) 
                          
                            <option data-price="₹{{ $pd->sale_price }}" value="{{ $pd->id }}">{{ $pd->weight }}</option>

                      @endforeach 
                          </select>

                      <button type="button" class="btn add_cart_btn mr-1 mb-2 mt-4 checkout addToCartBtn" data-url="{{ url('cart/add') }}" data-pid="{{ $item->p[0]->id }}">
                        <i class="fa fa-shopping-cart pr-2 " ></i> Add to cart
                      </button>
                      <span class="saleprice">₹{{ $item->p[0]->sale_price }}</span>
                  </span>
                      
                <a type="button" href="" class="btn btn-light mr-1 mb-2 p-detail" data-toggle="modal" data-target="#product_view{{ $item->slug }}">
                        <i class="fa fa-info-circle pr-2"></i> Details
                      </a>
                  
                  </h6>
                 <!--  <h6 class="mb-3 col-6 float-right" style="line-height: 30px;">
                    <span class="text-danger mr-1" id="demo">{{ Form::select('category_id',$sale_price_arr, '', ['class' => 'weight-select select2']) }}</span>
                      <span class="text-grey" id="{{ 'c_pd_id'.$pd->id}}"><s>₹{{ $item->regular_price }}</s></span>
                  </h6> --> 
              </div>
              
    
             
               
              <!-- <button type="button" class="btn btn-danger px-3 mb-2 material-tooltip-main" data-toggle="tooltip" data-placement="top" title="Add to wishlist">
                <i class="fa fa-heart"></i>
              </button> -->

            </div>

          </div>
<!-- Card -->
          </article>
        
            <div class="modal fade product_view" id="product_view{{ $item->slug }}">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <a href="#" data-dismiss="modal" class="class pull-right"><span class="fa fa-times"></span></a>
                            <h3 class="modal-title">Product</h3>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 product_img">
                                    <img src="{{ url('imgs/product/'.$item->image) }}" class="img-responsive">
                                </div>
                                <div class="col-md-6 product_content">
                                    <h4>{{ $item->title }}</h4>
                                    <!--<div class="rating">-->
                                    <!--    <span class="glyphicon glyphicon-star"></span>-->
                                    <!--    <span class="glyphicon glyphicon-star"></span>-->
                                    <!--    <span class="glyphicon glyphicon-star"></span>-->
                                    <!--    <span class="glyphicon glyphicon-star"></span>-->
                                    <!--    <span class="glyphicon glyphicon-star"></span>-->
                                    <!--    (10 reviews)-->
                                    <!--</div>-->
                                    <p>{!! $item->description !!}</p>
                                    <span class="my_setup">
                                        <select class="weight-selects select2">
                                            @foreach($item->p as $pd) 
                                                  <!-- ksort($pd, SORT_NUMERIC); -->
                                                
                                                  <option data-price="₹{{ $pd->sale_price }}" value="{{ $pd->id }}">{{ $pd->weight }}</option>

                                            @endforeach 
                                                </select>

                                          <button type="button" class="btn add_cart_btn mr-1 mb-2 mt-4 detailaddcart checkout addToCartBtn" data-url="{{ url('cart/add') }}" data-pid="{{ $item->p[0]->id }}">
                                            <i class="fa fa-shopping-cart pr-2 " ></i> Add to cart
                                          </button>
                                          <span class="detailprice">₹{{ $item->p[0]->sale_price }}</span>
                                          
                                      </span>
                                    <!-- <h3 class="cost">₹ {{ $item->sale_price }} <small class="pre-cost">₹ {{ $item->regular_price }}</small></h3>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                           
                                        </div>
                                       
                                        
                                        
                                    </div>
                                    <div class="space-ten"></div>
                                    <div class="btn-ground">
                                        <button type="button" class="btn add_cart_btn mr-1 mb-2 checkout addToCartBtn" data-url="{{ url('cart/add') }}" data-pid="{{ $item->id }}">
                                            <i class="fa fa-shopping-cart pr-2 " ></i> Add to cart
                                          </button>
                                        
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          

        
        @else
          
          @endif
          @endforeach
          </div>
        <!-- End Menu Content -->
      </div>
      <!-- #menu end -->
    </div>
    </div>
  </section>
@stop