@extends('frontend.layout.master')

@section('title','cart')

@section('contant')

	<main>
    <div class="main-part">
        <!-- Start Breadcrumb Part -->
        <section class="breadcrumb-part menu-part" data-stellar-offset-parent="true" data-stellar-background-ratio="0.5" style="background-image: url('{{ url('imgs/breadbg1.jpg') }}');">

            <div class="container">
                <div class="breadcrumb-inner">
                    <h2>CART</h2>
                    <a href="{{ url('/') }}">Home</a>
                    <a><span>Shop</span></a>
                </div>
            </div>
        </section>
        <!-- End Breadcrumb Part -->
        <section class="home-icon shop-cart bg-skeen">
            <div class="icon-default icon-skeen">
                <img src="{{url('imgs/scroll-arrow.png')}}" alt="">
            </div>
            <div class="container">
                <div class="checkout-wrap wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                    <ul class="checkout-bar">
                        <li class="active">Shopping Cart</li>
                        <li>Checkout</li>
                        <li>Order Complete</li>
                    </ul>
                </div>
                <div class="shop-cart-list wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
					<form action="{{ url('cart_resv/update') }}" method="post" id="cartResponse">
						@include('frontend.template.cart_resv', compact('cartProducts'))
					</form>
                </div>

				<div id="cartFooter">
					@include('frontend.template.cart_footer_resv', compact('totalPrice', 'setting', 'cartProducts'))
				</div>
            </div>
        </section>
    </div>
</main>

@stop
