@extends('frontend.layout.master')

@section('title','Blog')

@section('contant')
<section class="w3l-about-breadcrumb text-center">
    <div class="breadcrumb-bg breadcrumb-bg-about py-sm-5 py-4">
      	<div class="">
	        <h2 class="title">BLOG</h2>
	        <ul class="breadcrumbs-custom-path mt-2">
	          <li><a href="{{url('/')}}" style="color: #f2195d;font-weight: bold;">Home</a></li>
	          <li class="active"><span class="fa fa-arrow-right mx-2" aria-hidden="true"></span> Blog </li>
	        </ul>
      	</div>
    </div>
</section>
<div class="container">
	<section class="row my-5" >
		@include('frontend.template.blog-sidebar')

		<div  class="col-12 col-md-8 col-sm-8 col-lg-8 col-xs-12 w3l-services1">
	    	<div class="blog-right-section">
	    		@foreach( $latestblog as $list )
	            <div class="blog-card blog-shw">
	                <div class="blog-right-listing wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInDown;">
	                    <div class="feature-img">
		                    @if($list->image!='')
		                    <img src="{{ url('imgs/blogs/'.$list->image) }}" alt="{{ $list->title }}">
		                    @else                                            
		                    <img src="{{ url('imgs/unavailable-image-300x225.jpg') }}" alt="{{ $list->title }}" style="border-radius: 15px;">
		                    @endif
	                        <div class="date-feature">
	                            {{ $list->created_at->format('d') }}
	                            <br>
	                            <small>{{ $list->created_at->format('M') }}</small>
	                        </div>
	                    </div>
	                    <div class="feature-info">
	                        <span><i class="icon-user-1"></i>{{ $setting->sitename }}</span>
	                        <span><i class="icon-comment-5"></i> 0 Comments</span>
	                        <span class="share-tag-span" style="margin-right: 0;">
	                            <span class="hide-tag">
	                                SHARE :
	                            </span>
	                            
	                            <span class="social-facebook">
	                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('bloginfo',$list->slug) }}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i>
									</a>
	                            </span>
	                            <span class="social-tweeter" style="">
	                                <a href="https://twitter.com/home?status={{ route('bloginfo',$list->slug) }}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i>
									</a>
	                            </span>
	                            <span class="social-linked" style="margin-right: 0;">
	                                <a href="https://www.linkedin.com/shareArticle?mini=true&url={{ route('bloginfo',$list->slug) }}" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i>
									</a>
	                            </span>
	                        </span>
	                        
	                        <h5>{{ $list->title }}</h5>
	                        <p>{{ $list->excerpt }}</p>
	                        <a href="{{url('blog/info/'.$list->slug)}}" class="btn-default read-more">Read More <i class="icon-arrow-right2"></i></a>
	                    </div>
	                </div>
	            </div>  
	            @endforeach                                             
	            <div class="gallery-pagination">
	                <div class="gallery-pagination-inner">
	                    <!-- <ul>
	                        <li><a href="#" class="pagination-prev"><i class="icon-left-4"></i> <span>PREV page</span></a></li>
	                        <li class="active"><a href="#"><span>1</span></a></li>
	                        <li><a href="#"><span>2</span></a></li>
	                        <li><a href="#"><span>3</span></a></li>
	                        <li><a href="#" class="pagination-next"><span>next page</span> <i class="icon-right-4"></i></a></li>
	                    </ul> -->
	                    
	                    
	                </div>
	            </div>
	        </div>
		</div>
	</section>
</div>

@stop