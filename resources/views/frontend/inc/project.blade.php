@extends('frontend.layout.master')

@section('title','Project')

@section('contant')

<div class="theme-page padding-bottom-70" style="margin: auto;">
	<div class="row gray full-width page-header vertical-align-table">
		<div class="row full-width padding-top-bottom-50 vertical-align-cell">
			<div class="row">
				<div class="page-header-left">
					<h1>OUR PROJECTS</h1>
				</div>
				<div class="page-header-right">
					<div class="bread-crumb-container">
						<label>You Are Here:</label>
						<ul class="bread-crumb">
							<li>
								<a title="Home" href="{{url('/')}}">
									HOME
								</a>
							</li>
							<li class="separator">
								&#47;
							</li>
							<li>
								OUR PROJECTS
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix">
		<div class="row">
			
			<ul class="projects-list isotope" style="margin-top: 40px;">
				@foreach($project as $list)
				<li class="renovation">
					<a href="{{ url('project/'. $list->slug)}}" title="Interior Renovation">
						@if($list->image!='')
						<img src="{{url('imgs/project/'.$list->image)}}" alt="{{ $list->title }}">
						@else
						<img src="{{url('imgs/unavailable-image-300x225.jpg')}}" alt="{{ $list->title }}">
						@endif
					</a>
					<div class="view align-center">
						<div class="vertical-align-table">
							<div class="vertical-align-cell">
								<p class="description">{{ $list->title }}</p>
								<a class="more simple" href="{{ url('project/'. $list->slug)}}" title="VIEW PROJECT">VIEW PROJECT</a>
							</div>
						</div>
					</div>
				</li>	
				@endforeach			
			</ul>
		</div>
	</div>
</div>

@stop