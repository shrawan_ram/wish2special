@extends('frontend.layout.master')

@section('title', 'Checkout')

@section('contant')
<main>
    <div class="main-part">
        <!-- Start Breadcrumb Part -->
        <section class="breadcrumb-part menu-part" data-stellar-offset-parent="true" data-stellar-background-ratio="0.5" style="background-image: url('{{ url('imgs/breadbg1.jpg') }}');">
            <div class="container">
                <div class="breadcrumb-inner">
                    <h2>Checkout</h2>
                    <a href="{{ url('/') }}">Home</a>
                    <a><span>Shop</span></a>
                </div>
            </div>
        </section>
        <!-- End Breadcrumb Part -->
		@if(!empty($cartProducts))
		{{ Form::open(['class' => '', 'id' => 'helloCheckout']) }}
        <section class="home-icon shop-cart bg-skeen">
            <div class="icon-default icon-skeen">
                <img src="{{url('imgs/scroll-arrow.png')}}" alt="">
            </div>
            <div class="container">
                <div class="checkout-wrap wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                    <ul class="checkout-bar">
                        <li class="done-proceed">Shopping Cart</li>
                        <li class="active">Checkout</li>
                        <li>Order Complete</li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-7 col-sm-7 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="shop-checkout-right">
                            <div class="shop-checkout-box">
                                <h5>YOUR TABLE ORDER</h5>
                                <div class="shop-checkout-title">
                                    <h6>PRODUCT <span>TOTAL</span></h6>
                                </div>
                                <div class="shop-checkout-row">
                                    @foreach( $cartProducts as $p )
                                    <p><span>{{ $p->name }}</span> <small style="width: 50px; text-align: right;">₹{{ $p->sale_price * $p->qty }}</small> <small style="margin-right: 50px;">x{{ $p->qty }}</small></p>
                                    @endforeach
                                </div>
                                <div class="checkout-total">
                                    <h6>CART SUBTOTAL <small>₹{{ $totalPrice }}</small></h6>
                                </div>
                                <div class="checkout-total">
                                    <h6>SHIPPING <small>{{ !empty($setting->shipping_charge) ? '₹'.number_format( $setting->shipping_charge, 2 ) : 'Free Shipping' }}</small></h6>
                                </div>
                                @php
                                $totalAmt = !empty($setting->shipping_charge) ? $totalPrice + $setting->shipping_charge : $totalPrice;
                                @endphp
                                <div class="checkout-total">
                                    <h6>ORDER TOTAL <small class="price-big">₹{{ number_format($totalAmt) }}</small></h6>
                                </div>
                                <input type="hidden" name="order[total_amount]" value="{{ $totalAmt }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="shop-checkout-right">
                            <div class="shop-checkout-box">
                                <h5>PAYMENT METHODS</h5>

                                <!--<div class="payment-mode">-->
                                <!--    <label class="custom-radio-btn">-->
                                <!--        <input type="radio" name="order[payment_mode]" value="COT" class="paymentMethod">-->
                                <!--        <span>COT - Cash on Table</span>-->
                                <!--    </label>-->
                                <!--</div>-->
                                
                                <div class="payment-mode">
                                    <label class="custom-radio-btn">
                                        <input type="radio" name="order[payment_mode]" value="COD" checked class="paymentMethod">
                                        <span>COD - Cash on Delivery</span>
                                    </label>
                                </div>
                                <div class="payment-mode">
                                    <a href="#" class="pull-right" target="_blank"><i class="icon-info-circled-2"></i> What is RazorPay?</a>
                                    <label class="custom-radio-btn">
                                        <input type="radio" name="order[payment_mode]" value="RazorPay" class="paymentMethod">
                                        <span>RPay - RazorPay</span>
                                    </label>
                                    <input type="hidden" name="order[txn_id]" value="" id="paymentID">
                                </div>
                                <div class="checkout-terms">
                                    <label class="custom-checkbox">
                                        <input type="checkbox" name="checkbox" checked>
                                        <span>I’ve read and accept the terms &amp; conditions *</span>
                                    </label>
                                </div>
                                <div class="checkout-button">
                                    <button type="submit" class="button-default btn-large btn-primary-gold">PROCEED TO PAYMENT</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		{{ Form::close() }}
		@else
			<div>
				Cart is empty.
			</div>
		@endif
    </div>
</main>
@endsection


@section('footer-script')
<script>
    $('#rzp-footer-form').submit(function (e) {
        var button = $(this).find('button');
        var parent = $(this);
        button.attr('disabled', 'true').html('Please Wait...');
        $.ajax({
            method: 'get',
            url: this.action,
            data: $(this).serialize(),
            complete: function (r) {
                console.log('complete');
                //console.log(r);
            }
        })
        return false;
    })
</script>

<script>
    function padStart(str) {
        return ('0' + str).slice(-2)
    }

    function demoSuccessHandler(transaction) {
        let form = $('#helloCheckout');
        // You can write success code here. If you want to store some data in database.
        $("#paymentDetail").removeAttr('style');
        if(transaction) {
            $('#paymentID').val(transaction.razorpay_payment_id);
        }

        $.ajax({
            method: 'post',
            url: "{!! route('dopayment2') !!}",
            data: form.serialize(),
            //contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                window.location = baseUrl+"checkout_resv/thankyou";    
            }
        })
    }

    var options = {
        key: "{{ env('RAZORPAY_KEY') }}",
        amount: {{ !empty( $totalAmt ) ? round($totalAmt * 100) : 0 }},
        name: 'City Food',
        description: 'A Food Restaurant',
        image: '{{ url("imgs/logo.png") }}',
        handler: demoSuccessHandler
    }

    window.r = new Razorpay(options);

    $(document).on('submit', '#helloCheckout', function(e) {
        e.preventDefault();
        is_valid = $(this).is_valid();
        if(is_valid) {
            let payMethod = $('.paymentMethod:checked').val();

            if(payMethod == "RazorPay") {
                r.open();
            } else {
                demoSuccessHandler(false);
            }
        }
    });
</script>
@endsection

