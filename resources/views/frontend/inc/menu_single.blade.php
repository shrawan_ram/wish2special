@extends('frontend.layout.master')

@section('title','Cart')

@section('contant')
<!-- banner ============================================= -->
  <!--<section class="banner dark" >-->
  <!--  <div id="blog-parallax">-->
  <!--    <div class="bcg background2"-->
  <!--              data-center="background-position: 50% 0px;"-->
  <!--              data-bottom-top="background-position: 50% 100px;"-->
  <!--              data-top-bottom="background-position: 50% -100px;"-->
  <!--              data-anchor-target="#blog-parallax"-->
  <!--            >-->
  <!--      <div class="bg-transparent">-->
  <!--        <div class="banner-content">-->
  <!--          <div class="container">-->
  <!--            <div class="slider-content" style="padding-top: 100px;"> <img src="{{ url('imgs/'.$setting->logo) }}" width="40px">-->
  <!--              <h1>Menu</h1>-->
               
  <!--              <ol class="breadcrumb">-->
  <!--                <li><a href="{{url('/')}}">Home</a></li>-->
  <!--                <li>Menu</li>-->
  <!--                <li>{{ $slug->title }}</li>-->
  <!--              </ol>-->
  <!--            </div>-->
  <!--          </div>-->
  <!--        </div>-->
          <!-- End Banner content -->
  <!--      </div>-->
        <!-- End bg trnsparent -->
  <!--    </div>-->
  <!--  </div>-->
    <!-- Service parallax -->
  <!--</section>-->
  <div class="category_background" style="background-attachment: fixed; background-size: cover; color:#fff;">
        <div class="container">

        <div class="banner-content">
            <div class="container" >
                <h1 class="text-center text-white">Menu</h1>
                <ol class="breadcrumb text-center">
                  <li><a href="{{url('/')}}">Home</a></li>
                  <li><a href="{{url('/menu')}}">menu</a></li>
                  <li>{{ $slug->title }}</li>
                </ol>
            </div>
          </div>
      </div>
    </div>
<!-- End header -->
     <!-- End header -->
  <div id="content">
    <!-- Single menu
      ============================================= -->
    <section class="single-menu text-left padding-100">
      <div class="container">
        <div class="row">
          <!-- Menu thumb slider -->
          <div class="menu-thumb-slide col-md-6">
            
              <img src="{{ url('imgs/product/'.$slug->image)}}" alt="">
              
          </div>
          <!--End Menu thumb slider -->
          <!-- Menu Desc -->
          <div class="menu-desc col-md-6">
            <h2>{{ $slug->title }}<span class="pull-right">₹{{ $slug->sale_price }}</span></h2>
            <!-- Rating -->
            <!-- <fieldset class="rating">
              <span class="active"><i class="fa fa-star"></i></span> <span class="active"><i class="fa fa-star"></i></span> <span class="active"><i class="fa fa-star"></i></span> <span class="active"><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span>
            </fieldset> -->
            <!-- End Rating -->
            <!-- Tagged -->
            <!-- <div class="tagged"> <span class="label red">Spicy</span> <span class="label label-default">Dinner</span> <span class="label instock">In Stock</span> </div> -->
            <!-- Ends Tagged -->
            <!-- Description content -->
            <div class="desc-content">
              <p>{!! $slug->excerpt !!}</p>
              <!-- Meta description -->
              <div class="meta-desc"> 
                <button type="button" class="btn add_cart_btn mr-1 mb-2 checkout addToCartBtn" data-url="{{ url('cart/add') }}" data-pid="{{ $slug->id }}">
                <i class="fa fa-shopping-cart pr-2 " ></i> Add to cart
              </button>
                <ul class="social pull-right">
                  <li>
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ url('/menu/'.$slug->slug) }}" target="_blank">
                      <i class="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li><a href="https://twitter.com/intent/tweet?url={{ url('/menu/'.$slug->slug) }}" target="_blank"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="https://www.linkedin.com/shareArticle?mini=true&url={{ url('/menu/'.$slug->slug) }}" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                  <li><a href="mailto:info@example.com?&subject=&body={{ url('/menu/'.$slug->slug) }}" target="_blank"><i class="fa fa-envelope"></i></a></li>
                </ul>
              </div>
              <!--End Meta description -->
            </div>
            <!--End Description content -->
          </div>
          
          <!-- tab panel -->
        </div>
      </div>
    </section>
    <!-- End single menu -->
    <!-- Interest
    ============================================= -->
    <section class="interest-in padding-100 text-center">
      <div class="container">
        <div class="row">
          <div class="head_title text-center" style="margin-bottom: 0;">
            <i class="icon-intro icon-large"></i>
            <h1>MAY BE YOU INTEREST IN</h1>
          </div>
          <!-- Menu type -->
          <div class="menu-type dark">
            <!-- Item -->
            @foreach($data1['data'] as $sr)
            <div class="col-md-4 col-sm-4 col-xs-12 item">
              <!-- Overlay Content -->
              <div class="overlay_content overlay-menu">
                <!-- Overlay Item -->
                <div class="overlay_item"> <!-- <span class="label">best seller</span> --> <img src="{{ url('imgs/product/'.$sr['image'])}}" alt="" style="height: 350px;">
                  <!-- Overlay -->
                  <div class="overlay">
                    <!-- Icons -->
                    <div class="icons">
                      <h3>{{ $sr['title'] }}</h3>
                      <h3> ₹{{ $sr['sale_price'] }}</h3>
                      <span class="text-grey"><s>₹{{ $sr['regular_price'] }}</s></span>
                      <!-- Rating -->
                     <!--  <fieldset class="rating">
                        <span class="active"><i class="fa fa-star"></i></span> <span class="active"><i class="fa fa-star"></i></span> <span class="active"><i class="fa fa-star"></i></span> <span class="active"><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span>
                      </fieldset> -->
                      <!-- End Rating -->
                      <!-- Buttons -->
                      <div class="button"> 
                        <a type="button" class="btn btn-gold addToCartBtn" href="#" data-url="{{ url('cart/add') }}" data-pid="{{ $sr['id'] }}"><i class="fa fa-shopping-cart"></i></a> 
                        
                        <a class="btn btn-gold" href="{{ url('menu/'.$sr['slug'])}}"><i class="fa fa-link"></i></a> 
                      </div>
                      <!-- End Buttons -->
                      <a class="close-overlay hidden">x</a> </div>
                    <!-- End Icons -->
                  </div>
                  <!-- End Overlay -->
                </div>
                <!-- End Overlay Item -->
              </div>
              <!-- End Overlay Content -->
            </div>
            @endforeach
            
            
          </div>
          <!--End Menu type -->
        </div>
      </div>
    </section>
    <!-- End Interest -->
  </div>
@stop