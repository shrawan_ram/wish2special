@extends('frontend.layout.master')

@section('title', 'Checkout')

@section('contant')

<section>
   <div id="wrapper">
  <!-- banner 
    ============================================= -->
  <div class="category_background" style="background-attachment: fixed; background-size: cover; color:#fff;">
        <div class="container">

        <div class="banner-content">
            <div class="container" >
                <h1 class="text-center text-white">Checkout</h1>
                <ol class="breadcrumb text-center">
                  <li><a href="{{url('/')}}">Home</a></li>
                  <li>checkout</li>
                </ol>
            </div>
          </div>
      </div>
    </div>
  <!-- End Banner -->
        <!-- End Breadcrumb Part -->
		@if(!empty($cartProducts))
		{{ Form::open(['class' => 'form-checkout', 'id' => 'checkoutForm']) }}
        <section class="home-icon shop-cart bg-skeen">
            <div class="icon-default icon-skeen">
                <img src="{{url('imgs/scroll-arrow.png')}}" alt="">
            </div>
            <div class="container">
                <!-- <div class="checkout-wrap wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                    <ul class="checkout-bar">
                        <li class="done-proceed">Shopping Cart</li>
                        <li class="active">Checkout</li>
                        <li>Order Complete</li>
                    </ul>
                </div> -->
                <div class="row">
                    <div class="col-md-7 col-sm-7 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="shop-checkout-left mb-5">
                            <div class="row">
                                @if(Auth()->user())
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="checkout-log">
                                        <h4 style="margin-bottom: 0;">Logged in 
                                            <!-- <i class="fa fa-check-circle"></i> -->
                                        </h4>
                                        <div class="checkout-log-text">{{ auth()->user()->fname }} {{ auth()->user()->lname }} | {{ auth()->user()->mobile }}</div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="shop-checkout-left mb-5">
                            <div class="row">
                                @if(Auth()->user())
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-xs-6 mb-5">
                                                <h4 style="margin-bottom: 0;">Billing Address 
                                                    <!-- <i class="fa fa-check-circle"></i> -->
                                                </h4>
                                            </div>
                                            <div class="col-xs-6">
                                                <h6 class="text-right" style="margin-bottom: 0;"><a href="{{ route('add_address') }}">Add Address</a></h6>
                                            </div>
                                        </div>
                                        <div class="row">
                                            @foreach($address as $list)
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <div class="checkout-log checkout-checkbox mycheckbox">
                                                        <div class="row">
                                                            <div class="col-xs-1 checkout-log-add">
                                                                <div style="padding: 0 15px;">
                                                                    <i class="fa fa-map-marker"></i>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-9 checkout-log-add" style="padding: 10px 10px; min-height: 100px;">
                                                                <div>{{ $list->address }}, {{ $list->city }}, {{ $list->state }}, {{ $list->pincode }}, {{ $list->country }}</div>
                                                            </div>
                                                            <div class="col-xs-12 checkout-log-add text-center span">
                                                                <label class="custom-radio-btn1" style="margin-bottom: 0;">
                                                                    <input type="radio" name="order[billing_addressid]" value="{{ $list->address.','.$list->city.','.$list->state.','.$list->pincode.','.$list->country }}" class="addressId select_addressR form-control">
                                                                    <div class="form-control alert-success">Select Address</div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="shop-checkout-left mb-5">
                            <div class="row">
                                <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-xs-9">
                                                <h4 style="margin-bottom: 0;">Shipping Details 
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="custom-checkbox" style="padding: 10px 0px 0px 16px;">

                                                <input type="checkbox" name="order[is_other_shipping]" value="1" id="shipDifferentAddress" class="form-control">
                                                <span>Ship to a different address?</span>
                                            </label>
                                        </div>
                                    </div>
                                </div> -->
								<!-- <div id="shippingAddress" class="hidden">
									<div class="col-md-6 col-sm-12 col-xs-12">
                                        <input type="text" name="order[shipping_fname]" placeholder="First Name *" class="require form-control">
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <input type="text" name="order[shipping_lname]" placeholder="Last Name" class="form-control">
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <input type="email" name="order[shipping_email]" placeholder="Email *" class="require form-control">
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <input type="text" name="order[shipping_phone]" placeholder="Phone *" class="require form-control">
                                    </div>
                                    @foreach($address as $list)
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="checkout-log mycheckboxA">
                                                <div class="row">
                                                    <div class="col-xs-1 checkout-log-add">
                                                        <div style="padding: 0 15px;">
                                                            <i class="fa fa-map-marker"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-9 checkout-log-add" style="padding: 10px 10px; min-height: 100px;">
                                                        <div>{{ $list->address }}, {{ $list->city }}, {{ $list->state }}, {{ $list->pincode }}, {{ $list->country }}</div>
                                                    </div>
                                                    <div class="col-xs-12 checkout-log-add text-center">
                                                        <label class="custom-radio-btn" style="margin-bottom: 0;">
                                                            <input type="radio" name="order[shipping_addressid]" value="{{ $list->id }}" class="addressId1 select_addressA">
                                                            <div class="form-control alert-success">Select Address</div>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label class="custom-checkbox" style="padding: 10px 0px 0px 16px;">
                                            <input type="checkbox" name="shippingA[add_new_address]" value="1" id="shipDifferentAddress1">
                                            <span>Add New Address</span>
                                        </label>    
                                    </div>
                                    <div id="shippingAddress1" class="hidden">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <textarea name="shipping[address]" placeholder="Address *"></textarea>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <select name="shipping[country]" class="select-dropbox require">
                                                <option value="">Select Country *</option>
                                                <option>India</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            
                                            {{Form::select('shipping[state]', $states,'0', ['class' => 'select-dropbox'])}}
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            
                                            {{Form::select('shipping[city]', $cities,'0', ['class' => 'select-dropbox'])}}
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <input type="text" name="shipping[pincode]" placeholder="Pincode *">
                                        </div>
                                    </div>
								</div> -->
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <textarea name="order[notes]" placeholder="Order Notes"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <!-- <div class="shop-checkout-right">
                            <div class="shop-checkout-box">
                                <h5>YOUR ORDER</h5>
                                <div class="shop-checkout-title">
                                    <h6>PRODUCT <span>TOTAL</span></h6>
                                </div>
                                <div class="shop-checkout-row">
									@foreach( $cartProducts as $p )
                                    <p><span>{{ $p->product->title }}</span> <small style="width: 50px; text-align: right;">₹{{ $p->sale_price * $p->qty }}</small> <small style="margin-right: 50px;">x{{ $p->qty }}</small></p>
									@endforeach
                                </div>
                                <div class="checkout-total">
                                    <h6>CART SUBTOTAL <small>₹{{ $totalPrice }}</small></h6>
                                </div>
                                <div class="checkout-total">
                                    <h6>SHIPPING <small>{{ !empty($setting->shipping_charge) ? '₹'.number_format( $setting->shipping_charge, 2 ) : 'Free Shipping' }}</small></h6>
                                </div>
                                @php 
                                    $coupan = session()->get('coupon');
                                @endphp
                                @if(Auth()->user())
                                    @if(!empty($coupan))
                                        @if($coupan->discount_type == 'Flat')
                                            @php
                                                $Discount = $coupan->discount;
                                            @endphp
                                        @else
                                            @php
                                                $Discount = $totalPrice * $coupan->discount / 100;
                                            @endphp
                                        @endif
                                    <div class="checkout-total">
                                        <h6>DISCOUNT <small>{{ !empty($Discount) ? '₹'.number_format( $Discount ) : '₹'.number_format( 0 ) }}</small></h6>
                                    </div>
                                    @endif
                                @endif
								@php
								$totalAmt = !empty($Discount) ? $totalPrice + $setting->shipping_charge - $Discount : $totalPrice;
								@endphp
                                <div class="checkout-total">
                                    <h6>ORDER TOTAL <small class="price-big">₹{{ number_format($totalAmt) }}</small></h6>
                                </div>
								<input type="hidden" name="order[total_amount]" value="{{ $totalAmt }}">
                            </div>
                            <div class="shop-checkout-box">
                                <h5 style="margin-bottom: 0;">PAYMENT METHODS</h5>
                                
                                  
                                </div>
                                
                                <div class="payment-mode">
                                    <label class="custom-radio-btn">
                                        <input type="radio" name="order[payment_mode]" value="COD" checked class="paymentMethod">
                                        <span>COD - Cash on Delivery</span>
                                    </label>
                                </div>
                                <div class="payment-mode">
									<a href="#" class="pull-right" target="_blank"><i class="fa fa-info-circle"></i> What is RazorPay?</a>
                                    <label class="custom-radio-btn">
                                        <input type="radio" name="order[payment_mode]" value="RazorPay" class="paymentMethod">
                                        <span>RPay - RazorPay</span>
                                    </label>
                                    <input type="hidden" name="order[txn_id]" value="" id="paymentID">
                                </div>
                                <div class="checkout-terms">
                                    <label class="custom-checkbox">
                                        <span><input type="checkbox" name="checkbox" checked ></span>
                                        <span>I’ve read and accept the terms &amp; conditions *</span>
                                    </label>
                                </div>
                                <div class="checkout-button">
                                    <button type="submit" class="button-default btn-large btn-primary-gold">PROCEED TO PAYMENT</button>
                                </div>
                            </div> -->
                            <div class="shop-checkout-right">
                            <div class="shop-checkout-box">
                                <h5>YOUR ORDER</h5>
                                <div class="shop-checkout-title">
                                    <h6>PRODUCT <span>TOTAL</span></h6>
                                </div>
                                
                                <div class="shop-checkout-row">
                                    @foreach( $cartProducts as $p )
                                    <p><span>{{ $p->product->title }}</span> <small style="width: 50px; text-align: right;">₹{{ $p->sale_price * $p->qty }}</small> <small style="margin-right: 50px;">x{{ $p->qty }}</small></p>
                                    @endforeach
                                </div>
                                <div class="checkout-total">
                                    <h6>CART SUBTOTAL <small>₹{{ $totalPrice }}</small></h6>
                                </div>
                                <div class="checkout-total">
                                    <h6>SHIPPING <small>{{ !empty($setting->shipping_charge) ? '₹'.number_format( $setting->shipping_charge, 2 ) : 'Free Shipping' }}</small></h6>
                                </div>
                                @php 
                                    $coupan = session()->get('coupon');
                                    

                                    $gstAmount = $totalPrice * ($setting->gst / 100);
                                @endphp
                                @if(Auth()->user())
                                    @if(!empty($coupan))
                                        @if($coupan->discount_type == 'Flat')
                                            @php
                                                $Discount = $coupan->discount;
                                            @endphp
                                        @else
                                            @php
                                                $Discount = $totalPrice * ($coupan->discount / 100);
                                                if($coupan->upto != ''){
                                                    if($Discount > $coupan->upto){
                                                        $Discount = $coupan->upto;
                                                    }
                                                }
                                            @endphp
                                        @endif
                                    <div class="checkout-total">
                                        <h6>DISCOUNT <small>{{ !empty($Discount) ? '₹'.number_format( $Discount, 2 ) : '₹'.number_format( 0 ) }}</small></h6>
                                    </div>
                                    @endif
                                    
                                @endif
                                @php
                                if(!empty($Discount)){
                                    $totalPrice = $totalPrice - $Discount;
                                }
                                
                                if(!empty($setting->shipping_charge)){
                                    $totalPrice = $totalPrice + $setting->shipping_charge;
                                }
                                $totalAmt = $totalPrice + $gstAmount;
                                $user = auth()->user();
                                @endphp
                                <div class="checkout-total">
                                        <h6>GST <small>{{ !empty($gstAmount) ? '₹'.number_format( $gstAmount, 2 ) : '₹'.number_format( 0 ) }}</small></h6>
                                    </div>
                                <div class="checkout-total">
                                    <h6>ORDER TOTAL <small class="price-big">₹{{ number_format($totalAmt, 2) }}</small></h6>
                                </div>
                                <input type="hidden" name="order[total_amount]" value="{{ $totalAmt }}">
                            </div>
                            <div class="shop-checkout-box">
                                <h5>PAYMENT METHODS</h5>
                                <!-- <div class="payment-mode form-group">
                                    <input type="checkbox" name="order[delivery_type]" value="Take Away" class="m-0" onclick="$('.details').slideToggle(function(){$('#show').html($('.shipping-details').is(':visible'));});" style="border-color:red; margin-top: -3px;">
                                    <span>&nbsp;Take away</span>
                                  </div>
                                  <div class="row details" style="display: none;">
                                    <div class="col-sm-6">
                                        <input type="text" min="<?php echo date('m/d/Y') ?>" name="order[schedule_date]" placeholder="Pick a Date" class="date-pick">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="order[schedule_time]" class="select-dropbox">
                                            <option value="">Select Time</option>
                                            @for($hr = $setting->opening_hour; $hr <= $setting->closing_hour; $hr++)
                                                @php
                                                    $min = $hr == $setting->opening_hour ? $setting->opening_minute : 0;
                                                    $max = $hr == $setting->closing_hour ? $setting->closing_minute : 45;
                                                    $hour = $hr > 12 ? $hr - 12 : $hr;
                                                    $apm  = $hr > 12 ? "PM" : "AM";
                                                @endphp
                                                @while($min <= $max)
                                                    <option value="{{ sprintf("%02d:%02d %s", $hour, $min, $apm) }}">{{ sprintf("%02d:%02d %s", $hour, $min, $apm) }}</option>
                                                    @php
                                                        $min += 15;
                                                    @endphp
                                                @endwhile
                                            @endfor
                                        </select>
                                    </div>
                                  </div> -->
                                </div>
                                <div class="payment-mode">
                                    <label class="custom-radio-btn">
                                        <input type="radio" name="order[payment_mode]" value="COD" checked class="paymentMethod">
                                        <span>COD - Cash on Delivery</span>
                                    </label>
                                </div>
                                <div class="payment-mode">
                                    <a href="#" class="pull-right" target="_blank"><i class="icon-info-circled-2"></i> What is RazorPay?</a>
                                    <label class="custom-radio-btn">
                                        <input type="radio" name="order[payment_mode]" value="RazorPay" class="paymentMethod">
                                        <span>RPay - RazorPay</span>
                                    </label>
                                    <input type="hidden" name="order[txn_id]" value="" id="paymentID">
                                </div>
                                <div class="checkout-terms">
                                    <label class="custom-checkbox">
                                        <input type="checkbox" name="checkbox" checked>
                                        <span>I’ve read and accept the terms &amp; conditions *</span>
                                    </label>
                                </div>
                                <div class="checkout-button">
                                    <button type="submit" class="button-default btn-large btn-primary-gold">PROCEED TO PAYMENT</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		{{ Form::close() }}
		@else
			<div>
				Cart is empty.
			</div>
		@endif
   


<script>
    $('#rzp-footer-form').submit(function (e) {
        var button = $(this).find('button');
        var parent = $(this);
        button.attr('disabled', 'true').html('Please Wait...');
        $.ajax({
            method: 'get',
            url: this.action,
            data: $(this).serialize(),
            complete: function (r) {
                console.log('complete');
                //console.log(r);
            }
        })
        return false;
    })
</script>

<script>
    function padStart(str) {
        return ('0' + str).slice(-2)
    }

    function demoSuccessHandler(transaction) {
        let form = $('#checkoutForm');
        // You can write success code here. If you want to store some data in database.
        $("#paymentDetail").removeAttr('style');
        if(transaction) {
            $('#paymentID').val(transaction.razorpay_payment_id);
        }

        $.ajax({
            method: 'post',
            url: "{!! route('dopayment') !!}",
            data: form.serialize(),
            //contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                window.location = baseUrl+"checkout/thank-you/?order="+r.id;    
            }
        })
    }

    var options = {
        key: "{{ env('RAZORPAY_KEY') }}",
        amount: {{ !empty( $totalAmt ) ? round($totalAmt * 100) : 0 }},
        name: 'City Food',
        description: 'A Food Restaurant',
        image: '{{ url("imgs/logo.png") }}',
        prefill: {
            "email": "{{ $user->email }}",
            "contact": "{{ $user->mobile }}"
        },
        handler: demoSuccessHandler
    }

    window.r = new Razorpay(options);

    $(document).on('submit', '#checkoutForm', function(e) {
        e.preventDefault();
        is_valid = $(this).is_valid();
        if(is_valid) {
            let payMethod = $('.paymentMethod:checked').val();

            if(payMethod == "RazorPay") {
                r.open();
            } else {
                demoSuccessHandler(false);
            }
        }
    });
</script>
@stop