@extends('frontend.layout.master')

@section('title','')

@section('contant')
<!-- <section style="">
    <ul id="responsive">
      @foreach($category as $item)
      <li class="">
        <div class="marquee-box">
          <a href="#" data-filter=".pf-{{ $item->slug }}">{{  $item->title }}</a>
        </div>
      </li>
      @endforeach
    </ul>
  </section> -->
  <section>
      <div class="menu-tabs">
      <div class="category_background">
        <div class="container">
        <!-- menu Filter ============================================= -->

        <ul id="product-fillter" onclick="myFunction(event)">
          <div id="responsive">
            <a href="#" data-filter="" style="background: transparent;"><li class="" style="display: inline;"><div class="marquee-box click-btn active">All</div></li></a>
          @foreach($category as $c)
            <a href="#" data-filter=".pf-{{ $c->slug }}"><li class="" style="display: inline;"><div class="marquee-box click-btn  ">{{ $c->title }}</div></li></a>
          @endforeach
          </div>
        </ul>
        <!-- #menu-filter end -->
      </div>
    </div>

      <!-- Menu Items
                    ============================================= -->
      <div class="container">
        <!-- Menu Items Masonary Content -->
        <div id="menu-items" style="position: relative;margin-top: 50px;">
          <!-- Menu Item -->
          @foreach($product as $key => $item)
          @if($item->p_count)
          <article class="menu-item-list col-md-6 col-lg-3 col-sm-6 pf-{{$item->category->slug}}" style="position: absolute; left: 0px; top: 100px;">
            <!-- Card -->
          <div class="card">

            <div class="view zoom overlay">
              <img class="img-fluid w-100" src="{{ url('imgs/product/'.$item->image) }}" alt="{{ $item->title }}" style="height: 165px;">
                 <a href="#!">
                <div class="mask">
                  <img class="img-fluid w-100" src="{{ url('imgs/product/'.$item->image) }}" alt="{{ $item->title }}" style="height: 165px;">
                  <div class="mask rgba-black-slight"></div>
                </div>
              </a>
            </div>

            <div class="card-body text-center" style="padding-bottom: 20px">

              <h5>{{ $item->title }}</h5>
              <p class="small text-muted text-uppercase">{{ $item->category ? $item->category->title : '' }}</p>
              
              <hr class="mt-0">
              <div class="row">
                 <h6 class="col-6 float-left">
                     <!-- @php
                     $weight         = $item->weight;
                     $weight_arr     = explode (",", $weight);

                     $unit           = $item->unit;
                     $unit_arr       = explode (",", $unit);

                     $sale_price     = $item->sale_price;
                     $sale_price_arr = explode (",", $sale_price);

                     @endphp -->

                     
                     
                     @php
                     $parentArr = [];
                     
                     @endphp
                    <span class="my_setup">
                          <select class="weight-select select2">
                      @foreach($item->p as $pd) 
                          
                            <option data-price="₹{{ $pd->sale_price }}" value="{{ $pd->id }}">{{ $pd->weight }}</option>

                      @endforeach 
                          </select>

                      <button type="button" class="btn add_cart_btn mr-1 mb-2 mt-4 checkout addToCartBtn" data-url="{{ url('cart/add') }}" data-pid="{{ $item->p[0]->id }}">
                        <i class="fa fa-shopping-cart pr-2 " ></i> Add to cart
                      </button>
                      <span class="saleprice">₹{{ $item->p[0]->sale_price }}</span>
                  </span>
                      
                <a type="button" href="" class="btn btn-light mr-1 mb-2 p-detail" data-toggle="modal" data-target="#product_view{{ $item->slug }}">
                        <i class="fa fa-info-circle pr-2"></i> Details
                      </a>
                  
                  </h6>
                 <!--  <h6 class="mb-3 col-6 float-right" style="line-height: 30px;">
                    <span class="text-danger mr-1" id="demo">{{ Form::select('category_id',$sale_price_arr, '', ['class' => 'weight-select select2']) }}</span>
                      <span class="text-grey" id="{{ 'c_pd_id'.$pd->id}}"><s>₹{{ $item->regular_price }}</s></span>
                  </h6> --> 
              </div>
              
    
             
               
              <!-- <button type="button" class="btn btn-danger px-3 mb-2 material-tooltip-main" data-toggle="tooltip" data-placement="top" title="Add to wishlist">
                <i class="fa fa-heart"></i>
              </button> -->

            </div>

          </div>
<!-- Card -->
          </article>
        
            <div class="modal fade product_view" id="product_view{{ $item->slug }}">
                <div class="modal-dialog md-dt">
                    <div class="modal-content modal-co-t">
                        <div class="modal-header m-d-h-t">
                            <a href="#" data-dismiss="modal" class="class pull-right"><span class="fa fa-times"></span></a>
                            <h3 class="modal-title m-t-t">Product</h3>
                        </div>
                        <div class="modal-body model-body-t">
                            <div class="row">
                                <div class="col-md-12 product_img">
                                    <img src="{{ url('imgs/product/'.$item->image) }}" class="img-responsive">
                                </div>
                                <div class="col-md-12 product_content">
                                    <h4>{{ $item->title }}</h4>
                                    <!--<div class="rating">-->
                                    <!--    <span class="glyphicon glyphicon-star"></span>-->
                                    <!--    <span class="glyphicon glyphicon-star"></span>-->
                                    <!--    <span class="glyphicon glyphicon-star"></span>-->
                                    <!--    <span class="glyphicon glyphicon-star"></span>-->
                                    <!--    <span class="glyphicon glyphicon-star"></span>-->
                                    <!--    (10 reviews)-->
                                    <!--</div>-->
                                    <p>{!! $item->description !!}</p>
                                    <span class="my_setup">
                                        <select class="weight-selects select2 mo-sel-t">
                                            @foreach($item->p as $pd) 
                                                  <!-- ksort($pd, SORT_NUMERIC); -->
                                                
                                                  <option data-price="₹{{ $pd->sale_price }}" value="{{ $pd->id }}">{{ $pd->weight }}</option>

                                            @endforeach 
                                                </select>

                                          <span class="detailprice price-t">₹{{ $item->p[0]->sale_price }}</span>

                                          
                                          <button type="button" class="btn add_cart_btn mr-1 mb-2  detailaddcart checkout buton-add-t addToCartBtn" data-url="{{ url('cart/add') }}" data-pid="{{ $item->p[0]->id }}">
                                            <i class="fa fa-shopping-cart pr-2 " ></i> Add to cart
                                          </button>
                                          
                                        </span>  
                                      
                                    <!-- <h3 class="cost">₹ {{ $item->sale_price }} <small class="pre-cost">₹ {{ $item->regular_price }}</small></h3>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                           
                                        </div>
                                       
                                        
                                        
                                    </div>
                                    <div class="space-ten"></div>
                                    <div class="btn-ground">
                                        <button type="button" class="btn add_cart_btn mr-1 mb-2 checkout addToCartBtn" data-url="{{ url('cart/add') }}" data-pid="{{ $item->id }}">
                                            <i class="fa fa-shopping-cart pr-2 " ></i> Add to cart
                                          </button>
                                        
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @else
            @endif
          @endforeach
           
          <!-- End Menu Item -->
        </div>
        <!-- End Menu Content -->
      </div>
      <!-- #menu end -->
    </div>
    </div>
  </section>
  
  <div id="content">
    <!-- Latest News ============================================= -->
      <section class="latest_news">
        <div class="container">
          <div class="row">
           <!-- Head Title -->
           <div class="head_title text-center">
            <i class="icon-intro"></i>
            <h1>Latest Blog</h1>
            <span class="welcome">Stay up to Date</span>
          </div>
          <!-- End# Head Title -->

          <!-- News Content -->
          <div class="news-content dark">
            
            <!-- blog item-->
            @foreach( $blog as $list )
            <div class="blog-item col-md-4 col-sm-6 col-xs-12 mb-5">
              <figure>
                @if($list->image!='')
                <img class="img-responsive" src="{{ url('imgs/blogs/'.$list->image) }}" alt="{{ $list->title }}" style="height: 350px;">
                @else
                <img class="img-responsive" src="{{ url('imgs/unavailable-image-300x225.jpg') }}" alt="{{ $list->title }}"  style="height: 350px;">
                @endif
                <figcaption class="text-center">
                  <div class="fig_container"> <i class="fa fa-picture-o"></i>
                    <h3><a href="{{url('blog/info/'.$list->slug)}}">{{ $list->title }}</a></h3>
                    <!-- <p>Event</p> -->
                    <div class="fig_content" > <a href="{{url('blog/info/'.$list->slug)}}">{{ $list->excerpt }}</a> </div>
                  </div>
                  <span class="btn btn-gold primary-bg text-white" style="font-size: 13px; border-radius: 8px;">{{ $list->created_at->format('d M Y') }}</span> 
                </figcaption>
              </figure>
            </div>
            @endforeach 
            <!-- End blog item-->
            </div>
            <!-- End News Content -->
          </div>
        </div>
      </section>
      
      <!-- End latest News-->
    </div>
    <script>
    // Add active class to the current button (highlight it)
    var header = document.getElementById("responsive");
    var btns = header.getElementsByClassName("click-btn");
    for (var i = 0; i < btns.length; i++) {
      btns[i].addEventListener("click", function() {
      var current = document.getElementsByClassName("active");
      if (current.length > 1) { 
        current[1].className = current[1].className.replace(" active", '');
      }
      this.className += " active";
      });
    }
    </script>
    <!-- End content !-->
@stop