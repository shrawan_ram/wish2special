@extends('frontend.layout.master')

@section('title','About us')

@section('contant')
  
  <div class="category_background" style="background-attachment: fixed; background-size: cover; color:#fff;">
        <div class="container">

        <div class="banner-content">
            <div class="container" >
                <h1 class="text-center text-white">Security</h1>
                <ol class="breadcrumb text-center">
                  <li><a href="{{url('/')}}">Home</a></li>
                  <li>Security</li>
                </ol>
            </div>
          </div>
      </div>
    </div>
<!-- End header -->
<div id="content">
    <section id="intro01" class="padding-100 intro2_01">
      <div class="container">
         
            <!-- Head Title -->
            <div class="head_title">
              <i class="icon-intro"></i>
                <h1>{{ $security->title }}</h1>
                <span class="welcome">Security</span>
            </div>
            <!-- End# Head Title -->
            <p>{!! $security->description !!}</p>
          </div>
          <!-- End intro center -->
        
    </section>
    <!-- End intro -->
@stop