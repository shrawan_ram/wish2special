@extends('frontend.layout.master')

@section('title','Single Blog')

@section('contant')
<!-- banner ============================================= -->
  
  <div class="category_background" style="background-attachment: fixed; background-size: cover; color:#fff;">
        <div class="container">

        <div class="banner-content">
            <div class="container" >
                <h1 class="text-center text-white">{{ $blog->title }}</h1>
                <ol class="breadcrumb text-center">
                  <li><a href="{{url('/')}}">Home</a></li>
                  <li><a href="{{url('/blog')}}">Blog</a></li>
                  <li>{{ $blog->title }}</li>
                </ol>
            </div>
          </div>
      </div>
    </div>
<!-- End header -->
<!-- Content ============================================= -->
<div id="content">
    <!-- Blog list
    ============================================= -->
    <section class="blog_single text-left padding-100">
      <div class="container">
        <div class="row">
          <!-- Blog main content -->
          <div class="blog-main-content">
            <!-- Left side -->
            <div class="col-md-9 col-lg-9 col-sm-12 col-12">
              <!-- Blog row -->
              <div class="blog_row">
                <!-- Blog img -->
                <figure class="blog-img col-md-12"> <img class="img-responsive" src="{{ url('imgs/blogs/'.$blog->image) }}" alt=""> </figure>
                <!-- End Blog img -->
                <!-- Blog content -->
                <div class="blog-content col-md-12">
                  <div class="float-left">
                    
                  </div>
                  <h2>{{ $blog->title }}</h2>
                                    <!-- Links -->
                  <div class="links">
                    <ul class="float-left">
                      <li><i class="fa fa-calendar"></i> Added on {{ $blog->created_at->format('d M Y') }}</li>
                      <li> <a href="#"><i class="fa fa-user"></i> By Admin</a></li>
                      <li><a href="#"><i class="fa fa-comments"></i> {{ count($comment) }} Comments</a></li>
                    </ul>
                  </div>
                  <!-- Category -->
                  <div class="float-right category"> <i class="fa fa-picture-o"></i> <span>{{ $blog->cat->title }}</span> </div>
                  <!--End Category -->
                  <div class="clearfix"></div>
                  <!-- End links -->
                  <div class="text-content">
                    <p>{!! $blog->description !!}</p>
                  </div>
                </div>
                <!-- End Blog content -->
                <!-- Post Meta -->
                <div class="post-meta">
                  <p>tagged by </p>
                  <ul class="labels">
                    <!-- <li><a href="#"><span class="label label-tagged">Pizza</span> </a></li>
                    <li><a href="#"><span class="label label-tagged">Burger</span></a></li>
                    <li><a href="#"><span class="label label-tagged">Sea Food</span></a></li> -->
                  </ul>
                  <!-- Social share -->
                  <div class="social-share pull-right">
                    <ul>
                      <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ route('bloginfo',$blog->slug) }}" target="_blank" style="color: black;"><i class="fa fa-facebook"></i> Share</a></li>
                      <li><a href="https://twitter.com/home?status={{ route('bloginfo',$blog->slug) }}" target="_blank" style="color: black;"><i class="fa fa-twitter"></i> Tweet</a></li>
                    </ul>
                  </div>
                  <!-- Social share -->
                </div>
                <!-- End Post Meta -->
              </div>
              <!-- End Blog row -->
              <!-- Comments -->
              <div class="comment-blog">
                <h3>Comments [{{ count($comment) }}]</h3>
                <div id="comments">
                @foreach($comment as $c)
                  <div id="comments-list-wrapper" class="comments">
                    <ol id="comments-list">
                      <li id="comment-1" class="comment-x byuser">
                        <div class="the-comment">
                          <div class="comment-author vcard"> <img src="{{ url('imgs/admin.png') }}" class="avatar" alt=""> <span class="fn n"><a>{{ $c->name }}</a></span> </div>
                          <div class="comment-meta"> <span> {{ $c->created_at->format('d M Y h:i A') }}</span> </div>
                          <div class="comment-content">
                            <p> {{ $c->comment }} </p>
                          </div>
                        </div>
                      </li>
                    </ol>
                  </div>
                 @endforeach
                  <div id="respond">
                    <h3 id="reply-title">add a reply<small> <a rel="nofollow" id="cancel-comment-reply-link" href="#" class="cancelled">Cancel reply</a></small> </h3>
                    <!-- Contact form -->
                    <div class="contact-form">
                      {{ Form::open(['url' => route('save_comment'), 'name' => 'form', 'class' => 'form']) }}
                              {{ Form::hidden('blogid', $blog->id) }}
                        <!-- Form Group -->
                        <div class="form-group">
                          <div class="row">
                            <div class="col-md-6 col-sm-6 col-sx-12">
                              <!-- Element -->
                              <div class="element">
                                <input type="text" name="name" class="form-control text" placeholder="Name *">
                              </div>
                              <!-- End Element -->
                            </div>
                            <div class="col-md-6 col-sm-6 col-sx-12">
                              <!-- Element -->
                              <div class="element">
                                <input type="text" name="email" class="form-control text" placeholder="E-mail *">
                              </div>
                              <!-- End Element -->
                            </div>
                          </div>
                        </div>
                        <!-- End form group -->
                        <div class="row">
                          <div class="col-md-12">
                            <!-- Form Group -->
                            <div class="form-group">
                              <!-- Element -->
                              <div class="element">
                                <textarea name="comment" class="text textarea" placeholder="Comment *"></textarea>
                              </div>
                              <!-- End Element -->
                            </div>
                            <!-- End form Group -->
                          </div>
                        </div>
                        <!-- Element -->
                        <div class="element text-center">
                          <button type="submit" id="submit-form" value="Send" class="btn btn-black btn-gold">Submit</button>
                          <div class="loading"></div>
                        </div>
                        <!-- End Element -->
                      {{ Form::close() }}
                              
                    </div>
                    <!-- End contact form -->
                  </div>
                </div>
              </div>
              <!-- End# Comments -->
            </div>
            <!-- End Left side -->
            <!-- side bar -->
            <aside>
              <div class="col-md-3 side-bar">
                {!! Form::open(['url' => url('blog'), 'method' => 'GET']) !!}
                <div class="input-group custom-search-form">
                  <input type="text" class="form-control" name="title" placeholder="Search">
                  <!-- <input type="submit" value="&#xf002;"> -->
                    <span class="input-group-btn">
                    <button class="btn" type="submit"> <i class="fa fa-search"></i> </button>
                    </span>
                </div>
              {!! Form::close() !!}
                <!-- /input-group -->
                <hr>
                <!-- Recent posts -->
                <div class="recent-posts">
                  <h4>Category</h4>
                  <span class="sidebar_divider"></span>
                  <ul>
                    @foreach($category as $c)
                    <li><a href="{{ url('blog', $c->slug) }}">{{ $c->title }}</a></li>
                    @endforeach
                  </ul>
                </div>
                <!-- End Recent posts -->
                <hr>
                <!-- Recent comments -->
                <div class="recent-comments">
                  <h4>Recent Blog</h4>
                  <span class="sidebar_divider"></span>
                  <!-- comments content -->
                  <div class="comments-content blog-img">
                    @foreach($latestblog as $lb)
                    <div class="media">
                      <div class="media-left"> <a href="blog_single_slider.html"> <img class="media-object" src="{{ url('imgs/blogs/'.$lb->image) }}" alt=""> </a> </div>
                      <div class="media-body">
                        <p class="media-heading" ><a href="{{ url('blog/info', $lb->slug) }}">{{ $lb->title }}</a></p>
                        <span style="overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical;">{{ $lb->excerpt }}</span>
                      </div>
                    </div>
                    @endforeach
                  </div>
                  <!--End comments content -->
                </div>
                <!-- End recent comments -->
                <hr>
                <!-- Tags -->
                <!-- <div class="tags">
                  <h4>Tags</h4>
                  <span class="sidebar_divider"></span>
                  <ul>
                    <li><a href="#"><span class="label label-tagged">Pizza</span> </a></li>
                    <li><a href="#"><span class="label label-tagged">Burger</span> </a></li>
                    <li><a href="#"><span class="label label-tagged">Sea Food</span> </a></li>
                    <li><a href="#"><span class="label label-tagged">Steak</span></a></li>
                    <li><a href="#"><span class="label label-tagged">Shawarma</span></a></li>
                    <li><a href="#"><span class="label label-tagged">Red</span> </a></li>
                    <li><a href="#"><span class="label label-tagged">Chef</span></a></li>
                    <li><a href="#"><span class="label label-tagged">French Bread</span></a></li>
                  </ul>
                </div> -->
                <!--End Tags -->
              </div>
            </aside>
            <!-- End side bar -->
          </div>
          <!--End Blog main content -->
        </div>
      </div>
    </section>
    <!-- End blog list -->
  </div>
<!-- end of #content -->
@stop