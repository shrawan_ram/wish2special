@extends('frontend.layout.master')

@section('title', 'Thank You')

@section('contant')

<main>
    <div id="wrapper">
  <!-- banner 
    ============================================= -->
  <div class="category_background" style="background-attachment: fixed; background-size: cover; color:#fff;">
        <div class="container">

        <div class="banner-content">
            <div class="container" >
                <h1 class="text-center text-white">Thank You</h1>
                <ol class="breadcrumb text-center">
                  <li><a href="{{url('/')}}">Home</a></li>
                  <li><a href="{{url('/account')}}">Profile</a></li>
                  <li>My order</li>
                </ol>
            </div>
          </div>
      </div>
    </div>
  
        <section class="home-icon login-register bg-skeen">
            <!--<div class="icon-default icon-skeen">-->
            <!--    <img src="{{url('imgs/scroll-arrow.png')}}" alt="">-->
            <!--</div>-->
            <div class="container text-center" style="margin-top: 50px;">
                <h1>Thank You.</h1>
				<h4>Your order has been submited successfully.</h4>

                            </div>
        </section>
    </div>
</main>

@stop