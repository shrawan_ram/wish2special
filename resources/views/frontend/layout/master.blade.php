@php
session_start();
    ob_start();

	$setting  =  App\model\Setting::findOrFail(1);
	$category =  App\model\Category::get();
    $blog     =  App\model\Blog::latest()->paginate(2);
    $about    =  App\model\About::findOrFail(1);

    $city_id = session()->get('city_session');

    if(empty($city_id)) {
        $city = App\model\City::where('is_default', 1)->first();
     
        session()->put('city_session', $city->id);

      
    }
      
        
@endphp
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from miraclestudio.design/html/majesty/index01.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 19 Nov 2020 05:59:01 GMT -->
<head>
<!-- Meta Tags
    ============================================= -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Shrawan Choudhary">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- Your Title Page
    ============================================= -->
<title>{{ $setting->site_title }}</title>
<base href="{{ url('/') }}">
<!-- Favicon Icons
     ============================================= -->
<link rel="shortcut icon" href="{{url('imgs/'.$setting->favicon)}}">
<!-- Standard iPhone Touch Icon-->
<link rel="apple-touch-icon" sizes="57x57" href="{{url('imgs/'.$setting->favicon)}}">
<!-- Retina iPhone Touch Icon-->
<link rel="apple-touch-icon" sizes="114x114" href="{{url('imgs/'.$setting->favicon)}}">
<!-- Standard iPad Touch Icon-->
<link rel="apple-touch-icon" sizes="72x72" href="{{url('imgs/'.$setting->favicon)}}">
<!-- Retina iPad Touch Icon-->
<link rel="apple-touch-icon" sizes="144x144" href="{{url('imgs/'.$setting->favicon)}}">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.css" integrity="sha512-+1GzNJIJQ0SwHimHEEDQ0jbyQuglxEdmQmKsu8KI7QkMPAnyDrL9TAnVyLPEttcTxlnLVzaQgxv2FpLCLtli0A==" crossorigin="anonymous" />


{{Html::style('css/mdb.min.css')}}
{{Html::style('css/bootstraps.min.css')}}
{{Html::style('css/bootstrap.min.css')}}
{{Html::style('css/owl.carousel.css')}}
{{Html::style('css/owl.theme.css')}}
{{Html::style('css/animate.css')}}
{{Html::style('css/jquery.datetimepicker.css')}}
{{Html::style('css/prettyPhoto.css')}}
{{Html::style('css/font-awesome.min.css')}}
{{Html::style('css/main.css')}}
{{Html::style('css/main1.css')}}
{{Html::style('css/main-responsive1.css')}}
<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Google Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link href="fonts/fonts.css" rel="stylesheet">
<style>
    .my_setup{
        position: relative;
    }
    .weight-select{
        color: #000;
        height:30px;
        padding:0 15px;
        margin-bottom:0;
    }
    #shop_cart.shop_cart_open .shop_cart_content {
    height: 330px;
    overflow: scroll;
    }
    .saleprice{
        position: absolute;
        top: 10%;
        right: -60%;
    }
    .p-detail{
        position: absolute;
        top: 52%;
        right: -85%;
    }
    /*.detailprice{
        font-family: 'Fjalla One', sans-serif;
        font-size: 24px;
        position: absolute;
        top: 175%;
        left: 10%;
    }*/
    /*.detailaddcart{
        position: absolute;
        top: 250%;
        left: 7%;
    }*/
</style>
</head>
<body style="overflow-x: hidden;">
<!-- Loader
    ============================================= -->
 <div id="loader" style="background:#f2f2f2;">
  <div class="loader-item"> <img src="{{url('imgs/'.$setting->logo)}}" alt="">
    <div class="spinner">
      <div class="bounce1"></div>
      <div class="bounce2"></div>
      <div class="bounce3"></div>
    </div>
  </div>
</div> 
<!-- End Loader -->
<!-- Document Wrapper
    ============================================= -->
<div id="wrapper">
    @include('frontend.template.home-header')

	@show
	@yield('contant')
	@section('footer')

	<!-- Footer ============================================= -->
  <footer id="footer" class="padding-50 dark">
    <div class="container">
      <div class="row">
        <!-- Latest Post !-->
        <div class="col-md-4 col-sm-4 col-xs-12 latest_post ">
          <h3>About</h3>
          <p>{!! $setting->about !!}</p>
        </div>
        <!-- End latest Post -->
        <!-- Quick Link !-->
        <div class="col-md-3 col-6 col-sm-6 col-xs-6 opening_time mb-4">
          <h3>Quick Link</h3>
          <ul>
            <li><p><a href="{{ url('/') }}">Home</a></p></li>            
            <li><p><a href="{{ url('/menu') }}">Menu</a></p></li>            
            <li><p><a href="{{ url('/blog') }}">Blog</a></p></li>
            <li><p><a href="{{ url('/about-us') }}">About Us</a></p></li>
            <li><p><a href="{{ url('/contact') }}">Contact Us</a></p></li>
          </ul>
        </div>
        <div class="col-md-2 col-6 col-sm-6 col-xs-6 opening_time mb-4">
          <h3>Terms</h3>
          <ul>
            <li><p><a href="{{ url('/privacy') }}">Privacy</a></p></li>
            <li><p><a href="{{ url('/term') }}">Term</a></p></li>
            <li><p><a href="{{ url('/security') }}">Security</a></p></li>
            <!--<li><p><a href="{{ url('/') }}">Sitemap</a></p></li>-->
          </ul>
        </div>
        <!-- End Quick Link -->
        <!-- Our location !-->
        <div class="col-md-3 col-sm-6 col-xs-12 our_location">
          <h3>Our Location</h3>
          <p><i class="fa fa-map-marker"></i> :
          <span>{{ $setting->address }}</span></p>
          <p class="mt15"><i class="fa fa-phone"></i> :<span  > {{ $setting->mobile }}</span></p>
          <p><i class="fa fa-envelope"></i> : <span>{{ $setting->email }}</span> </p>
          <ul class="social mt30">
            @if($setting->facebook_url!='')
            <li><a href="{{$setting->facebook_url}}" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i></a></li>
            @else
            @endif
            @if($setting->insta_url!='')
            <li><a href="{{$setting->insta_url}}" data-toggle="tooltip" title="instagram"><i class="fa fa-instagram"></i></a></li>
            @else
            @endif
            @if($setting->twitter_url!='')
            <li><a href="{{$setting->twitter_url}}" data-toggle="tooltip" title="twitter"><i class="fa fa-twitter"></i></a></li>
            @else
            @endif
            @if($setting->pinterest_url!='')
            <li><a href="{{$setting->pinterest_url}}" data-toggle="tooltip" title="pinterest"><i class="fa fa-pinterest"></i></a></li>
            @else
            @endif
            @if($setting->youtube_url!='')
            <li><a href="{{$setting->youtube_url}}" data-toggle="tooltip" title="youtube"><i class="fa fa-youtube"></i></a></li>
            @else
            @endif
          </ul>
        </div>
        <!-- End our location -->
      </div>
    </div>
    <!-- End container -->
    <!-- Footer logo !-->
    <div class="footer_logo text-center"> 
      <!-- <img  src="{{url('imgs/'.$setting->logo)}}"  alt="logo"> -->
      <p> 2020 ALL RIGHT RESERVED. DESIGNED BY <a target="_blank" href="https://www.suncitygroup.org/">SUNCITY GROUP</a></p>
    </div>
    <!-- End Footer logo !-->

  </footer>
 
  
  <!-- start login modal -->

        <div class="modal fade user_login" id="user_login" tabindex="-1" role="dialog" aria-labelledby="user_login">
            <div class="modal-dialog" style="margin-top: 100px;" role="document">
                <div class="modal-content mode-lo-t">
                    <div class="modal-body modal-b-t">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="auth-model active" id="userLoginModel">
                            <h2 class="text-coffee text-center">Login</h2>
                            @if (\Session::has('success'))
                                <div class="alert alert-success toast-msg" style="color: green">
                                    {!! \Session::get('success') !!}</li>
                                </div>
                            @endif

                            @if (\Session::has('danger'))
                                <div class="alert alert-danger toast-msg" style="color: red;">
                                    {!! \Session::get('danger') !!}</li>
                                </div>
                            @endif
                            <p id="registere_error"></p>
                            {{ Form::open(['url' => url('user_login'), 'method'=>'POST', 'id'=>'user_login', 'class'=>'user_login']) }}
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="tel" name="login" placeholder="Mobile No. (10 Digits)" class="input-fields" id="user_number" autocomplete="username">
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="password" name="password" placeholder="Password" class="input-fields" id="user_password" autocomplete="new-password" style="margin-bottom: 0;">
                                        <!-- <span id="user_password_msg">Error!! Password Not Match.</span> -->
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 mt-5">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12" style="padding-left: 18px;">
                                                <label class="custom-checkbox che-cu-t">
                                                    <input type="checkbox" name="remember" value="1" id="shipDifferentAddress">
                                                    <span>Remember me</span>
                                                </label>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12" style="padding-right: 18px;">
                                                <a href="javascript:toggleModel('#userForgotpassModel')" class="pull-right pas-t">Forgot Password?</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="submit" name="submit" value="LOGIN" class="button-default button-default-submit login_popup_btn">
                                    </div>
                                    <div class="col-xs-12 text-center mb-5">
                                        If you don't have account, then 
                                        <a href="javascript:toggleModel('#userRegisterModel')" class="">Register Here</a>
                                    </div>
                                    
                                    <!-- <div class="col-md-6 col-xs-12 mb-5 offset-md-4 text-center button mt-5">
                                         <a href="{{ url('/login/facebook') }}" class="btn btn-primary facebook"><i class="fa fa-facebook" aria-hidden="true"></i>&nbsp; Continue with Facebook</a>
                                    </div>
                                    <div class="col-md-6 col-xs-12 offset-md-4 text-center button mt-5">
                                         <a href="{{ url('/login/google') }}" class="btn btn-danger google"><i class="fa fa-google"></i>&nbsp; Continue with Google</a>
                                    </div> -->
                                </div>
                                {{ Form::close() }}
                        </div>

                        <div class="auth-model" id="userRegisterModel">
                            <h2 class="text-coffee text-center">Register</h2>
                            @if (\Session::has('success'))
                                <div class="alert alert-success toast-msg" style="color: green">
                                    {!! \Session::get('success') !!}</li>
                                </div>
                            @endif

                            @if (\Session::has('danger'))
                                <div class="alert alert-danger toast-msg" style="color: red;">
                                    {!! \Session::get('danger') !!}</li>
                                </div>
                            @endif

                            {{ Form::open(['url' => url('user_register'), 'method'=>'POST', 'id'=>'registerForm', 'class'=>'user_register register-form']) }}
                                @csrf
                                <div class="row">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <!-- <input type="text" name="fname" placeholder="First Name" class="input-fields" required> -->
                                        {{Form::text('fname', '', ['class' => 'form-control, input-fields', 'placeholder'=>'First Name','required'=>'required', 'id'=>'fname'])}}
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <!-- <input type="text" name="lname" placeholder="Last Name" class="input-fields"> -->
                                        {{Form::text('lname', '', ['class' => 'form-control, input-fields', 'placeholder'=>'Last Name', 'id'=>'lname'])}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <!-- <input type="email" name="email" placeholder="Enter Email" class="input-fields"> -->
                                        {{Form::text('email', '', ['class' => 'form-control, input-fields', 'placeholder'=>'Enter Email', 'id'=>'email'])}}
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <!-- <input type="tel" name="mobile" placeholder="Enter Mobile No." class="input-fields mobile" required> -->
                                        {{Form::tel('mobile', '', ['class' => 'form-control, input-fields', 'placeholder'=>'Enter Mobile No.','required'=>'required', 'id'=>'mobile'])}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <input type="password" name="password" id="password" placeholder="Password" class="input-fields password" autocomplete="new-password" required>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <input type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password" class="input-fields confirm-password" required>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="submit" name="submit" class="button-default button-default-submit" id="user_login_sub" value="RegIster now">
                                    </div>
                                </div>
                           
                                <p class="text-center mb-5">By clicking on “Register Now” button you are accepting the <a href="{{ route('terms') }}">Terms &amp; Conditions</a></p>
                                <p class="text-center mb-5">Already have an account? Then 

                                        <a href="javascript:toggleModel('#userLoginModel')">Login Here</a>

                                   <!--  <div class="col-md-6 offset-md-4 text-center button mt-5 mb-5">
                                         <a href="{{ url('/login/facebook') }}" class="btn btn-primary facebook"><i class="fa fa-facebook" aria-hidden="true"></i>&nbsp; Continue with Facebook</a>
                                    </div>
                                    <div class="col-md-6 offset-md-4 text-center button mt-5">
                                         <a href="{{ url('/login/google') }}" class="btn btn-danger google"><i class="fa fa-google"></i>&nbsp; Continue with Google</a>
                                    </div> -->
                                    <!-- <a href="javascript:toggleModel('#userVerifyotpModel')">hello</a> -->
                                </div>
                            {{ Form::close() }}
                        


                        <div class="auth-model" id="userVerifyotpModel">
                            <h2 class="text-coffee text-center">Verify OTP</h2>
                            @if (\Session::has('success'))
                                <div class="alert alert-success toast-msg" style="color: green">
                                    {!! \Session::get('success') !!}</li>
                                </div>
                            @endif

                            @if (\Session::has('danger'))
                                <div class="alert alert-danger toast-msg" style="color: red;">
                                    {!! \Session::get('danger') !!}</li>
                                </div>
                            @endif
                            <!-- <form class="login-form" method="post" name="login"> -->
                            {{ Form::open(['url' => url('user_verifyotp'), 'method'=>'POST', 'id'=>'user_verifyotp', 'class'=>'user_verifyotp register-form user_login']) }}
                                @csrf
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="tel" name="otp" placeholder="Enter OTP" class="input-fields otp-match" required>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="submit" name="submit" value="VERIFY" class="button-default button-default-submit">
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <label>
                                                    Didn't recieved OTP? then <a id="login_resendotp">Re-send OTP</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {{ Form::close() }}
                        </div>

                        <div class="auth-model" id="userRecoverpassModel">
                            <h2 class="text-coffee text-center">Recover Password</h2>
                            {{ Form::open(['url' => url('user_recoverpass'), 'method'=>'POST', 'id'=>'user_recoverpass', 'class'=>'user_recoverpass register-form user_login']) }}
                                    @if (\Session::has('success'))
                                        <div class="alert alert-success toast-msg" style="color: green">
                                            {!! \Session::get('success') !!}</li>
                                        </div>
                                    @endif
                                    @if (\Session::has('danger'))
                                        <div class="alert alert-danger toast-msg" style="color: red;">
                                            {!! \Session::get('danger') !!}</li>
                                        </div>
                                    @endif
                                    @if($message = Session::get('error'))
                                       <div class="alert alert-danger alert-block">
                                         <button type="button" class="close" data-dismiss="alert">x</button>
                                         {{$message}}
                                       </div>
                                    @endif
                                  @if(count($errors->all()))
                                    <div class="alert alert-danger">
                                      <ul>
                                        @foreach($errors->all() as $error)
                                          <li>{{$error}}</li>
                                        @endforeach
                                      </ul>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p style="color: red;" id="new_password_msg"></p>
                                        {{ Form::label('new_password') }}
                                        {{ Form::password('new_password', ['placeholder' => 'New Password', 'required' => 'required', 'id' => 'new_password', 'autocomplete' => 'new-password']) }}
                                    </div>
                                    <div class="col-xs-12">
                                        {{ Form::label('confirm_password') }}
                                        {{ Form::password('confirm_password', ['placeholder' => 'Confirm Password', 'required' => 'required', 'id' => 'confirm_password', 'class' => 'pass-match', 'autocomplete' => 'new-password']) }}
                                    </div>
                                    <div class="col-xs-12">
                                        {{ Form::submit('Recover Password', ['class' => 'btn-main']) }}
                                    </div>
                                </div>
                                {{ Form::close() }}
                        </div>

                        <div class="auth-model" id="userVerifyotpforgotModel">
                            <h2 class="text-coffee text-center">Verify OTP</h2>
                            @if (\Session::has('success'))
                                <div class="alert alert-success toast-msg" style="color: green">
                                    {!! \Session::get('success') !!}</li>
                                </div>
                            @endif

                            @if (\Session::has('danger'))
                                <div class="alert alert-danger toast-msg" style="color: red;">
                                    {!! \Session::get('danger') !!}</li>
                                </div>
                            @endif
                            <!-- <form class="login-form" method="post" name="login"> -->
                            {{ Form::open(['url' => url('user_verifyforgototp'), 'method'=>'POST', 'id'=>'user_verifyforgototp', 'class'=>'user_verifyforgototp register-form user_login']) }}
                                @csrf
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="tel" name="otp" placeholder="Enter OTP" class="input-fields otp-match" required>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="submit" name="submit" value="VERIFY" class="button-default button-default-submit">
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <label>
                                                    Didn't recieved OTP? then <a id="login_resendotp">Re-send OTP</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {{ Form::close() }}
                        </div>

                        <div class="auth-model" id="userForgotpassModel">
                            <h2 class="text-coffee text-center">Forgot Password</h2>
                            @if (\Session::has('success'))
                                <div class="alert alert-success toast-msg" style="color: green">
                                    {!! \Session::get('success') !!}</li>
                                </div>
                            @endif

                            @if (\Session::has('danger'))
                                <div class="alert alert-danger toast-msg" style="color: red;">
                                    {!! \Session::get('danger') !!}</li>
                                </div>
                            @endif
                            {{ Form::open(['url' => url('user_forgotpass'), 'method'=>'POST', 'id'=>'user_forgotpass', 'class'=>'user_forgotpass register-form']) }}
                            
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="tel" name="mobile" placeholder="Enter Mobile Number" class="input-fields mobile-match" id="resrv_mobile" required>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="submit" name="submit" value="Submit" class="button-default button-default-submit">
                                    </div>
                                </div>
                            {{ Form::close() }}
                        </div>



                    </div>
                    </div>
                    
                </div>
            </div>
        </div>

        <!-- End login modal -->
  <!-- End footer -->
  <!--  scroll to top of the page-->
   </div>
   <div id="notification">
   <div class="modal fade" id="ignismyModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="border:0;">
                        <button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
                     </div>
          
                    <div class="modal-body text-center">
                       
                    <div class="thank-you-pop">
                      <img src="http://goactionstations.co.uk/wp-content/uploads/2017/03/Green-Round-Tick.png" alt="">
                      <h1>Thank You!</h1>
                      <p>Your submission is received and we will contact you soon</p>
                      <!-- <h3 class="cupon-pop">Your Id: <span>12345</span></h3> -->
                      
                    </div>
                         
                    </div>
          
                </div>
            </div>
        </div>
    </div>
<!-- End wrapper -->
<!-- Core JS Libraries -->

{{Html::script('js/libs/jquery-3.5.1.min.js')}}
{{Html::script('js/libs/plugins.js')}}
{{Html::script('js/libs/modernizr.js')}}
{{Html::script('js/custom/main.js')}}
{{Html::script('js/custom-script.js')}}
{{Html::script('js/ajax.js')}}
{{Html::script('js/custom/mbBgGallery_init.js')}}

<!-- For This Pgae Only Zooming slider -->
<script src="js/custom/mbBgGallery_init.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.js" integrity="sha512-sww7U197vVXpRSffZdqfpqDU2SNoFvINLX4mXt1D6ZecxkhwcHmLj3QcL2cJ/aCxrTkUcaAa6EGmPK3Nfitygw==" crossorigin="anonymous"></script>
<script type="text/javascript">
    
    setTimeout(function() { $("#notification").hide("#ignismyModal"); }, 3000);
</script>
<script>
  $(document).ready(function() {
    $('#responsive').lightSlider({
        item:5,
        loop:false,
        nav:false,
        slideMove:2,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:600,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:6,
                  }
            },
            {
                breakpoint:480,
                settings: {
                    item:2,
                    slideMove:1
                  }
            }
        ]
    });  
  });
  </script>
  <script>
    $(function() {
        $(document).on('change', 'select.weight-select', function () {
            var pid = $(this).val();
            var value = $(this).find('option:selected').attr('data-price');
            $(this).parent('span').children('span.saleprice').html(value);
            $(this).closest('.my_setup').find('button.add_cart_btn').attr('data-pid', pid);
        });
        $(document).on('change', 'select.weight-selects', function () {
            var pid = $(this).val();
            // var selectedCountry = s.val();
            var value = $(this).find('option:selected').attr('data-price');
            $(this).parent('span').children('span.detailprice').html(value);
            // $(this).parent('span').append('<span class="saleprice">'+value+'</span>');
            $(this).closest('.my_setup').find('button.add_cart_btn').attr('data-pid', pid);
        });
        
    });
  
    </script>
    <script type="text/javascript">
        
    $( "#city" ).change(function()   {

        window.location = "{{ url('change-city') }}" + '/' + $(this).val();
        
    });
</script>
  

  <!-- <script>
      $('#responsive li').click(function() {
          $('.abcd').removeClass('abcd');
          $(this).parent().addClass('active');
        });
  </script> -->
</body>

<!-- Mirrored from miraclestudio.design/html/majesty/index01.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 19 Nov 2020 05:59:46 GMT -->
</html>