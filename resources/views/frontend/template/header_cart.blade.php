@php
    $cart = session()->get('cart');

    $cartProducts = [];
    $totalPrice   = 0;
    if(!empty($cart)) {
        foreach($cart as $pid => $pData) {
            
            $productInfo = \App\model\Productdetail::find( $pid );
            if(!empty($productInfo->id)) {
                $productInfo->qty   = $pData['qty'];
                $cartProducts[]     = $productInfo;
                $totalPrice        += $productInfo->sale_price * $pData['qty'];
            }
        }
    }

@endphp

@if( !empty($cartProducts) )
<a href="{{url('cart')}}" class="header_count header_cart_icon" style="height: 25px;
    padding-left: -14px;
    padding: 0px;
    padding-left: -10px;
    position: absolute;
    top: 25%;
    left: 64%;"><p style="    height: 9px;
    padding-left: 23px;
    /* padding: 0px; */
    position: absolute;

    left: -51%;">{{ count($cartProducts) }}</p></a>
<div id="shop_cart" class="shop_cart_open" style="position: absolute;">
            <div class="shop_cart_content">
              <h4>Shopping Cart</h4>
              <div class="cart_items">
                
                 @foreach($cartProducts as $cp )
                
                <div class=" item row" style="margin: 0">
                    <div class="col-lg-4 col-sm-4 col-4"><a href="#"><img src="{{ url('imgs/product/'.$cp->product->image) }}" alt="" width="100%"></a></div>
                    <div class="col-lg-2 col-sm-2 col-2"><h5 style="color: #fff;">{{ $cp->weight ? $cp->weight : '-' }}</h5></div>
                    <div class="col-lg-6 col-sm-6 col-6" style="line-height: 0;">
                       <h5 style="color: #fff;">{{ $cp->product->title ? $cp->product->title : '-' }}</h5>
                        <h6 style="color: #fff;">₹{{ number_format($cp->sale_price) }} x {{ $cp->qty }}</h6>

                    </div>
                </div>
                @endforeach
                <!-- End item -->
                
                <!-- End item -->
                <div class="shop_action"> 
                    <span class="shop_checkout_price" style="float: left;">₹{{ number_format($totalPrice, 2) }}</span>
                    <span class="cart-btn float-right" style="float: right;">
                        <a href="{{url('cart')}}" class="btn-black view">VIEW ALL</a>
                       <!--  @guest()
                        <a href="javascript:user_login()" class="btn-main checkout">CHECK OUT</a>
                        @else
                            <a href="{{ url('checkout') }}" class="btn-main checkout">CHECK OUT</a>
                        @endif -->
                    </span>
                </div>
              </div>
              <!-- End cart items -->
            </div>
            <!-- End shop cart content -->
          </div>
@endif
