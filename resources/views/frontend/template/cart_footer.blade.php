@if(!empty($cartProducts))
 @if(Auth()->user())
<div class="cart-total wow fadeInDown" style="float: left; max-width: 630px;" data-wow-duration="1000ms" data-wow-delay="300ms">
   
        <?php 
            $coupan = session()->get('coupon');
            
        ?>
        @if(empty($coupan))
            <div class="cupon-part">
                <input type="text" id="coupon_code" placeholder="Coupon Code" style="margin-top: 25px;">
            </div>
            <button type="button" class="btn  btn-black btn-medium btn-skin apply_coupon" data-url="{{ url('ajax/check-coupon') }}">Apply Coupon</button>
        @endif
        <span id="massege"></span>
        
</div>
    @endif
<div class="cart-total wow fadeInDown mb-5" id="cartFooter" data-wow-duration="1000ms" data-wow-delay="300ms">
    <div class="cart-total-title">
        <h5>CART TOTALS</h5>
    </div>
    <div class="product-cart-total">
        <small>Subtotal</small>
        <span>₹ {{ number_format($totalPrice, 2) }}</span>
    </div>
    <?php 
        $coupan = session()->get('coupon');
        
    ?>
    @if(Auth()->user())
        @if(!empty($coupan))
            @if($coupan->discount_type == 'Flat')
                <?php
                    $Discount = $coupan->discount;
                ?>
            @else
                <?php
                    $Discount = $totalPrice * ($coupan->discount / 100);
                    if($coupan->upto != ''){
                        if($Discount > $coupan->upto){
                            $Discount = $coupan->upto;
                        }
                    }
                ?>
            @endif
        <div class="product-cart-total">
            <small>Discount</small>
            <span>{{ !empty($Discount) ? '₹'.number_format( $Discount, 2 ) : '₹'.number_format( $Discount, 2 ) }}</span>
            <br>
            <small style="color: green;">"{{ $coupan->code }}" Applied!</small>
            <span class="remove-coupan del-coupan" data-url="{{ url('ajax/remove-coupon') }}">Remove <i class="fa fa-remove"></i></span>
        </div>
        @endif
        
    @endif
    <div class="product-cart-total">
        <small>Total shipping</small>
        <span>{{ !empty($setting->shipping_charge) ? '₹'.number_format( $setting->shipping_charge, 2 ) : 'FREE' }}</span>
    </div>
    
    <div class="grand-total">
        @php
            if(!empty($Discount)){
                $totalPrice = $totalPrice - $Discount;
            }
            
        @endphp
        <h5>TOTAL <span>₹ {{ !empty($setting->shipping_charge) ? number_format( $totalPrice + $setting->shipping_charge, 2 ) : number_format( $totalPrice, 2 ) }}</span></h5>
    </div>
    <div class="proceed-check checkout-button">
        @guest()
            <a href="javascript:user_login()" class="button-default btn-large btn-primary-gold">PROCEED TO CHECKOUT</a>
        @else
            <a href="{{ url('checkout') }}" class="button-default btn-large btn-primary-gold" style="margin:auto;">PROCEED TO CHECKOUT</a>
        @endif

        @guest()
            <a href="{{ url('guest') }}" class="button-default btn-large btn-primary-gold">Continue as Guest</a>
        @else
            <a href=""></a>
        @endif
        
    </div>
</div>
@endif
