<header id="header" class="header-transparent">
    <div class="container">
      <div class="row">
        <div id="main-menu-trigger"><i class="fa fa-bars"></i></div>
        <!-- Logo
                    ============================================= -->
        <div id="logo"> <a href="{{ url('/') }}" class="light-logo"><img src="{{url('imgs/'.$setting->logo)}}" alt="Logo" style="width: 65%;"></a> <a href="{{ url('/') }}" class="dark-logo"><img src="{{url('imgs/'.$setting->logo)}}" alt="Logo" style="width: 65%; height: 44px;"></a> </div>
        <!-- #logo end -->
        <!-- Primary Navigation
                    ============================================= -->
        <nav id="main-menu" class="dark ml-auto">
          <ul>
            <li><a href="{{ url('/') }}">
              <div>Home</div>
              </a>
            </li>
            <li><a href="{{ url('/menu') }}">
              <div>Menu</div>
              </a>
            </li>
            <li><a href="{{ url('/blog') }}">
              <div>Blog</div>
              </a>
            </li>
            <li><a href="{{ url('/about-us') }}">
              <div>About Us</div>
              </a>
            </li>
            <li><a href="{{ url('/contact') }}">
              <div>Contact Us</div>
              </a>
            </li>
            <li>
              <div class="cart-icon cart_header_icon text-center home_header_cart">
                <a href="{{ url('/cart') }}" style="color: black;" class="">
                  <i class="fa fa-cart-plus "></i>
                  </a> 
                  <div class="shop-cart header-collect">
                      @include('frontend.template.header_cart')
                  </div>
                

                            
                        
              </div>
            </li>
          </ul>
          
          
        </nav>
        <!-- #main-menu end -->
      </div>
    </div>
  </header>