@if(!empty($cartProducts))
<section class="carts text-center padding-100">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <!-- Table carts -->
            <table class="table table-striped table-responsive table-cart">
              <thead>
                <tr>
                  <th style="width:40%">Product Image</th>
                  <th style="width:15%">Weight</th>
                  <th style="width:15%">Price</th>
                  <th style="width:20%">Quantity</th>
                  <th style="width:25%">Total</th>
                  <th style="width:25%"></th>
                </tr>
              </thead>
              <tbody>

                @foreach($cartProducts as $cp)

                <tr>
                  <td><a href="{{ url('/menu/'.$cp->slug)}}"><img src="{{url('imgs/product/'.$cp->product->image)}}" alt=""> {{ $cp->product->title }}</a> </td>
                  <td>{{ $cp->weight }}</td>
                  <td>
                    <strong>₹ {{ $cp->sale_price }}</strong>
                    <del>₹ {{ $cp->regular_price }}</del>
                  </td>
                  <td><!-- input group minus & plus-->
                    <div class="input-group plus-minus price-textbox">
                        <span class="input-group-btn">
                            <button type="button" class="btn minus-text" style="margin: 0;"> 
                                <span class="fa fa-minus"></span> 
                            </button>
                        </span>
                        <input type="text" name="qty[{{ $cp->id }}]" class="form-control input-number btn-number" style="height: 33px;" value="{{ $cp->qty }}">
                        <span class="input-group-btn">
                            <button type="button" class="btn plus-text">
                                <span class="fa fa-plus"></span>
                            </button>
                        </span>
                    </div>
                    
                  </td>
                    <!-- End input group minus & plus --></td>
                  <td><span class="total">₹ {{ $cp->sale_price * $cp->qty }}</span> </td>
                  <td><span class="delete-icon cart-remove-icon" class="pull-right" data-pid="{{ $cp->id }}" data-url="{{ url('cart/remove') }}"><i class="fa fa-trash"></i></span></td>
                </tr>
                @endforeach
              </tbody>
            </table>
            <!-- End Table carts  -->
          </div>
          <div class="product-cart-detail">
              <button type="button" class="btn  btn-black btn-medium btn-skin pull-right updateCartBtn">UPDATE CART</button>
          </div>
        </div>
      </div>
    </section>
@else
 
 Cart Is Empty
 @endif