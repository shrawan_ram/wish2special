<div class="hide-tag col-md-4 col-sm-4 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
	<div class="blog-left-section">
		<div class="blog-left-search blog-common-wide">
			{!! Form::open(['method' => 'GET']) !!}
				<input type="text" name="title" placeholder="Search">
				<input type="submit" value="&#xf002;">
			{!! Form::close() !!}
		</div>
		<div class="mob_bottom">
			<div class="blog-left-categories blog-common-wide">
				<h5>Categories</h5>
				<ul class="list">
					@foreach($category1 as $list)
					<li><a href="{{url('blog/'.$list->slug)}}">{{ $list->title }}</a></li>
					@endforeach
				</ul>
			</div>
			<div class="blog-recent-post blog-common-wide">
			<h5>Recent Posts</h5>
				@foreach($latestblog as $list)
					<div class="recent-blog-list">
						@if( !empty($list->image) )
						<p>
							<a href="{{url('blog/info/'.$list->slug)}}"><img style="width: 300px;" src="{{ url('imgs/blogs/'.$list->image) }}" alt=""></a>
						</p>
						@endif
						<p><small>{{ $list->updated_at->format('F d, Y') }}</small></p>
						<h6><a href="{{url('blog/info/'.$list->slug)}}">{{ $list->title }}</a></h6>
					</div>
				@endforeach
			</div>
		</div>
	</div>
</div>