@php
	$query = App\model\Product::latest();

	if( !empty( $request->title ) ) {
            $query->where('title', 'LIKE', '%'.$request->title.'%');
        }
    $menu  = $query->paginate(60);

    $city_id = session()->get('city_session');

    $cities = App\model\City::pluck('name', 'id');
   
@endphp
<header style="background: #fff;position:sticky; top:0;z-index:9;box-shadow: 0px 7px 15px -9px #888888;">
	<div class="container">
		<div class="row" style="line-height: 100px">
			<div class="col-5 col-lg-2 col-md-2 col-sm-5">
				<a href="{{ url('/') }}"><img src="{{url('imgs/'.$setting->logo)}}" width="65%"></a>
			</div>
			<div class="col-lg-8 col-3 col-md-8 col-sm-3">
				
				<div class="row search-header d-none d-sm-none d-md-block">
					<div class="col-lg-4 py-0 px-0">
						<select class="form-control select2" id="city" style="border-radius: 10px 0 0px 10px;border-color: #f4a4b1;border-right:transparent;" >
							@foreach($cities as $id => $name)
							<option value="{{ $id }}" @if($city_id == $id) selected @endif>{{ $name }}</option>
							@endforeach
						</select>						
					</div>
					
					{!! Form::open(['url' => url('menu'), 'method' => 'GET']) !!}
					<div class="col-lg-6 col-md-7 col-sm-8" style="padding-right: 0;padding-left: 0;">
						<input type="text" name="title" class="form-control bg-white search-input-header" placeholder="Search product" aria-label="Recipient's username" aria-describedby="basic-addon2" style="border-radius: 0px; height: 42px; border-color: #f4a4b1;">
					</div>
					<div class="col-lg-2 col-md-5 col-sm-4" style="padding-left: 0">
						<button style="border-radius:  0 10px 10px 0px; border-color: #f4a4b1; background: #f4a4b1; color: #fff;" class="header-search-btn form-control" type="submit">Search</button>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
			<div class="col-2 col-lg-1 col-md-1 col-sm-2">
				<div class="cart-icon text-center home_header_cart">
					<a href="{{ url('/cart') }}" style="color: black;" class="">
						<i class="fa fa-cart-plus "></i>
						</a> 
						<div class="shop-cart header-collect">
			                @include('frontend.template.header_cart')
			            </div>
					

                
                
	                    
	                
				</div>

			    
			</div>
			<div class="col-2 col-lg-1 col-md-1 col-sm-2">
				<div class="cart-icon text-center"> 
					@guest()
                    <!-- <a href="{{url('/login')}}" class="text-white">LOGIN</a> -->
                    <a href="javascript:user_login()" class="text-white"><i class="fa fa-user "></i></a>
					@else
					<a href="{{ url('account') }}" class="text-white" style="color: #fff;"><i class="fa fa-user "></i><!-- Hello, {{ auth()->user()->name }} --></a>
					@endif
				</div>			
			</div>
		</div>
	</div>
</header>
