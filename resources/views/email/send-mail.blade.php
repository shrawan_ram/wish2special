<!DOCTYPE html>
<html>
<head>
    <title>{{ $subject }}</title>
</head>
<body>
    <div>Dear {{ $name }}</div>
    <h2>{{ $subject }}</h2>
	<p>{!! $msg !!}</p>
</body>
</html>