const baseUrl = $('base').attr('href')+'/';

$(function() {
    $.fn.showLoading = (function() {
        if(!$('.loading-box').length) {
            let loadingHtml = `
                <div class="loading-box">
                    <div class="loading-bg"></div>
                    <div class="loading-body">
                        <div class="form-group">
                            <img src="${baseUrl}imgs/loader-2_food.gif" alt="">
                        </div>
                        <div>
                            Loading, please wait...
                        </div>
                    </div>
                </div>
            `;
            $('body').append( loadingHtml );
        }
    });
    $.fn.hideLoading = (function() {
        setTimeout(function() {
            $('.loading-box').remove();
        }, 300);
    })
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
    });

    // Send Contact Enquiry
    $(document).on('submit', '[name="contact-form"]', function(e) {
        e.preventDefault();

        let form        = $(this),
            submit_btn  = form.find('[type=submit]'),
            ajax_url    = form.attr('action'),
            is_valid    = form.is_valid();

        form.find('.form-msg').removeClass('alert alert-info alert-success alert-danger').html('');

        if(is_valid) {
            submit_btn.attr('disabled', 'disabled').html('Please wait...');
            form.find('.form-msg').addClass('alert alert-info').html("Please wait, progressing...");
            $.ajax({
                url: ajax_url,
                type: 'POST',
                data: form.serialize(),
                success: function(response) {
                    submit_btn.removeAttr('disabled').html('SEND MESSAGE');
                    form.find('.form-msg').removeClass('alert-info').addClass('alert-success').html(response.message);

                    form.find('input, textarea').val('');
                },
                error: function(err) {
                    console.log(err);
                    form.find('.form-msg').removeClass('alert-info').addClass('alert-danger').html('Some error occurs, mail not sent.');
                }
            });
        }
    });

    
    // Apply Coupon
    $(document).on('click', '.apply_coupon', function() {
        let coupon_code = $('#coupon_code').val(),
            ajax_url    = $(this).data('url');

        if(coupon_code == ''){
            $('#coupon_code').focus();
            $('#coupon_code').css({'border-color':'red'});
        } else {
            $('#coupon_code').css({'border-color':''});
            $(document).showLoading();
            $.ajax({
                url: ajax_url,
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {
                    coupon_code: coupon_code
                },
                success: function(res) {
                    $(document).hideLoading();
                    if(res.status == false){
                        CMessage = `<div class="mt-3" id="couponmassege" style="color:red;">${res.message}</div>`;
                    } else {
                        CMessage = `<div class="mt-3" id="couponmassege" style="color:green;">${res.message}</div>`;
                    }
                    $('#massege').closest("span").append( CMessage );       
                    $('#cartFooter').html( res.cart_footer );
                    setTimeout(function() {
                        $('#couponmassege').remove();
                    }, 5000);
                }
            });
        }
    });

    $(document).on('click', '.remove-coupan', function() {
        ajax_url    = $(this).data('url');

        $(document).showLoading();

        $.ajax({
            url: ajax_url,
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(res) {
                $('#cartFooter').html( res.cart_footer );
                $(document).hideLoading();
            }
        });

    });

    $(document).on('click', '.price-textbox .plus-text', function() {
        let inp = $(this).closest('.price-textbox').find('input'),
            qty = parseInt( inp.val() );

        inp.val( qty + 1 );
    });

    $(document).on('click', '.price-textbox .minus-text', function() {
        let inp = $(this).closest('.price-textbox').find('input'),
            qty = parseInt( inp.val() );

        if(qty > 1) {
            inp.val( qty - 1 );
        }
    });

    $(document).on('click', '.updateCartBtn', function() {
        let form        = $(this).closest('form'),
            ajax_url    = form.attr('action');

        $(document).showLoading();
        $.ajax({
            url: ajax_url,
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: form.serialize(),
            success: function(res) {
                $(document).hideLoading();
                $('.shop-cart.header-collect').html( res.header_cart );
                $('#cartResponse').html( res.cart_table );
                $('#cartFooter').html( res.cart_footer );
            }
        });
    });

    $(document).on('click', '.send_otp_btn', function() {
        
        let mobile = $($(this).data('target'));

        mob     = /^[0-9]{10}$/;
        // var url =  $(this).attr('data-url');
        mobile.closest("div").find(".invalid-feedback").remove();
        mobile.closest("div").find(".is-invalid").removeClass("is-invalid");
        mobile.closest("div").find("div").removeClass("has-danger");

        if( mobile.val() != '' && mob.test( mobile.val() ) ) {
            
            $(document).showLoading();
            $.ajax({
                url: "ajax/send-otp", 
                type: 'POST',
                headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {
                    mobile: mobile.val()
                },
                success: function(res) {
                    $(document).hideLoading();
                    mobile.addClass("is-valid");
                    mobile.closest("div").append('<div class="valid-feedback">OTP has been sent.</div>');
                }
            });
        } else if( mobile.val() == '' ) {
            mobile.addClass("is-invalid");
            mobile.closest("div").append('<div class="invalid-feedback">Please enter mobile no.</div>');
        } else {
            mobile.addClass("is-invalid");
            mobile.closest("div").append('<div class="invalid-feedback">Please enter only 10 digit numeric value.</div>');
        }
    });

    $(document).on('click', '.shop-cart-close, .cart-remove-icon', function() {
        let ajax_url = $(this).data('url'),
            pid      = $(this).data('pid');

        $(document).showLoading();

        $.ajax({
            url: ajax_url,
            type: 'POST',
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
                pid: pid,
                qty: 1
            },
            success: function(res) {
                $(document).hideLoading();
                $('.shop-cart.header-collect').html( res.header_cart );
                if( $('#cartResponse').length > 0 ) {
                $('#ignismyModal').modal('show');
                    $('#cartResponse').html( res.cart_table );
                }
                if( $('#cartFooter').length > 0 ) {
                    $('#cartFooter').html( res.cart_footer );
                }
            }
        });
    });

    // Rate N Reviews
    $(document).on('click', '.rate_menu_btn', function() {
        let id = $(this).data('id');
        $('#review_pid').val(id);
        $('#reviewModal').modal('show');
    });

    $(document).on("submit", "#productRatingForm", function(e) {
        e.preventDefault();

        let form     = $(this),
            ajax_url = form.attr('action'),
            formData = form.serialize();

        let is_valid = form.is_valid();

        if(is_valid) {
            $('#reviewModal').modal('hide');
            $(document).showLoading();
            $.ajax({
                url: ajax_url,
                type: 'POST',
                data: formData,
                success: function( res ) {
                    $(document).hideLoading();
                    location.reload();
                }
            });
        }
    });

    $(window).on("load", function() {
        // $('.portfolioFilter .current , .galleryportfolio .current').removeClass('current');
        // $(this).addClass('current');
        var selector = $('.portfolioFilter .current , .galleryportfolio .current').attr('data-filter');
        var $container = $('.portfolioContainer , .gallery-filter');
        $container.isotope({
            layoutMode: 'masonry',
            filter: selector,
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false,
            }
        });
    });

    $(document).on('keyup change', '#fname, #lname', function() {
        let name = ($('#fname').val() + ' ' + $('#lname').val()).trim();

        $('#name').val( name );
    });
    $(document).on('click', '.addToCartBtnSingle', function() {
        let ajax_url = $(this).data('url'),
            qty      = $('.price-textbox').find('input').val(),
            pid      = $(this).data('pid');

        $(document).showLoading();

        $.ajax({
            url: ajax_url,
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
                pid: pid,
                qty: qty
            },
            success: function(res) {
                $(document).hideLoading();
                $('.shop-cart.header-collect').html( res.header_cart );

                let cartMessage = `
                    <div class="alert alert-success">${res.message}</div>
                `;
                $('#cartMessage').html( cartMessage );
                setTimeout(function() {
                    $('#cartMessage').html( '' );
                }, 5000);
            }
        });
    });
    $(document).on('click', '.addToCartBtnSinglewithshow', function() {
        let ajax_url = $(this).data('url'),
            qty      = $('.price-textbox').find('input').val(),
            pid      = $(this).data('pid');

        $(document).showLoading();

        $.ajax({
            url: ajax_url,
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
                pid: pid,
                qty: qty
            },
            success: function(res) {
                $(document).hideLoading();
                $('.shop-cart.header-collect').html( res.header_cart );

                let cartMessage = `
                    <div class="alert alert-success">${res.message}</div>
                `;
                $('#cartMessage').html( cartMessage );
                setTimeout(function() {
                    $('#cartMessage').html( '' );
                }, 5000);
                
                window.location = baseUrl+'cart';
            }
        });
    });
    $(document).on('click', '.addToCartBtn', function() {
        let ajax_url = $(this).data('url'),
            pid      = $(this).attr('data-pid');

        $(document).showLoading();

        $.ajax({
            url: ajax_url,
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
                pid: pid,
                qty: 1
            },
            success: function(res) {
                $(document).hideLoading();
                $('.shop-cart.header-collect').html( res.header_cart );
                
                let cartMessage = `
                    <div class="alert alert-success">${res.message}</div>
                `;
                
                $('#cartMessage').html( cartMessage );
                setTimeout(function() {
                    $('#cartMessage').html( '' );
                }, 5000);
            }
        });
    });
    
    $(document).on('click', '.addToCartBtnwithshow', function() {
        let ajax_url = $(this).data('url'),
            pid      = $(this).data('pid');

        $(document).showLoading();

        $.ajax({
            url: ajax_url,
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
                pid: pid,
                qty: 1
            },
            success: function(res) {
                $(document).hideLoading();
                $('.shop-cart.header-collect').html( res.header_cart );
                
                let cartMessage = `
                    <div class="alert alert-success">${res.message}</div>
                `;
                
                $('#cartMessage').html( cartMessage );
                setTimeout(function() {
                    $('#cartMessage').html( '' );
                }, 5000);
                // return redirect(route('cart'));
                window.location = baseUrl+'cart';
            }
        });
    });

    /*
     * ====================================================
     * CHECKOUT SCRIPTS
     * ====================================================
    **/
    $(document).on('click change', '#shipDifferentAddress', function() {
        if( $(this).prop('checked') ) {
            $('#shippingAddress').removeClass('hidden');
            $('.require').attr('required', 'required');
        } else {
            $('#shippingAddress').addClass('hidden');
            $('.require').removeAttr('required');
        }
    });
    $(document).on('click change', '[name="create_account"]', function() {
        if( $(this).prop('checked') ) {
            $('#customerPassword').removeClass('hidden');
            $('[name="password"]').attr('required', 'required');
        } else {
            $('#customerPassword').addClass('hidden');
            $('[name="password"]').removeAttr('required');
        }
    });
    
    $(document).on('click', '.subscribe-btn', function(e) {
        let ajax_url = $(this).data('url');
        e.preventDefault();
        var email = $('#subs_email').val();
        email = email.toLowerCase();
        
        var reg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,8}$/;
        if(email==''){
            $('#subs_email').focus();
        }
        else if(!reg.test(email)){
            $('#subs_email').focus();
        }
        else{
        $.ajax({
            url: ajax_url,
            type: 'POST',
            data: $('form.welcome_popup').serialize(),
            success: function(res) {
                
                window.location = baseUrl;
            }
        });
        }
    });

    $(document).on('submit', '#user_login', function(e) {
        let ajax_url = $(this).attr('action');
        e.preventDefault();
        
            $.ajax({
                url: ajax_url,
                type: 'POST',
                data: $('form.user_login').serialize(),
                success: function(res){

                    if(res.success==1)
                        {
                            location.reload();
                        }
                    else if(res.success==2){
                        toggleModel('#userVerifyotpModel');
                    }
                    else if(res.success==3){
                        $("form").before("<div>"+res.message+"</div>");
                        toggleModel('#userRegisterModel');
                    }
                    else{
                        $("#user_password").focus();
                        $("#user_password").css({'border-color':'red'});
                    }

                }
            });
        
    });

    $(document).on('submit', '#registerForm', function(e) {

        let ajax_url = $(this).attr('action');
        
        e.preventDefault();

            $.ajax({
                url: ajax_url,
                type: 'POST',
                data: $('form.user_register').serialize(),
                success: function(res){
                     if(res.success==1){
                        toggleModel('#userVerifyotpModel');
                    }
                    else if(res.success==2){
                        toggleModel('#userVerifyotpModel');
                    }
                    else{  
                        $("#registere_error").html("<div>"+res.message+"</div>");
                        // $("#registere_error").html('Error ');
                  // $("#contact_message").html('<strong>Thank you!</strong> We have received your message.');
                        toggleModel('#userLoginModel');
                    }
                     
                }
            });
        
    });

    $(document).on('submit', '#user_verifyotp', function(e) {

        let ajax_url = $(this).attr('action');
        
        e.preventDefault();

            $.ajax({
                url: ajax_url,
                type: 'POST',
                data: $('form.user_verifyotp').serialize(),
                success: function(res){

                     if(res.success){
                        $("#registere_error").html("<div>"+res.message+"</div>");
                            toggleModel('#userLoginModel');
                        }
                    else{
                        $(".otp-match").focus();
                        $(".otp-match").css({'border-color':'red'});
                    }
                     
                }
            });
        
    });

    $(document).on('submit', '#user_forgotpass', function(e) {

        let ajax_url = $(this).attr('action');
        
        e.preventDefault();

            $.ajax({
                url: ajax_url,
                type: 'POST',
                data: $('form.user_forgotpass').serialize(),
                success: function(res){

                     if(res.success){
                            toggleModel('#userVerifyotpforgotModel');
                        }
                    else{
                        $(".mobile-match").focus();
                        $(".mobile-match").css({'border-color':'red'});
                    }
                     
                }
            });
        
    });

    $(document).on('submit', '#user_verifyforgototp', function(e) {

        let ajax_url = $(this).attr('action');
        
        e.preventDefault();

            $.ajax({
                url: ajax_url,
                type: 'POST',
                data: $('form.user_verifyforgototp').serialize(),
                success: function(res){

                     if(res.success){
                            toggleModel('#userRecoverpassModel');
                        }
                    else{
                        $(".otp-match").focus();
                        $(".otp-match").css({'border-color':'red'});
                    }
                     
                }
            });
        
    });

    $(document).on('submit', '#user_recoverpass', function(e) {

        let ajax_url = $(this).attr('action');
        e.preventDefault();

        var new_password = $('#new_password').val();
        var confirm_password = $('#confirm_password').val();
        
        if(new_password==''){
            $('#new_password').focus();
            $('#new_password').css({'border-color':'red'});
        }
        else if(new_password.length<8){
            $("#new_password_msg").html("");
            $("#new_password_msg").html("Password at list 8 characters.");
            $('#new_password').focus();
            $('#new_password').css({'border-color':'red'});

        }else if(new_password!==confirm_password){
            $('#confirm_password').focus();
            $('#confirm_password').css({'border-color':'red'});
        }
        else{

            $.ajax({
                url: ajax_url,
                type: 'POST',
                data: $('form.user_recoverpass').serialize(),
                success: function(res){

                     if(res.success){
                        $("form").before("<div>"+res.message+"</div>");
                            toggleModel('#userLoginModel');
                        }
                    else{
                        $(".pass-match").focus();
                        $(".pass-match").css({'border-color':'red'});
                    }
                     
                }
            });
        }
        
    });

    $(document).on('click', '#login_resendotp', function() {
        
            $.ajax({
                url: baseUrl+"resend-otp", 
                type: 'POST',
                success: function(res) {
                    
                }
            });
       
    });

    $(document).on('submit', '#reservationForm', function(e) {

        let ajax_url = $(this).attr('action');
        
        e.preventDefault();
        $(document).showLoading();
            $.ajax({
                url: ajax_url,
                type: 'POST',
                data: $('form.reservation_table').serialize(),
                success: function(res){
                    if(res.success==1){
                        $(document).hideLoading();
                        location.reload();
                    }
                    else{  
                        $(document).hideLoading();
                        $("#otp_msg").html(res.message);
                    }
                     
                }
            });
        
    });

});

function user_login(){
    $("#user_login").modal('show');
}

function slider_details(pid){
    $.ajax({ 
        url: baseUrl+"slider_details/"+pid,
        type: 'POST',
        success: function(res) {
            $("#slider_details").modal('show');
            $('#menu_slider').html(res);
        }
    });
}

function toggleModel(target) {
    $(target).addClass('active');
    $('.auth-model').not(target).removeClass('active');
}