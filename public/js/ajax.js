$(function(){
    
    
$("#inquiry_form").on('submit', function(e){
    e.preventDefault();

    var url =  $(this).attr('data-url');
    
   $.ajax({
          url:url,
          type:"POST",
          headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          data: $(this).serialize(),
          success:function(response){
              
              if(response.status){
                  $("#ajax_message").html();
                  $("#ajax_message").html("Successful");
              }
            else{
                 $("#ajax_message").html();
                 $("#ajax_message").html('faild');
            }
            
          },
    });
    
});
$("#contact_form").on('submit', function(e){
    e.preventDefault();

    var url         =  $(this).attr('data-url');
    var name        = $('#user_name').val();
    var mobile_no   = $('#user_mobile').val();
    var email       = $('#user_email').val();
    var message     = $('#user_message').val();
    
    
   $.ajax({
          url:url,
          type:"POST",
          headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          data:{
            name      : name,
            mobile_no : mobile_no,
            email     : email,
            message   : message,
           
          },
          success:function(response){
              
              if(response.status){
                  // document.getElementById("contact_form").reset();
                  $('#ignismyModal').modal('show');
                  document.getElementById("contact_form").reset();
                  // $("#contact_message").html();
                  // $("#contact_message").html('<strong>Thank you!</strong> We have received your message.');
              }
            else{
                 $("#contact_message").html();
                 $("#contact_message").html('faild');
            }
            
          },
    });
});
    
})