$(function(){
    
    
$("#inquiry_form").on('submit', function(e){
    e.preventDefault();

    var url =  $(this).attr('data-url');
    var c_name = $('#company_name').val();
    var states = $('#states').val();
    var city = $('#city').val();
    var category = $('#business_category').val();
    var name = $('#name').val();
    var mobile_number = $('#mobile_no').val();
    var email = $('#email').val();
    var hear = $('#hear').val();
    // var term_condition = $('#term_condition').val();
    var term_condition = $('#term_condition').val();
    
   $.ajax({
          url:url,
          type:"POST",
          headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          data:{
            c_name:c_name,
            states:states,
            city:city,
            category:category,
            name:name,
            email:email,
            mobile_number:mobile_number,
            hear:hear,
            term_condition:term_condition
          },
          success:function(response){
              
              if(response.status){
                   $("#ajax_message").html();
                  $("#ajax_message").html("Successfull");
              }
            else{
                 $("#ajax_message").html();
                 $("#ajax_message").html('faild');
            }
            
          },
    });
    document.getElementById("inquiry_form").reset();
});
    
})