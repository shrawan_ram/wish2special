-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 20, 2020 at 08:38 AM
-- Server version: 5.7.32
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cityfood_drvivekbindra`
--

-- --------------------------------------------------------

--
-- Table structure for table `sc_courses`
--

CREATE TABLE `sc_courses` (
  `id` int(11) NOT NULL,
  `service_title` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `service_short_description` longtext,
  `service_description` longtext,
  `image` varchar(255) DEFAULT NULL,
  `image_hover` varchar(255) DEFAULT NULL,
  `parent` bigint(20) UNSIGNED DEFAULT NULL,
  `banner_image` varchar(255) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `seo_description` longtext,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sc_courses`
--

INSERT INTO `sc_courses` (`id`, `service_title`, `slug`, `service_short_description`, `service_description`, `image`, `image_hover`, `parent`, `banner_image`, `seo_title`, `keywords`, `seo_description`, `created_at`, `updated_at`) VALUES
(45, 'SEO Company In Jodhpur', 'seo-company-in-jodhpur', 'SEO Company in Jodhpur We offer Search Engine Optimization SEO Services in Jodhpur, India at a reasonable price, Contact us for improving your online presence by our best SEO services', '<h1>SEO Company In Jodhpur</h1>\r\n<p>Seo 24X7, a top-notch SEO services company in Jodhpur, India, and around the world, has a proven track record verified by its loyal client across the globe.</p>\r\n<p>Being the best leading SEO company in Jodhpur, we help businesses of all sizes get better search engine rankings with SEO services. Jodhpur based SEO agency team has years of experience in successfully ranking websites for their targeted keywords. They identify defects in web design and fix them.</p>\r\n<p>Now more and more people in India go online to find products or services and about 95% of them don\'t go past the first page of Google! Our Search Engine Optimization (SEO) strategies are made by only one goal that is first page rankings in search engines that will drive business growth.</p>\r\n<p>We at 24X7 Seo are more than a regular SEO company in Jodhpur. We are a Total Digital Growth Company, a team of specialists in shaping the appearance and visibility of your business online including SEO, PPC, Social Media Management, E-mail Marketing, Digital Marketing, and much more. We have everything when it comes to SEO Services and offer a large wide range of services including Content marketing, Social Media Marketing Services (SMM Services), PPC, SMO services, etc. under one roof to helping businesses improve their online marketing ventures.</p>\r\n<h3>Choose Our SEO Services:</h3>\r\n<ul>\r\n<li>To improve your rankings on important keywords through proper SEO.</li>\r\n<li>To increase natural visibility to related users through search engines.</li>\r\n<li>To make sure that the original traffic drives to your website.</li>\r\n<li>To concentrate on your companies aims, services, highest value products, your brand, and more.</li>\r\n<li>To keep up with the latest plans and updates of search engine optimization.</li>\r\n</ul>', 'banner1.jpg', 'ab1.jpg', NULL, 'banner4.jpg', 'SEO Company in Jodhpur | Best SEO Company in Jodhpur | SEO Services in Jodhpur', NULL, '24x7 SEO is one of the best leading SEO company in Jodhpur We have an impressive list of dedicated clients. Best Search Engine Optimization Services in Jodhpur for more details contact us.', '2020-10-07 11:45:11', '2020-10-12 10:56:13'),
(46, 'Digital Marketing Company in Jodhpur', 'digital-marketing-company-in-jodhpur', 'SEO Company in Jodhpur We offer Search Engine Optimization SEO Services in Jodhpur, India at a reasonable price, Contact us for improving your online presence by our best SEO services', '<h2>Digital Marketing Company in Jodhpur</h2>\r\n<p>24X7 SEO, as a leading Digital Marketing Company in Jodhpur, helps you establish a foothold on the online medium for Search Engines and Social Media. we use ethical and proven methods to get your brand get and sustain a remarkable online presence.</p>\r\n<p>We are a full-scale Digital Marketing Agency based out of Jodhpur India. We mix our years of experience and experience to create solutions for our clients which are not only performance-driven but also inventive.&nbsp; We are operatingkick-ass digital campaigns for our clients, even as you read this!</p>\r\n<p>Your website is key to reaching your business objectives, then unquestionably you need it to perform. Your website can generate meaningful revenue, but without visibility and customer commitment it will struggle to fulfill its potential. In this complex digital ecosystem raising brand awareness, increasing customer engagement, and driving sales or repeat purchases requires a data-driven digital marketing strategy. Thus, choosing the right digital marketing company in Jodhpur is the most important choice for your business\' long-term growth.</p>\r\n<p>we aspire to be your &ldquo;ROI Driven&rdquo; &amp; &ldquo;Accountable&rdquo; Branding, Marketing Technology, eCommerce Partner, and Digital Marketing company in Jodhpur. Our team starts by unraveling the consumer/market Insight to build different Business Strategy and creative digital solutions to support you win over the competition. We facilitate the implementation of your strategy with excellent web design, responsive website development, and mobile apps to help your organization win the digital marketing war.</p>\r\n<p>&nbsp;we believe in plans that lead to measurable and desired results. Our team regularly supports digital marketing training as the digital world is constantly changing. We genuinely think that all the members of our team need to learn digital marketing approaches to deliver optimized results for our clients. That is why live projects are the most important learning opportunities for our clients and us. These practices are what keep our clients transforming their industries.</p>\r\n<p>we have years of experience navigating the secure-evolving digital landscape and delivering quality digital marketing services in jodhpur. We have a long history of accouching successful business outcomes for clients from diverse industry verticals. We owe this achievement to our motto of &ldquo;Digital Excellence.&rdquo; Our culture of going above and behind to deliver results, no matter what!</p>\r\n<p>Our Digital Marketing Company includes passionate marketers and certified experts who are adept at handling all aspects of Digital &ndash; from Search to Content to Social to Paid Media to Design and everything in among. Each one of us lives by our motto of #DigitalExcellence and understands what it takes to succeed.</p>', NULL, NULL, NULL, NULL, 'Digital Marketing Company in Jodhpur', NULL, NULL, '2020-10-10 03:18:14', '2020-10-14 09:47:01'),
(47, 'SMM Company in Jodhpur', 'smm-company-in-jodhpur', 'SEO Company in Jodhpur We offer Search Engine Optimization SEO Services in Jodhpur, India at a reasonable price, Contact us for improving your online presence by our best SEO services', '<h2>SMM Company in Jodhpur</h2>\r\n<p>Social media marketing means the procedure of attaining website traffic or attention through social media sites. Social Media Marketing plans typically focus on endeavors to make content that pulls in consideration and urges users to impart it to their social networks.</p>\r\n<p>Social media marketing is the use of social media programs and websites to promote a product, services, and Lead generation, we basically target the business with his behavior to counter all aspect of human nature. Our SMM ( Social Media Marketing ) services one of the best services in Jodhpur. Mostly the company needs Social Media Marketing to build brand awareness and Lead capturing in the related market. Social Media Marketing Company in Jodhpur: We are a top SMO SMM Lead Generation Services Agency In Jodhpur. Get SMM SMO Lead Generation Services Today.</p>\r\n<p>Spread Digital, best SMM Social media Marketing services company in Jodhpur to promote all types of business at a high-level campaign strategy for the Tours &amp; travel business management, Restaurant management, Education, Health management, and All types of business to improve brand and Leads generation services at a very competitive price.</p>\r\n<p>Our Best SMM Company in Jodhpur will monitor and maintain your social network profiles, constantly updating strategies and ensuring that your online reputation and image remains at a high level, which leads to more sales and increases revenue.</p>', NULL, NULL, NULL, NULL, 'SMM Company in Jodhpur', NULL, NULL, '2020-10-10 03:20:59', '2020-10-14 09:46:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sc_courses`
--
ALTER TABLE `sc_courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sc_courses`
--
ALTER TABLE `sc_courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
