<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    
    protected $table 		= "client";

	public function cat(){
		return $this->hasOne('App\model\Category', 'id', 'parent');
	}

    
}
