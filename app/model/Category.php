<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{


    protected $table        = "categories";
    const CREATED_AT        = 'created_at';
    const UPDATED_AT        = 'updated_at';
  
    public function service(){
        return $this->hasMany('App\model\Blog', 'cid', 'id');
   }
   public function products(){
    return $this->hasMany('App\model\Product', 'category_id', 'id');
}
    public function faq(){
        return $this->hasMany('App\model\Faq', 'cid', 'id');
   }
}
