<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $guarded = [];

    public function states()
    {
        return $this->hasOne('App\model\State', 'id', 'state');
    }
    public function citys()
    {
        return $this->hasOne('App\model\City', 'id', 'city');
    }
}
