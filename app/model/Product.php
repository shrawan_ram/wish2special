<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
    protected $table 		= "menu";

	public function category(){
		return $this->hasOne('App\model\Category', 'id', 'category_id');
	}
	// $sortDirection = 'desc';
	public function p(){  
       return $this->hasMany('App\model\Productdetail', 'p_id', 'id')->orderBy('id','DESC');  
    } 
    public function order(){  
       return $this->hasMany('App\model\OrderMenu', 'menu_id', 'id');  
    }

  public function vendor()
  {
    return $this->belongsTo(Vendor::class);
  }

    
}
