<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Whatdo extends Model
{
    
    protected $table 		= "whatdo";

	public function cat(){
		return $this->hasOne('App\Model\Category', 'id', 'parent');
	}

    
}
