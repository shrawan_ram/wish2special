<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Fevorite extends Model
{
    protected $guarded = [];

    public function products()
    {
        return $this->hasOne('App\model\Product', 'id', 'pid');
    }
    public function users()
    {
        return $this->hasOne('App\User', 'id', 'uid');
    }
    public function media()
    {
        return $this->hasOne('App\model\Media', 'id', 'image');
    }
}
