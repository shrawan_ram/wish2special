<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $guarded = [];

    protected $casts = [
        'created_at' => 'date:F d, Y',
        'updated_at' => 'date:F d, Y',
    ];

    public function state()
    {
        return $this->hasOne('App\model\State', 'id', 'sid');
    }
    public function states(){
	    return $this->hasMany('App\model\State', 'sid', 'id');
	}
    public function vendor(){
        return $this->hasMany('App\model\Vendor', 'city_id', 'id');
    }
}
