<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderMenu extends Model
{
    protected $table    = "order_menu";
    protected $guarded  = [];

    public function menu()
    {
        return $this->hasOne('App\model\Productdetail', 'id', 'menu_id');
    }
    public function order()
    {
        return $this->hasOne('App\model\Order', 'id', 'oid');
    }
    public function media()
    {
        return $this->hasOne('App\model\Media', 'id', 'image');
    }
}
