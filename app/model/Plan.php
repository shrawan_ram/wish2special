<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    
    protected $table 		= "plans";

	public function cat(){
		return $this->hasOne('App\model\Category', 'id', 'parent');
	}

    
}
