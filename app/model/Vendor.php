<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{


    protected $table        = "vendors";
    const CREATED_AT        = 'created_at';
    const UPDATED_AT        = 'updated_at';
  
    public function city(){
        return $this->hasMany('App\model\City', 'id', 'city_id');
   }
//    public function products(){
//     return $this->hasMany('App\model\Product', 'category_id', 'id');
// }
    public function products(){
        return $this->hasMany('App\model\Product', 'vendor_id', 'id');
   }
}
