<?php
namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = [];
	public $casts = [
        'created_at'    => 'date: d M Y',
        'updated_at'    => 'date: d M Y',
    ];
    
    public function blog()
    {
        return $this->hasMany('App\model\Blog', 'tid', 'id');
    }
}
