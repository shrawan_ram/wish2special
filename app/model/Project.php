<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {
	protected $table 		= "projects";

	public function cat(){
		return $this->hasOne('App\Model\Category', 'id', 'parent');
	}
}
