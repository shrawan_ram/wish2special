<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    
    protected $table = "blog";

	public function cat(){
		return $this->hasOne('App\model\Category', 'id', 'cid');
	}

    
}
