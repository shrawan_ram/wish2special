<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Productdetail extends Model
{
    
    protected $table 		= "menu_detail";

    protected $fillable = ['volume', 'unit'];
    protected $guarded = ['weight'];

	public function product(){
		return $this->hasOne('App\model\Product', 'id', 'p_id');
	}
	public function getFullName()
{
    return "{$this->volume} {$this->unit}";
}

    
}