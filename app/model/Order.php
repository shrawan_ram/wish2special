<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];

    protected $with    = ['products'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'uid');
    }
    public function state()
    {
        return $this->hasOne('App\model\State', 'id', 'billing_state');
    }
    public function city()
    {
        return $this->hasOne('App\model\City', 'id', 'billing_city');
    }

    public function s_state()
    {
        return $this->hasOne('App\model\State', 'id', 'shipping_state');
    }
    public function s_city()
    {
        return $this->hasOne('App\model\City', 'id', 'shipping_city');
    }

    public function billingaddress()
    {
        return $this->hasOne('App\model\Address', 'id', 'billing_addressid');
    }
    
    public function shippingaddress()
    {
        return $this->hasOne('App\model\Address', 'id', 'shipping_addressid');
    }
    public function products()
    {
        return $this->hasMany('App\model\OrderMenu', 'oid', 'id');
    }
    public function setting()
    {
        return $this->hasOne('App\model\Setting', 'id', 'uid');
    }
    public function media()
    {
        return $this->hasOne('App\model\Media', 'id', 'image');
    }
}
