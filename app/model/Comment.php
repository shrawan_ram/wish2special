<?php
namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $guarded = [];
    protected $appends = ['user_name'];

    public function getUserNameAttribute()
    {
    	return $this->user->name;
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'uid');
    }
}
