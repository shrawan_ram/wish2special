<?php

namespace App\Http\Controllers;
use App\model\Blog;
use App\model\Category;


use Illuminate\Routing\Controller as BaseController;

class SingleBlogController extends BaseController {
    public function index(Blog $slug , Category $category) {
    	  	
    	$data1 = Blog::where('id','!=', $slug->id)->paginate(4)->toArray();	

    	$category1   = Category::with('service')->latest()->get();
        
        $cat_name = $category->title;
        $latestblog=Blog::latest()->paginate(3);
        $blog = Blog::where('cid', $category->id)->paginate(15);
    	$data = compact( 'slug','data1','category','category1','cat_name','blog','latestblog');

    
    	return view('frontend.inc.single-blog',$data);
    }
}


