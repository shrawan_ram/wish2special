<?php

namespace App\Http\Controllers;
// use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\model\Client;
use App\model\Plan;
use App\model\Slider;
use App\model\About;
use App\model\Whatdo;
use App\model\Product;
use App\model\Productdetail;
use App\model\Category;
use App\model\Marquee;
use App\model\Pages;
use App\model\Faq;
use App\model\Setting;
use App\model\Offer;
use App\model\Blog;
use App\model\City;



class HomeController extends Controller {
    public function index(Request $request, $cslug = null, $id=1) {
        
        $slider=Slider::latest()->paginate(10);       
    	$term=Pages::findOrFail(2);	  	
    	$about=About::findOrFail(1);	  	
        $whatdo=Whatdo::findOrFail(1);      
    	$setting=Setting::findOrFail(1); 
                
        $offer=Offer::get();        
    	$city=City::get();	  	
    	$faq=Faq::latest()->get();


        $city_id = session()->get('city_session');
        // var_dump($city_id);
        // die;
        if($city_id===null)
        // $cid = City::where('is_default', 1)->get();
    	$city_id = 1;
         // dd($cid);



        $product  =  Product::withCount('p')
        ->with('category','p')
        ->whereHas('vendor', function ($q) use ($city_id) {
            $q->where('city_id', $city_id);
        })
        ->get();
        
        
        // $productdetail = Productdetail::with(['product'])->get();
        // dd($productdetail);
        

        $category =  Category::with(['products'])->has('products')->get();
        
        $blog =  Blog::get();
        $weight = $product;
    	$data = compact('slider','about','whatdo','product','term','faq','setting','offer','category','blog','cslug','city');
    	
    	return view('frontend.inc.homepage' ,$data);
    }

    public function change_city($city_id) {

        session()->put('city_session', $city_id);

        return redirect()->back()->with('success', 'city changed');
    }
}
