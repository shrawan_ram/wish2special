<?php

namespace App\Http\Controllers;
use App\model\Project;

use Illuminate\Routing\Controller as BaseController;

class SingleProjectController extends BaseController {
    public function index(Project $slug) {
    	
    	$data1 = Project::where('id','!=', $slug->id)->paginate(4)->toArray();	
    	$data = compact( 'slug','data1');  	
    	
    	return view('frontend.inc.single-project',$data);
    }
}
