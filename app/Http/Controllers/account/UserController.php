<?php

namespace App\Http\Controllers\account;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\model\Setting;
use App\User;
use Auth;
use Hash;

class UserController extends BaseController
{
    public function profile()
    {
        $user = Auth()->user();
        $setting = Setting::findOrfail(1);
        $data = compact('user','setting');
        return view('frontend.profile.index', $data);
    }
    public function edit_profile()
    {
        $user = Auth()->user();
        $setting = Setting::findOrfail(1);
        $data = compact('user','setting');
        return view('frontend.profile.edit', $data);
    }
    public function save_profile(Request $request)
    {
        $rules = [
            'record' => 'required|array',
            'record.fname' => 'required|string|regex:/[A-Za-z ]+/',
            'record.lname' => 'regex:/[A-Za-z ]*/',
            'record.email' => 'required|email',
            'record.name'  => 'required|string|regex:/[A-Za-z ]+/'
        ];
        $request->validate($rules);

        Auth()->user()->update($request->record);

        return redirect()->back()->with('success', 'Success! Information has been updated.');
    }
    public function change_password()
    {
        $setting = Setting::findOrFail(1);
        $data    = compact('setting');
        return view('frontend.profile.change_password',$data);
    }
    public function save_password(Request $request)
    {
        $rules = [
            'current_password' => 'required|string',
            'new_password'     => 'required|string|min:8|same:new_password',
            'confirm_password' => 'required|string|min:8|same:new_password'
        ];
        $request->validate($rules);

        $new_password = Hash::make($request->new_password);
        
        if($request->current_password != $request->new_password){
            Auth()->user()->update(['password' => $new_password]);
            return redirect()->back()->with('success', 'Your password has been changed successfully.');
        } else {
            return redirect()->back()->with('danger', 'Error!! Current and new password are same.');
        }
    }
    public function logout()
    {
        Auth()->logout();
        return redirect(url('/'))->with('success', 'You\'re logged out successfully.');
    }
}
