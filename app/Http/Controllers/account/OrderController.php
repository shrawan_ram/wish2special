<?php

namespace App\Http\Controllers\account;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

use App\model\Setting;
use App\model\Order;
use App\model\OrderMenu;
use App\model\Fevorite;

class OrderController extends Controller
{
    public function my_orders()
    {
        $setting = Setting::find(1);
        $user   = Auth()->user();

        $lists  = Order::latest()->where('uid', $user->id)->with('state', 'city', 's_state', 's_city', 'billingaddress', 'shippingaddress')->get();
        // dd($user->id);

        $data   = compact('user', 'lists', 'setting');
        return view('frontend.profile.my_orders', $data)->with('sn', 1);
    }
    public function single(Order $order)
    {   
        
        $user    = Auth()->user();
        $setting = Setting::find(1);
        $menu   = OrderMenu::where('oid', $order->id)->with('menu')->get();

        $data    = compact('user', 'order', 'setting', 'menu');
        return view('frontend.profile.order_info', $data);
    }
    public function save_rating(Request $request)
    {
        $arr = $request->rate;
        Auth()->user()->reviews()->create($arr);

        $re = [
            'status'  => true,
            'message' => 'Rating has been placed.'
        ];
        return response()->json($re);
    }
    public function wishlist()
    {
        $setting = Setting::findOrFail(1);
        $user    = Auth()->user();
        $lists   = Fevorite::where('uid', $user->id)->with('products')->get();
        
        $data   = compact('lists','setting');
        return view('frontend.profile.wishlist', $data)->with('sn', 1);
    }
}
