<?php

namespace App\Http\Controllers\account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

use App\User;
use App\model\Address;
use App\model\Setting;
use App\model\State;
use App\model\City;

class AddressController extends Controller
{
    public function index()
    {
        $state = State::get();
        $states  = ['' => 'Select State'];
        if (!$state->isEmpty()) {
            foreach ($state as $mcat) {
                $states[$mcat->name] = $mcat->name;
            }
        }
        $city = City::get();
        $cities  = ['' => 'Select City'];
        if (!$city->isEmpty()) {
            foreach ($city as $mcat) {
                $cities[$mcat->name] = $mcat->name;
            }
        }
        $setting = Setting::findOrFail(1);
        $data   = compact('cities', 'states','setting');
        // return data to view
        return view('frontend.profile.add_address', $data);
    }
    public function list()
    {
        $user = Auth()->user();
        $address = Address::where('uid', $user->id)->with('citys', 'states')->get();
        $setting = Setting::findOrFail(1);
        
        $data   = compact('address','setting');
        return view('frontend.profile.address',$data);
    }
    public function add(Request $request)
    {
        $rules = [
            'country' => 'required|string',
            'state'   => 'required|string',
            'city'   => 'required|string',
            'pincode'   => 'required|numeric',
            'address'   => 'required|string'
        ];
        $request->validate($rules);

        $state = State::get();
        $states  = ['' => 'Select State'];
        if (!$state->isEmpty()) {
            foreach ($state as $mcat) {
                $states[$mcat->name] = $mcat->name;
            }
        }
        $city = City::get();
        $cities  = ['' => 'Select City'];
        if (!$city->isEmpty()) {
            foreach ($city as $mcat) {
                $cities[$mcat->name] = $mcat->name;
            }
        }
        
        $dataArr = [
            'uid'      => Auth()->user()->id,  
            'country'  => request('country'),
            'state'    => request('state'),
            'city'     => request('city'),
            'pincode'  => request('pincode'),
            'address'  => request('address')
        ];
        
        $newAddress    = Address::create($dataArr);
        
        return redirect(route('address'))->with('success', 'Success! New record has been added.');
        
    }
    // edit record
    public function edit(Request $request, $id)
    {
        $address      = Address::find($id);
        $edit_data = [
            'record' => $address->toArray()
        ];

        $state = State::get();
        $states  = ['' => 'Select State'];
        if (!$state->isEmpty()) {
            foreach ($state as $mcat) {
                $states[$mcat->name] = $mcat->name;
            }
        }

        $city = City::get();
        $cities  = ['' => 'Select City'];
        if (!$city->isEmpty()) {
            foreach ($city as $mcat) {
                $cities[$mcat->name] = $mcat->name;
            }
        }

        $request->replace($edit_data);
        $request->flash();
        $setting = Setting::findOrFail(1);
        $data   = compact('id', 'address', 'cities', 'states','setting');
        return view('frontend.profile.edit_address', $data);
    }
    public function update(Request $request, $id)
    {
        $record     = Address::findOrFail($id);
        
        $record->update($request->all());

        return redirect(route('address'))->with('success', 'Success! Record has been edided');
    }
    public function destroy($id)
    {
        $lists  = Address::findOrFail($id);
        $lists->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
    
}
