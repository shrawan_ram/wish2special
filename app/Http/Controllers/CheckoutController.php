<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Validator;

use App\model\Product;
use App\model\Productdetail;
use App\model\Setting;
use App\model\Order;
use App\model\State;
use App\model\City;
use App\model\Address;
use App\model\Coupon;
use App\model\Couponapply;
use App\User;

use Hash;

class CheckoutController extends BaseController
{
    public function index()
    {
        $cart = session()->get('cart');
        $cartProducts = [];
        $totalPrice   = 0;

        if (!empty($cart)) {
            foreach ($cart as $pid => $pData) {
                $productInfo = \App\model\Productdetail::find($pid);

                if (!empty($productInfo->id)) {
                    $productInfo->qty   = $pData['qty'];
                    $cartProducts[]     = $productInfo;
                    $totalPrice        += $productInfo->sale_price * $pData['qty'];
                }
            }
        }

        $setting = Setting::findOrFail(1);
        $user = auth()->user();
        $state = State::get();
        $states  = ['' => 'Select State'];
        if (!$state->isEmpty()) {
            foreach ($state as $mcat) {
                $states[$mcat->id] = $mcat->name;
            }
        }
        $city = City::get();
        $cities  = ['' => 'Select City'];
        if (!$city->isEmpty()) {
            foreach ($city as $mcat) {
                $cities[$mcat->id] = $mcat->name;
            }
        }

        $id = Auth()->user()->id;

        $address = Address::where('uid', $id)->get();
        $data = compact('cartProducts', 'totalPrice', 'setting', 'states', 'cities', 'address','user');

        return view('frontend.inc.checkout', $data);
    }

    

    public function save(Request $request)
    {
            $coupan = session()->get('coupon');
            
            $cart = session()->get('cart');
            $cartProducts = [];
            $totalPrice   = 0;

            if (!empty($cart)) {
                foreach ($cart as $pid => $pData) {
                    $productInfo = \App\model\Productdetail::find($pid);
                    if (!empty($productInfo->id)) {
                        $productInfo->qty   = $pData['qty'];
                        $cartProducts[]     = $productInfo;
                        $totalPrice        += $productInfo->sale_price * $pData['qty'];
                    }
                }
            }

            if(Auth()->user()){
                if(!empty($coupan)){
                    if($coupan->discount_type == 'Flat'){
                        $Discount = $coupan->discount;
                    }
                    else {
                        $Discount = $totalPrice * ( $coupan->discount / 100 );
                        if($coupan->upto != ''){
                            if($Discount > $coupan->upto){
                                $Discount = $coupan->upto;
                            }
                        }
                    }
                }
            }
            
            $currentMonth   = date('n', time());
            $financialYear  = $currentMonth > 3 ? date('Y')."-".(date('y') + 1) : (date('Y') - 1)."-".date('y');

            $maxInvoice     = Order::where('financial_year', $financialYear)->max('invoice_no');
            $invocieNo      = $maxInvoice + 1;

            $orderArr       = request('order');


            if(Auth()->user()){
                $orderArr['billing_fname'] = Auth()->user()->fname ?? '';
                $orderArr['billing_lname'] = Auth()->user()->lname ?? '';
                $orderArr['billing_email'] = Auth()->user()->email ?? '';
                $orderArr['billing_phone'] = Auth()->user()->mobile ?? '';
            }

            // if($request->is_other_shipping == '1'){
            //     if($request->order['shipping_addressid'] == ''){
            //         $shippingArr       = request('shipping');
            //         $shipping = Address::create($shippingArr);
            //         $orderArr['shipping_addressid'] = $shipping->id;
            //     }else{
            //         $orderArr['shipping_addressid'] = $request->order['shipping_addressid'];
            //     }
            // }

            if($orderArr['payment_mode'] == 'COD'){
                $orderArr['payment_status'] = 'Due';
            }
            if($orderArr['payment_mode'] == 'RazorPay'){
                $orderArr['payment_status'] = 'Paid';
            }
            
            if(!empty($coupan)){
                $orderArr['coupon_code'] = $coupan->code;
                $orderArr['discount'] = $Discount;
            }
            

            $orderArr['financial_year'] = $financialYear;
            $orderArr['invoice_no']     = $invocieNo;
            $orderArr['uid'] = auth()->user()->id;
            $order = Order::create($orderArr);
            
            

            if($order->coupon_code != ''){
                $coupon_details = Coupon::where('code', $order->coupon_code)->first();
                $coupon_details->limit_user  = $coupon_details->limit_user-1;
                $coupon_details->save();

                $peruserlimit = new Couponapply;
                $peruserlimit->uid = Auth()->user()->id;
                $peruserlimit->coupon = $coupon_details->id;
                $peruserlimit->save();
            }

            $cart = session()->get('cart');
            
            if (!empty($cart)) {
                foreach ($cart as $pid => $pData) {
                    $productInfo = \App\Model\Productdetail::find($pid);
                    if (!empty($productInfo->id)) {
                         $productInfo->qty   = $pData['qty'];
                        
                        $isJainFood = $pData['is_jain_food'] ? "Yes" : "No";

                        $menuArr = [
                            'menu_id'   => $pid,
                            'qty'       => $pData['qty'],
                            'price'     => $productInfo->sale_price,
                            'jain_food' => $isJainFood,
                        ];
                        
                        $order->products()->create($menuArr);
                    }
                }
            }

            session()->forget('cart');
            session()->forget('coupon');

            // Order Message
            $setting    = Setting::find(1);
            $mobile_no  = $order->billing_phone;
            
            $msg    = urlencode("Your City Food order no. #000".$order->id." for Rs. ".$order->total_amount." is expected to arrive within some time.");

            // $apiUrl = str_replace(["[message]", "[number]"], [$msg, $mobile_no], $setting->sms_api);

            // $sms    = file_get_contents($apiUrl);

            // return response()->json(['foo'=>'bar', 'id'=>$order->id]);
            return redirect(url('checkout/thank-you/?order='.$order->id));
        
    }
    
    public function thank_you(Order $orderInfo)
    {
        $setting = Setting::findOrFail(1);
        $orderId = request('order');
        $orderInfo = Order::findOrFail($orderId);
        
        $data = compact('orderId','setting');
        return view('frontend.inc.thankyou', $data);
        // dd($orderInfo);
    }
}
