<?php

namespace App\Http\Controllers;

use App\model\Service;

use Illuminate\Routing\Controller as BaseController;

class SingleServiceController extends BaseController {
    public function index(Service $slug) {
    $service=Service::latest()->paginate();	  	
    $data1 = Service::where('id','!=', $slug->id)->paginate(4)->toArray();	
    $data = compact( 'slug','data1','service');
    return view('frontend.inc.single-service',$data);
    }
}

