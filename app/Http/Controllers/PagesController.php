<?php

namespace App\Http\Controllers;
use App\model\Setting;
use App\model\Pages;

use Illuminate\Routing\Controller as BaseController;

class PagesController extends BaseController {
    public function index() {
    	  	
    	
    	return view('frontend.inc.pages');
    }
    public function privacy() {
    	$setting = Setting::findOrFail(1);
    	$privacy = Pages::findOrFail(1);
    	$data    = compact('setting','privacy');
    	
    	return view('frontend.inc.privacy',$data);
    }
    public function term() {
    	$setting = Setting::findOrFail(1);
    	$term    = Pages::findOrFail(2);
    	$data    = compact('setting','term'); 	
    	
    	return view('frontend.inc.term',$data);
    }
    public function security() {
    	$setting  = Setting::findOrFail(1);
    	$security = Pages::findOrFail(3);
    	$data     = compact('setting','security'); 	
    	
    	return view('frontend.inc.security',$data);
    }


}
