<?php
namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\model\Inquiry;
use App\model\Setting;
use App\model\Coupon;
use App\Model\Couponapply;
use App\Model\Productdetail;

use Auth;
use Hash;
use App\User;
use Mail;
// use App\Model\Coupon;
// use App\Model\Couponapply;
// use App\Model\Newsletter;
// use App\Model\Reservation;
// use App\Model\Fevorite;

class AjaxController extends BaseController {
    
    public function index(Request $request) {
        
        
        
         $data = new Inquiry();
         $data->name = $request->name;
         $data->email = $request->email;
         $data->mobile = $request->mobile;
         $data->message = $request->message;
         
         
         if($data->save()){
               return response()->json(['status'=>1]);
         }
         else{
                return response()->json(['status'=>0]);
         }


    }
    public function sendOtp(Request $request)
    {
        $setting    = Setting::find(1);
        $mobile_no  = $request->mobile;
        $otp        = rand(100000, 999999);

        // Send SMS
        $msg    = urlencode("Your one time password in ".$setting->site_title." is ".$otp." ");

        $apiUrl = str_replace(["[message]", "[number]"], [$msg, $mobile_no], $setting->sms_api);

        // $sms    = file_get_contents($apiUrl);

        session()->put('sms_otp', $otp);
        session()->put('mobile', $mobile_no);       

        $re = [
            'message'       => "SMS has been sent.",
            // "sms_response"  => $sms, 
            "otp"           => $otp
        ];

        return response()->json($re, 200);
    }

    public function resendOtp(Request $request)
    {
        $setting    = Setting::find(1);
        $mobile_no  = session()->get('mobile');

        $otp        = rand(100000, 999999);

        // Send SMS
        $msg    = urlencode("Your one time password in ".$setting->site_title." is ".$otp." ");

        $apiUrl = str_replace(["[message]", "[number]"], [$msg, $mobile_no], $setting->sms_api);

        $sms    = file_get_contents($apiUrl);

        session()->put('sms_otp', $otp);
        session()->put('mobile', $mobile_no);        

        $re = [
            'message'       => "SMS has been sent.",
            "sms_response"  => $sms,
        ];
        // print_r($re);

        return response()->json($re, 200);
    }
    public function auth(Request $request)
    {
        $rules = [
            'login'     => 'required|regex:/\d{10}/',
            'password'  => 'required|string'
        ];
        $request->validate($rules);

        // Check if username exists or not
        $user = User::where('login', $request->login)->first();
        if (!empty($user->id)) {

            $credentials = $request->only('login', 'password');
            $remember    = !empty($request->remember) ? true : false;
            if (Auth::attempt($credentials, $remember)) {
                return response()->json(['success'=>1, 'message'=> 'Success!! Login successfully.']);
            } else {
                return response()->json(['success'=>0, 'message'=> 'Error!! Password not matched.']);
            }
             
        } else {
            return response()->json(['success'=>3, 'message'=> 'Error!! mobile number not exists.']);
        }
    }

    public function register(Request $request)
    {

        $rules = [
            'fname'             => 'required|string|regex:/[A-Za-z ]+/',
            'lname'             => 'string|regex:/[A-Za-z ]*/',
            'mobile'            => 'required|string|regex:/\d{10}/',
            'email'             => 'email',
            'password'          => 'required|string|same:password',
            'confirm_password'  => 'required|string|same:password',
        ];
        $request->validate($rules);
        $check  =  User::where('mobile', $request->mobile)->get()->count();


        if($check==0){
            $user = [
                'fname'    => $request->fname,
                'lname'    => $request->lname,
                'name'     => trim($request->fname.' '.$request->lname),
                'email'    => $request->email,
                'mobile'   => $request->mobile,
                'login'    => $request->mobile,
                'password' => Hash::make($request->password),
                'role'     => 3,
                'is_verified'  => 'Y'
            ];
        
        $setting    = Setting::find(1);
        $mobile_no  = $request->mobile;
        // $otp        = rand(100000, 999999);
        $otp   = '123456';

        // Send SMS
        // $msg    = urlencode("Your one time password for Registration in ".$setting->site_title." is ".$otp." ");

        // $apiUrl = str_replace(["[message]", "[number]"], [$msg, $mobile_no], $setting->sms_api);
        
        session()->put('sms_otp', $otp);
        session()->put('mobile', $request->mobile);
        // $sms    = file_get_contents($apiUrl);
        
        session()->put('user_details', $user);
        
        return response()->json(['success'=>1, 'message' => 'Success!! Registration successful. Verify your account.']);
        }

        else{

            return response()->json(['success'=>0, 'message' => 'Error!! Account allready exist. Please login.']); 

        }   
        dd($user);
    }

    public function verifyOtp(Request $request)
    {
        $OTP = $request->session()->get('sms_otp');
        $mobile = $request->session()->get('mobile');
        $user = $request->session()->get('user_details');
        // dd($user);
       
        if($OTP == $request->otp){

            $newUser    = User::create($user);

            session()->forget('user_details');
            session()->forget('sms_otp');
            session()->forget('mobile');

        return response()->json(['success'=>1, 'message' => 'Success!!  Account verified. Please login.']);    
            
        } else {

            return response()->json(['success'=>0, 'message' => 'Error!!  OTP not match!.']);  

        }  
        
    }


    public function forgot(Request $request)
    {
        $check  =  User::where('mobile', $request->mobile)->get()->count();
        if($check==1){

        
        $setting    = Setting::find(1);
        $mobile_no  = $request->mobile;
        // $otp        = rand(100000, 999999);
        $otp = '123456';

        // Send SMS
        // $msg    = urlencode("Your one time password for Registration in ".$setting->site_title." is ".$otp." ");

        // $apiUrl = str_replace(["[message]", "[number]"], [$msg, $mobile_no], $setting->sms_api);

        session()->put('sms_otp', $otp);
        session()->put('mobile', $request->mobile);
        // $sms    = file_get_contents($apiUrl);
        
        return response()->json(['success'=>1, 'message' => 'Success!!  Enter OTP.']);    
        // return redirect(route('forgot-otp'))->with('success', 'Enter OTP.');
        }
        else {
        return response()->json(['success'=>0, 'message' => 'Error!!  Mobile no must not match.']);    
             // return redirect()->back()->with('danger', 'Mobile no must not match');
        }  
    }

    public function verifyForgotOtp(Request $request)
    {
        $OTP = $request->session()->get('sms_otp');
        $mobile = $request->session()->get('mobile');
        
        if($OTP == $request->otp){

           $obj =  User::where('mobile', $mobile)->first();
           
            session()->forget('sms_otp');
            
            
        return response()->json(['success'=>1, 'message' => 'Success!!  Enter OTP.']);    
        // return redirect(route('recoverpassword'));
        }
        else
        {
        return response()->json(['success'=>0, 'message' => 'Error!!  OTP not match.']);    
           // return redirect()->back()->with('danger', 'OTP not match.');
        }

    }

    public function save_password(Request $request)
    {
            dd($request);
        $rules = [
            'new_password'     => 'required|string|min:8|same:new_password',
            'confirm_password' => 'required|string|min:8|same:new_password'
        ];
        $request->validate($rules);

        $new_password = Hash::make($request->new_password);

        // $user = Auth()->user();
         $mobile = $request->session()->get('mobile');

           $obj =  User::where('mobile', $mobile)->first();
           $obj->password  = $new_password;
           $obj->save();
          
            session()->forget('mobile');

        return response()->json(['success'=>1, 'message' => 'Success!!  Your password has been changed successfully.']);    
        // return redirect(route('login'))->with('success', 'Your password has been changed successfully.');
    }
    public function removeCoupon()
    {
        session()->forget('coupon');
        $cart = session()->get('cart');
        $cartProducts = [];
        $totalPrice   = 0;
        if (!empty($cart)) {
            foreach ($cart as $pid => $pData) {
                $productInfo = \App\Model\Productdetail::find($pid);
                if (!empty($productInfo->id)) {
                    $productInfo->qty   = $pData['qty'];
                    $productInfo->is_jain_food   = $pData['is_jain_food'] ?? 0;
                    $cartProducts[]     = $productInfo;
                    $totalPrice        += $productInfo->sale_price * $pData['qty'];
                }
            }
        }
        $setting     = Setting::findOrFail(1);
        $cart_footer = view('frontend.template.cart_footer', compact('totalPrice', 'setting', 'cartProducts'))->render();

        $re = [
            'status'    => true,
            'message'   => 'Coupon removed.',
            'cart_footer'   => $cart_footer
        ];

        return response()->json($re, 200);
    }

    public function checkCoupon(Request $request)
    {
        $rules = [
            "coupon_code" => 'required|string'
        ];
        $request->validate($rules);

        $coupon = Coupon::where('code', $request->coupon_code);
        // print_r($coupon);
        if ($coupon->count() > 0) {
            $coupon_info = $coupon->first();
            // dd($coupon_info);
            $cart        = session()->get('cart');

            $is_valid    = true;

            $current_date = strtotime(date('Y-m-d'));
            $valid_from   = strtotime($coupon_info->valid_from);

            $user_limit     = Couponapply::where('uid', auth()->user()->id)->where('coupon', $coupon_info->id)->count();

            if ($user_limit >= $coupon_info->limit_per_user) {
                $is_valid = false;
                $re = [
                    'status'    => false,
                    'message'   => 'Coupon uses limit complete.'
                ];
            }

            if ($coupon_info->limit_user == 0) {
                $is_valid = false;
                $re = [
                    'status'    => false,
                    'message'   => 'Coupon limit complete.'
                ];
            }

            if ($valid_from > $current_date) {
                $is_valid = false;
                $re = [
                    'status'    => false,
                    'message'   => 'Coupon is not activated yet.'
                ];
            }
            
            if ($coupon_info->never_end == 0) {
                $valid_upto   = strtotime($coupon_info->valid_upto);

                if ($current_date > $valid_upto) {
                    $is_valid = false;
                    $re = [
                        'status'    => false,
                        'message'   => 'Coupon code has been expired.'
                    ];
                }
            }

            // if (!empty($coupon_info->menu_includes)) {
            //     $mids = explode(",", $coupon_info->menu_includes);
            //     $flag = 0;
            //     foreach ($cart as $pid => $c) {
            //         if (!in_array($pid, $mids)) {
            //             $flag = 1;
            //         }
            //     }
            //     if ($flag) {
            //         $is_valid = false;
            //     }
            // }

            // if (!empty($coupon_info->menu_excludes)) {
            //     $mids = explode(",", $coupon_info->menu_excludes);
            //     $flag = 0;
            //     foreach ($cart as $pid => $c) {
            //         if (in_array($pid, $mids)) {
            //             $flag = 1;
            //         }
            //     }
            //     if ($flag) {
            //         $is_valid = false;
            //     }
            // }

            if (!empty($coupon_info->min_cart_amt)) {

                $cart_total = 0;
                foreach ($cart as $pid => $c) {
                    $productInfo = Productdetail::find($pid);
                    $cart_total  += $productInfo->sale_price * $c['qty'];
                    // dd($cart_total);
                }

                if ($cart_total < $coupon_info->min_cart_amt) {
                    $is_valid = false;
                    $re = [
                        'status'    => false,
                        'message'   => 'Coupon is not valid for cart amount.'
                    ];
                }
            }

            $cart = session()->get('cart');
            $cartProducts = [];
            $totalPrice   = 0;
            if (!empty($cart)) {
                foreach ($cart as $pid => $pData) {
                    $productInfo = \App\Model\Productdetail::find($pid);
                    if (!empty($productInfo->id)) {
                        $productInfo->qty   = $pData['qty'];
                        $productInfo->is_jain_food   = $pData['is_jain_food'] ?? 0;
                        $cartProducts[]     = $productInfo;
                        $totalPrice        += $productInfo->sale_price * $pData['qty'];
                    }
                }
            }
            $setting     = Setting::findOrFail(1);

            if ($is_valid) {
                session()->put('coupon', $coupon_info);
                $cart_footer = view('frontend.template.cart_footer', compact('totalPrice', 'setting', 'cartProducts'))->render();
                // dd($cart_footer1);
                $re = [
                    'status'    => true,
                    'message'   => 'Coupon code is applied.',
                    'cart_footer'   => $cart_footer
                ];
            }
        } else {
            $re = [
                'status'    => false,
                'message'   => 'Coupon code is not available.'
            ];
        }

        return response()->json($re, 200);
    }

    public function contactInquiry(Request $request) {
        
        
        
         $data = new Inquiry();
         $data->name   = $request->name;
         $data->mobile = $request->mobile_no;
         $data->email = $request->email;
         $data->message = $request->message;
         
         // dd($data);
         
         
         if($data->save()){
               return response()->json(['status'=>1]);
         }
         else{
                return response()->json(['status'=>0]);
         }


    }
}