<?php

namespace App\Http\Controllers;
use App\Model\Project;


use Illuminate\Routing\Controller as BaseController;

class ProjectController extends BaseController {
    public function index() {
    	$project=Project::latest()->paginate(12);
    	$data=compact('project');
    	  	
    	
    	return view('frontend.inc.project',$data);
    }
}
