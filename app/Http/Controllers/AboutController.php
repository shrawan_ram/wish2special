<?php

namespace App\Http\Controllers;
use App\model\About;
use App\model\Setting;

use Illuminate\Routing\Controller as BaseController;

class AboutController extends BaseController {
    public function index() {

    	$about=About::findOrFail(1);
    	$setting=Setting::findOrFail(1);
    	$data=compact('about','setting');
    	  	
    	
    	return view('frontend.inc.about-us',$data);
    }
}
