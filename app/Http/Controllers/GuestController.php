<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Validator;
use Log;
use App\model\Product;
use App\model\Productdetail;
use App\model\Setting;
use App\model\Order;
use App\model\Address;


class GuestController extends BaseController
{
    public function index()
    {
        $cart = session()->get('cart');

        $cartProducts = [];
        $totalPrice   = 0;

        if (!empty($cart)) {
            foreach ($cart as $pid => $pData) {
                $productInfo = \App\model\Productdetail::find($pid);
                if (!empty($productInfo->id)) {
                    $productInfo->qty   = $pData['qty'];
                    $cartProducts[]     = $productInfo;
                    $totalPrice        += $productInfo->sale_price * $pData['qty'];
                }
            }
        }

        $setting = Setting::findOrFail(1);

        $data = compact('cartProducts', 'totalPrice', 'setting');

        return view('frontend.inc.guest', $data);
    }

    public function save(Request $request)
    {
        $rules = [
            'order'                 => 'required|array',
            'order.billing_fname'   => 'required|regex:/[A-Za-z ]+/',
            'order.billing_lname'   => 'regex:/[A-Za-z ]*/',
            'order.billing_email'   => 'required|email',
            'order.billing_phone'   => 'required|regex:/\d{10}/',
            'billing.country' => 'required',
            'billing.state'   => 'required',
            'billing.city'    => 'required',
            'billing.pincode' => 'required',
        ];
        $request->validate($rules);
        // dd(request('order'));
        $OTP = $request->session()->get('sms_otp');
  

        $currentMonth   = date('n', time());
        $financialYear  = $currentMonth > 3 ? date('Y')."-".(date('y') + 1) : (date('Y') - 1)."-".date('y');

        $maxInvoice     = Order::where('financial_year', $financialYear)->max('invoice_no');
        $invocieNo      = $maxInvoice + 1;

        $orderArr       = request('order');
        // dd($orderArr);

        $billingArr       = request('billing');
        $billing = Address::create($billingArr);
        $orderArr['billing_addressid'] = $billing->id;

        

        $orderArr['financial_year'] = $financialYear;
        $orderArr['invoice_no']     = $invocieNo;
        $orderArr['uid'] = '2';

        $order = Order::create($orderArr);

       
        // Order Products
        $cart = session()->get('cart');
        
        if (!empty($cart)) {
            foreach ($cart as $pid => $pData) {
                $productInfo = \App\model\Product::find($pid);
                if (!empty($productInfo->id)) {
                     $productInfo->qty   = $pData['qty'];
                    
                    $isJainFood = $pData['is_jain_food'] ? "Yes" : "No";

                    $menuArr = [
                        'menu_id'   => $pid,
                        'qty'       => $pData['qty'],
                        'price'     => $productInfo->sale_price,
                        'jain_food' => $isJainFood,
                    ];
                    
                    $order->products()->create($menuArr);
                }
            }
        }

        session()->forget('cart');


        return redirect(url('guest/thank_you'));
        // return response()->json(['success'=>1, 'id'=>$order->id]);

        
    }
    public function thank_you()
    {
        $setting = Setting::findOrFail(1);
        $data    = compact('setting');
        
         return view('frontend.inc.thankyoug',$data);
        // dd($orderInfo);
    }
}
