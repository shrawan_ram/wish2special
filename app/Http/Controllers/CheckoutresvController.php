<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Validator;

use App\model\MenuItem;
use App\model\Setting;
use App\model\Resvorder;
use App\model\Reservations;
use App\User;

use Hash;

class CheckoutresvController extends BaseController
{
    public function index()
    {
        $cart = session()->get('cart_resv');
        $cartProducts = [];
        $totalPrice   = 0;

        if (!empty($cart)) {
            foreach ($cart as $pid => $pData) {
                $productInfo = \App\model\MenuItem::with('media')->find($pid);
                if (!empty($productInfo->id)) {
                    $productInfo->qty   = $pData['qty'];
                    $cartProducts[]     = $productInfo;
                    $totalPrice        += $productInfo->sale_price * $pData['qty'];
                }
            }
        }

        $setting = Setting::findOrFail(1);

        $data = compact('cartProducts', 'totalPrice', 'setting');

        return view('frontend.inc.checkout-resv', $data);
    }

    public function save(Request $request)
    {   
        $cart = session()->get('cart_resv');
        $cartProducts = [];
        $totalPrice   = 0;

        if (!empty($cart)) {
            foreach ($cart as $pid => $pData) {
                $productInfo = \App\model\MenuItem::with('media')->find($pid);
                if (!empty($productInfo->id)) {
                    $productInfo->qty   = $pData['qty'];
                    $cartProducts[]     = $productInfo;
                    $totalPrice        += $productInfo->sale_price * $pData['qty'];
                }
            }
        }
        
        $currentMonth   = date('n', time());
        $financialYear  = $currentMonth > 3 ? date('Y')."-".(date('y') + 1) : (date('Y') - 1)."-".date('y');

        $maxInvoice     = Resvorder::where('financial_year', $financialYear)->max('invoice_no');
        $invocieNo      = $maxInvoice + 1;

        $reservation_id = session()->get('reservation_id');
        // dd($reservation_id);
        $resvorderArr       = request('resvorder');
        $resvorderArr['financial_year'] = $financialYear;
        $resvorderArr['invoice_no']     = $invocieNo;
        $resvorderArr['total_amount']     = $totalPrice;
        $resvorderArr['invoice_no']     = $invocieNo;
        
        if($resvorderArr['payment_mode'] == 'COD'){
            $resvorderArr['payment_status'] = 'Due';
        }
        if($resvorderArr['payment_mode'] == 'RazorPay'){
            $resvorderArr['payment_status'] = 'Paid';
        }
        
        $resvorderArr['rid']     = $reservation_id;
        $resvorder = Resvorder::create($resvorderArr);
       
        // Order Products
        $cart = session()->get('cart_resv');
        
        if (!empty($cart)) {
            foreach ($cart as $pid => $pData) {
                $productInfo = \App\model\MenuItem::with('media')->find($pid);
                if (!empty($productInfo->id)) {
                     $productInfo->qty   = $pData['qty'];
                    
                    $isJainFood = $pData['is_jain_food'] ? "Yes" : "No";

                    $menuArr = [
                        'menu_id'   => $pid,
                        'qty'       => $pData['qty'],
                        'price'     => $productInfo->sale_price,
                        'jain_food' => $isJainFood,
                    ];
                    
                    $resvorder->products()->create($menuArr);
                }
            }
        }

        session()->forget('cart_resv');
        // session()->forgot('reservation_id');

        // return redirect(url('checkout_resv/thank-you'));
        return response()->json(['foo'=>'bar', 'id'=>$resvorder->id]);

    }
    
    public function thank_you()
    {
        
         return view('frontend.inc.thankyou_resv');
        // dd($orderInfo);
    }
}
