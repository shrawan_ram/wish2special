<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\model\Setting;
use App\model\Inquiry;


use Illuminate\Routing\Controller as BaseController;

class ContactController extends BaseController {
    public function index() {
    	  	
    	$setting=Setting::findOrFail(1);

    	$data=compact('setting');
    	return view('frontend.inc.contact',$data);
    }
    public function save(Request $request)
    {
        //
        $rules = [
            'name'             => 'required',            
                ];
            // $file = $request->file('image','image_hover');
                   
        $request->validate( $rules );
        $obj = new Inquiry;
        $obj->name      = $request->name;
        $obj->email     = $request->email;
        $obj->mobile    = $request->mobile;       
        $obj->message   = $request->message;

        
        $obj->save();

        return redirect()->back()->with('success', 'Successful');
    }
}
