<?php

namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\Faq;
class FaqsController extends Controller{
   
    public function index(request $request){
        //
        $query = Faq::latest();

        if( !empty( $request->question ) ) {
            $query->where('question', 'LIKE', '%'.$request->question.'%');
        }
        $faqs = $query->paginate(20);

        $data = compact( 'faqs' ); // Variable to array convert
        return view('backend.inc.faqs.index', $data);
    }

    

   
    public function add()
    {
        
        return view('backend.inc.faqs.add');
    }

    
    public function addData(Request $request)
    {
        //
        $rules = [
            'question'        => 'required',
            'answer'        => 'required'
            
            
        ];
        $request->validate( $rules );
       
        $obj = new Faq;
        $obj->question        = $request->question;
        $obj->answer          = $request->answer;
        
        // $obj->image  = $request->$file->getClientOriginalName();
        
        $obj->save();

        return redirect( url('admin-control/faqs/') )->with('success', 'Success! New record has been added.');
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit = Faq::findOrFail( $id )->toArray();
        $request->replace($edit);
        $request->flash();

        

        return view('backend.inc.faqs.edit');
    }

    
    public function editData(Request $request, $id)
    {
        //
        $rules = [
            'question'        => 'required',
            'answer'        => 'required'
            
        ];
        $request->validate( $rules );
        $obj = Faq::findOrFail( $id );
        $obj->question        = $request->question;
        $obj->answer          = $request->answer;
        
        // $obj->image  = $request->$file->getClientOriginalName();
        $obj->save();

        return redirect( url('admin-control/faqs') )->with('success', 'Success! A record has been updated.');
    }
     public function remove(  $id ){
         
        $social = Faq::findOrFail($id);

        $social->delete();


        return back();
    }

    public function removeMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'checked' => 'required',
		]);

		if ($validator->fails()) {

			return back()->with('deleted', 'Please select one of them to delete');
		}

		foreach ($request->checked as $checked) {

			$this->remove($checked);
			
		}

		return back()->with('deleted', 'Faqs has been deleted');
    }

   
}
