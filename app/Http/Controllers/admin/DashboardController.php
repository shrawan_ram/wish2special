<?php

namespace App\Http\Controllers\admin;
use App\model\Category;
use App\model\Blog;
use App\model\Product;
use App\model\Setting;
use App\model\Slider;
use App\model\Faq;
use App\User;

use Illuminate\Routing\Controller;

class DashboardController extends Controller {
    public function index() {
    	$category_count=Category::count();
    	$blog_count=Blog::count();
    	$product_count=Product::count();
    	$slider_count=Slider::count();
    	$faq_count=Faq::count();
    	$user_count=User::count();
    	$data=compact('category_count','blog_count','product_count','slider_count','faq_count','user_count');
        return view('backend.inc.dashboard',$data);
    }
}
