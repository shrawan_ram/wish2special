<?php

namespace App\Http\Controllers\admin;

use Intervention\Image\ImageManagerStatic as Image;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\Plan;

class PlanController extends Controller{
   
    public function index(request $request){

        $query = Plan::latest();

        if( !empty( $request->title ) ) {
            $query->where('title', 'LIKE', '%'.$request->title.'%');
        }


        $lists1 = $query->paginate(20);        

        $data = compact( 'lists1' ); // Variable to array convert
        return view('backend.inc.plan.index', $data);
    }
    

    

   
    public function add()
    {
        //

        
        
        return view('backend.inc.plan.add');
    }

    
    public function addData(Request $request)
    {
        //
        $rules = [
            'plan'             => 'required',            
                ];
          
           
        $request->validate( $rules );
        $obj = new Plan;
        $obj->plan             = $request->plan;
        $obj->price             = $request->price;
        $obj->title             = $request->title;
        $obj->description       = $request->description;
        $obj->save();
        return redirect( url('admin-control/plan/') )->with('success', 'Success! New record has been added.');
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit = Plan::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();
        
        


        $lists1 = Plan::latest()->paginate(20);
        //
        

        

        $data = compact( 'lists1','edit');

        return view('backend.inc.plan.edit',$data);
    }

    
    public function editData(Request $request, $id)
    {
        //
        $rules = [
            'plan'          => 'required',
            
            
        ];
        $request->validate( $rules );
        

        $obj = Plan::findOrFail( $id );
        $obj->plan             = $request->plan;
        $obj->price             = $request->price;
        $obj->title             = $request->title;
        $obj->description       = $request->description;
        
        
        
        $obj->save();

        return redirect( url('admin-control/plan') )->with('success', 'Success! A record has been updated.');
    }
     public function remove(  $id ){
        $remove = Plan::where('id',$id)->delete();
        return redirect( url('admin-control/plan') )->with('success', 'Success! A record has been deleted.');   
    }

    public function removeMultiple(Request $request)
    {
        $check = $request->check; // input type="checkbox" name="check[]"
        Plan::whereIn("id", $check)->delete(); // DELETE FROM news WHERE news_id IN (3,5,4)

        return redirect()->back()->with('success', 'Item(s) removed.');
    }

   
}
