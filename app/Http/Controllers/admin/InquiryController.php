<?php

namespace App\Http\Controllers\admin;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\model\Inquiry;
use App\User;

class InquiryController extends Controller{
   
    public function index(request $request){

        $query = Inquiry::latest();

        if( !empty( $request->mobile_no ) ) {
            $query->where('mobile_no', 'LIKE', '%'.$request->mobile_no.'%');
        }


        $inquiry = $query->paginate(20);
        //
        

        

        $data = compact( 'inquiry' ); // Variable to array convert
        return view('backend.inc.inquiry', $data);
    }
    public function mail(request $request){

        $users = User::get();
        

        $data = compact('users'); // Variable to array convert
        return view('backend.inc.mail', $data);
    }
    public function mailsend(Request $request)
    {
        $email = $request->email;
        // dd($email);
        // print_r($email);
        // $email = implode(',', $email);
        // $input['email']     = $email;
        // $emails = $input['email'];
        // $emails = explode(",", $email[0]);
        // dd($emails);
        $subject = $request->subject;
        $msg     = $request->message;
        // dd()
        foreach ($email as $e) {

        $data = array('name' => "Suncity Techno", 'subject' => $subject, 'msg' => $msg, 'email' => $e);

          Mail::send('email.send-mail', $data, function($message) use ($data) {
             $message->to($data['email'], $data['name'])->subject($data['subject']);
             $message->from('support@wish2special.com', 'Wish2special');
          }); 
        }          
        return redirect()->back()->with('success', 'Message Send Successful.');
    }

     public function remove(  $id ){
         
        $social = Inquiry::findOrFail($id);

        $social->delete();


        return back();
    }

    public function removeMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'checked' => 'required',
        ]);

        if ($validator->fails()) {

            return back()->with('deleted', 'Please select one of them to delete');
        }

        foreach ($request->checked as $checked) {

            $this->remove($checked);
            
        }

        return back()->with('deleted', 'Inquiry has been deleted');
    }
    
}