<?php

namespace App\Http\Controllers\admin;

use Intervention\Image\ImageManagerStatic as Image;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\model\Marquee;

class MarqueeController extends Controller{
   
    public function index(request $request){

        $query = Marquee::latest();

        if( !empty( $request->title ) ) {
            $query->where('title', 'LIKE', '%'.$request->title.'%');
        }


        $lists1 = $query->paginate(20);        

        $data = compact( 'lists1' ); // Variable to array convert
        return view('backend.inc.marquee-img.index', $data);
    }
    

    

   
    public function add()
    {
       
        return view('backend.inc.marquee-img.add');
    }

    
    public function addData(Request $request)
    {
        //
        $rules = [
            'title'             => 'required',            
                ];
            // $file = $request->file('image','image_hover');
                   
        $request->validate( $rules );
        $obj = new Marquee;
        $obj->title             = $request->title;

        if($request->hasFile('image'))  { 
            
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(115, 64);
            $image_resize->save(public_path('imgs/marquee/' .$filename));
            $obj->image   = $image->getClientOriginalName();
            // $banner_resize = Image::make($image->getRealPath());
            // $banner_resize->resize(750,500);
            // $banner_resize->save(public_path('imgs/blogs/original/' .$filename));
        }
        // $obj->image  = $request->$file->getClientOriginalName();
        
        $obj->save();

        return redirect( url('admin-control/marquee-img/') )->with('success', 'Success! New record has been added.');
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit = Marquee::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();
        $lists1 = Marquee::latest()->paginate(20);
        //
        

        

        $data = compact( 'lists1','edit');

        return view('backend.inc.marquee-img.edit',$data);
    }

    
    public function editData(Request $request, $id)
    {
        //
        $rules = [
            'title'          => 'required',
            
            
        ];
        $request->validate( $rules );
        

        $obj = Marquee::findOrFail( $id );
        $obj->title           = $request->title;
        
        if($request->hasFile('image'))  { 
            
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(115, 64);
            $image_resize->save(public_path('imgs/marquee/' .$filename));
            $obj->image   = $image->getClientOriginalName();
            // $banner_resize = Image::make($image->getRealPath());
            // $banner_resize->resize(750,500);
            // $banner_resize->save(public_path('imgs/blogs/original/' .$filename));
        }
        
        $obj->save();

        return redirect( url('admin-control/marquee-img') )->with('success', 'Success! A record has been updated.');
    }
     public function remove(  $id ){
        $remove = Marquee::where('id',$id)->delete();
        return redirect( url('admin-control/marquee-img') )->with('success', 'Success! A record has been deleted.');   
    }

    public function removeMultiple(Request $request)
    {
        $check = $request->check; // input type="checkbox" name="check[]"
        Marquee::whereIn("id", $check)->delete(); // DELETE FROM news WHERE news_id IN (3,5,4)

        return redirect()->back()->with('success', 'Item(s) removed.');
    }

   
}
