<?php

namespace App\Http\Controllers\admin;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\model\Product;
use App\model\Category;
use App\model\Vendor;

class ProductController extends Controller{
   
    public function index(request $request){

        $query = Product::latest()->withCount('p')->withCount('order')->with('category');

        if( !empty( $request->title ) ) {
            $query->where('title', 'LIKE', '%'.$request->title.'%');
        }


        $product = $query->get();

        $data = compact( 'product' ); // Variable to array convert
        return view('backend.inc.product.index', $data);
    }
    

    

   
    public function add()
    {
        //

        $categories = Category::get();
        $parentArr = [
            ''  => 'Select Category'
        ];

        foreach($categories as $c) {
            $parentArr[ $c->id ] = $c->title;
        }
        $vendors = Vendor::get();
        $vendorArr = [
            ''  => 'Select Vendor'
        ];

        foreach($vendors as $c) {
            $vendorArr[ $c->id ] = $c->shop_name;
        }
        $data = compact('parentArr', 'vendorArr');
        return view('backend.inc.product.add',$data);
    }

    
    public function addData(Request $request)
    {
        //
        $rules = [
            'title'             => 'required',
           

                ];
            
        $request->validate( $rules );
        
        $obj = new Product;
        $obj->title              = $request->title;
        $obj->slug               = $request->slug == '' ? Str::slug($request->title) : Str::lower($request->slug);
        $obj->excerpt            = $request->excerpt;
        $obj->description        = $request->description;        
        $obj->category_id        = $request->category_id;
        $obj->vendor_id          = $request->vendor_id;
        // $obj->regular_price      = $request->regular_price;
        // $obj->discount           = $request->discount;
        // $obj->sale_price         = $request->sale_price;
        $obj->seo_title          = $request->seo_title;
        $obj->seo_keywords       = $request->seo_keywords;
        $obj->seo_description    = $request->seo_description;
        
       
        
        



        if($request->hasFile('image'))  
        { 
            $image        = $request->file('image');
            $filename     = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(390, 260);
            $image_resize->save(public_path('imgs/product/' .$filename));
            $obj->image   = $image->getClientOriginalName();
        }
        
        // $obj->image  = $request->$file->getClientOriginalName();
        
        $obj->save();

        return redirect( url('admin-control/product/') )->with('success', 'Success! New record has been added.');
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit = Product::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();
        $categories = Category::get();
        $parentArr = [
            ''  => 'Select Category'
        ];

        foreach($categories as $c) {
            $parentArr[ $c->id ] = $c->title;
        }
        $vendors = Vendor::get();
        $vendorArr = [
            ''  => 'Select Vendor'
        ];

        foreach($vendors as $c) {
            $vendorArr[ $c->id ] = $c->shop_name;
        }

        $lists1 = Product::latest()->paginate(20);
        //
        

        

        $data = compact( 'lists1','edit','parentArr', 'vendorArr');

        return view('backend.inc.product.edit',$data);
    }

    
    public function editData(Request $request, $id)
    {
        //
        $rules = [
            
            
        ];
        $request->validate( $rules );
        

        $obj = Product::findOrFail( $id );
        $obj->title              = $request->title;
        $obj->slug               = $request->slug == '' ? Str::slug($request->title) : Str::lower($request->slug);
        $obj->excerpt            = $request->excerpt;
        $obj->description        = $request->description; 
        $obj->vendor_id          = $request->vendor_id; 
        // $obj->regular_price      = $request->regular_price;
        // $obj->discount           = $request->discount;
        // $obj->sale_price         = $request->sale_price; 
        $obj->category_id        = $request->category_id;
        $obj->seo_title          = $request->seo_title;
        $obj->seo_keywords       = $request->seo_keywords;
        $obj->seo_description    = $request->seo_description;       

        if($request->hasFile('image'))  
        { 
            $image        = $request->file('image');
            $filename     = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(950, 575);
            $image_resize->save(public_path('imgs/product/' .$filename));
            $obj->image   = $image->getClientOriginalName();
        }
        
        // $weight = $request->input('weight');        
        // $weight = implode(',', $weight);        
        // $obj['weight'] = $weight;

        //  $regular_price = $request->input('regular_price');        
        // $regular_price = implode(',', $regular_price);        
        // $obj['regular_price'] = $regular_price;

        // $discount = $request->input('discount');        
        // $discount = implode(',', $discount);        
        // $obj['discount'] = $discount;

        // $sale_price = $request->input('sale_price');        
        // $sale_price = implode(',', $sale_price);        
        // $obj['sale_price'] = $sale_price;

        // $unit = $request->input('unit');        
        // $unit = implode(',', $unit);        
        // $obj['unit'] = $unit;
        
        // $obj->image  = $request->$file->getClientOriginalName();
        $obj->save();

        return redirect( url('admin-control/product/') )->with('success', 'Success! New record has been added.');
    }
     public function remove(  $id ){
         
        $social = Product::findOrFail($id);

        $social->delete();


        return back();
    }

    public function removeMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'checked' => 'required',
		]);

		if ($validator->fails()) {

			return back()->with('deleted', 'Please select one of them to delete');
		}

		foreach ($request->checked as $checked) {

			$this->remove($checked);
			
		}

		return back()->with('deleted', 'Products has been deleted');
    }
     

   
}
