<?php

namespace App\Http\Controllers\admin;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\About;




class AboutController extends Controller {
    public function index(request $request) {

        $lists = About::latest()->paginate();
        

        $data = compact( 'lists' ); // Variable to array convert
        return view('backend.inc.about.index', $data);
    }


   
    public function edit( Request $request, $id ) {

        $edit = About::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();

        return view('backend.inc.about.edit', compact('edit'));

    }
    public function editData( Request $request, $id ) {
        $rules = [
            'title'        => 'required'
            
        ];
        $request->validate( $rules );
        

        $obj = About::findOrFail( $id );
        $obj->title         = $request->title;       
        $obj->description      = $request->description;
        $obj->seo_title      = $request->seo_title;
        $obj->seo_keywords      = $request->seo_keywords;
        $obj->seo_description      = $request->seo_description;
        

        if($request->hasFile('image'))  { 
            
            $image        = $request->file('image');
            $filename     = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(445, 450);
            $image_resize->save(public_path('imgs/about/' .$filename)); 
            $obj->image   = $image->getClientOriginalName();     
        }
        $obj->save();

        return redirect()->back()->with('success', 'Success! A record has been updated.');
    }
    

// SELECT * FROM `news` WHERE `news_id` = 1 or `news_id` = 2
    // News::where('news_id', 1)->orWhere('news_id', 2)->get();
}
