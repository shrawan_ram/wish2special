<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

// use Hash;
use Auth;

class UserController extends Controller {
    public function login() {
        // echo Hash::make( 'admin123' ); die;
    
        return view('backend.inc.login');
    }

    public function logout() {
        Auth::logout();

        return redirect( url('admin-control') )->with('success', 'You\'ve logged out');
    }
    public function checklogin(Request $request)
    {
        $rules = [
            "login"       => "required",
            "password"    => "required",
        ];
        $request->validate($rules);

        $user_data= array(
            'login'     => $request->login,
            'password'  => $request->password
        );
        $is_remembered = !empty($request->remember_me) ? true : false;
        if (Auth::guard('admin')->attempt($user_data, $is_remembered)) {
            return redirect(route('admin_home'));
        } else {
            // dd($request->all());
            return redirect()->back()->with('error', 'Credentials not matched.');
        }
    }

    public function auth( Request $request ) {


        $remember = ($request->has('remember')) ? true : false;


        $rules = [
            'login'    => 'required',
            'password'  => 'required|min:6|max:20'
        ];
        $request->validate( $rules );

        $userData = [
            'login'    => $request->login,
            'password'  => $request->password
        ];
        



        if( Auth::attempt( $userData, $remember ) ) {
            return redirect( url('admin-control/dashboard') );
        } else {
            return redirect( url('admin-control') )->with('danger', 'Credentials is not matched.');
        }
    }
    
}
