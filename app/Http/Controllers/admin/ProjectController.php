<?php

namespace App\Http\Controllers\admin;

use Intervention\Image\ImageManagerStatic as Image;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\model\Project;
use App\model\Category;

class ProjectController extends Controller{
   
    public function index(request $request){

        $query = Project::latest();

        if( !empty( $request->title ) ) {
            $query->where('title', 'LIKE', '%'.$request->title.'%');
        }


        $lists1 = $query->paginate(20);        

        $data = compact( 'lists1' ); // Variable to array convert
        return view('backend.inc.project.index', $data);
    }
    

    

   
    public function add()
    {
        //

        $categories = Category::get();
        $parentArr = [
            ''  => 'Select Category'
        ];

        foreach($categories as $c) {
            $parentArr[ $c->id ] = $c->category;
        }
        $data = compact('parentArr');
        return view('backend.inc.project.add',$data);
    }

    
    public function addData(Request $request)
    {
        //
        $rules = [
            'title'             => 'required',
           

                ];
            
        $request->validate( $rules );
        
        $obj = new Project;
        $obj->title              = $request->title;
        $obj->slug               = $request->slug == '' ? Str::slug($request->title) : Str::lower($request->slug);
        $obj->short_description  = $request->short_description;
        $obj->description        = $request->description;        
        $obj->parent             = $request->parent;
        $obj->seo_title          = $request->seo_title;
        $obj->keywords           = $request->keywords;
        $obj->seo_description    = $request->seo_description;


        if($request->hasFile('image'))  
        { 
            $image        = $request->file('image');
            $filename     = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(390, 260);
            $image_resize->save(public_path('imgs/project/' .$filename));
            $obj->image   = $image->getClientOriginalName();
        }
            
        if($request->hasFile('image_hover'))  
        { 
            $image2            = $request->file('image_hover');
            $filename          = $image2->getClientOriginalName('');
            $banner_resize     = Image::make($image2->getRealPath());              
            $banner_resize->resize(420,280);
            $banner_resize->save(public_path('imgs/project/' .$filename));
            $obj->image_hover = $image2->getClientOriginalName();
        }
        
        if($request->hasFile('banner_image'))  { 
            $image3             = $request->file('banner_image');
            $filename           = $image3->getClientOriginalName('');
            $banner_resize      = Image::make($image3->getRealPath());              
            $banner_resize->resize(420,280);
            $banner_resize->save(public_path('imgs/project/' .$filename));
            $obj->banner_image  = $image3->getClientOriginalName();
        }
        
        
        // $obj->image  = $request->$file->getClientOriginalName();
        
        $obj->save();

        return redirect( url('admin-control/project/') )->with('success', 'Success! New record has been added.');
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit = Project::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();
        $categories = Category::get();
        $parentArr = [
            ''  => 'Select Category'
        ];

        foreach($categories as $c) {
            $parentArr[ $c->id ] = $c->category;
        }
        


        $lists1 = Project::latest()->paginate(20);
        //
        

        

        $data = compact( 'lists1','edit','parentArr' );

        return view('backend.inc.project.edit',$data);
    }

    
    public function editData(Request $request, $id)
    {
        //
        $rules = [
            
            
        ];
        $request->validate( $rules );
        

        $obj = Project::findOrFail( $id );
        $obj->title              = $request->title;
        $obj->slug               = $request->slug;
        $obj->short_description  = $request->short_description;
        $obj->description        = $request->description;
        $obj->parent             = $request->parent;
        $obj->seo_title          = $request->seo_title;
        $obj->keywords           = $request->keywords;
        $obj->seo_description    = $request->seo_description;
        $obj->slug               = $request->slug == '' ? Str::slug($request->title) : Str::lower($request->slug); 

        if($request->hasFile('image'))  
        { 
            $image        = $request->file('image');
            $filename     = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(390, 260);
            $image_resize->save(public_path('imgs/project/' .$filename));
            $obj->image   = $image->getClientOriginalName();
        }
            
        if($request->hasFile('image_hover'))  
        { 
            $image2            = $request->file('image_hover');
            $filename          = $image2->getClientOriginalName('');
            $banner_resize     = Image::make($image2->getRealPath());              
            $banner_resize->resize(420,280);
            $banner_resize->save(public_path('imgs/project/' .$filename));
            $obj->image_hover = $image2->getClientOriginalName();
        }
        
        if($request->hasFile('banner_image'))  { 
            $image3             = $request->file('banner_image');
            $filename           = $image3->getClientOriginalName('');
            $banner_resize      = Image::make($image3->getRealPath());              
            $banner_resize->resize(420,280);
            $banner_resize->save(public_path('imgs/project/' .$filename));
            $obj->banner_image  = $image3->getClientOriginalName();
        }
        
        // $obj->image  = $request->$file->getClientOriginalName();
        $obj->save();

        return redirect( url('admin-control/project') )->with('success', 'Success! A record has been updated.');
    }
     public function remove(  $id ){
        $remove = Project::where('id',$id)->delete();
        return redirect( url('admin-control/project') )->with('success', 'Success! A record has been deleted.');   
    }

    public function removeMultiple(Request $request)
    {
        $check = $request->check; // input type="checkbox" name="check[]"
        Project::whereIn("id", $check)->delete(); // DELETE FROM news WHERE news_id IN (3,5,4)

        return redirect()->back()->with('success', 'Item(s) removed.');
    }

   
}
