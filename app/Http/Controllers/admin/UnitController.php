<?php

namespace App\Http\Controllers\admin;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\model\Unit;
class UnitController extends Controller{
   
    public function index(request $request){
        //
        $query = Unit::withCount('')->latest();

        if( !empty( $request->title ) ) {
            $query->where('title', 'LIKE', '%'.$request->title.'%');
        }
        $unit = $query->paginate(20);

        $data = compact( 'unit' ); // Variable to array convert
        return view('backend.inc.unit.index', $data);
    }   
    public function add()
    {
        
        return view('backend.inc.unit.add');
    }

    
    public function addData(Request $request)
    {
        //
        $rules = [
            'title'        => 'required'           
        ];
        $request->validate( $rules );
        $obj = new Unit;
        $obj->title       = $request->title;       
        $obj->short_name      = $request->short_name; 

    

        $obj->save();
        return redirect( url('admin-control/unit/') )->with('success', 'Success! New record has been added.');
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit = Unit::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();

        
        $data = compact('edit');
        return view('backend.inc.unit.edit', $data);
    }

    
    public function editData(Request $request, $id)
    {
        //
        $rules = [
            'title'        => 'required'
            
        ];
        
        $request->validate( $rules );
        $obj = Unit::findOrFail( $id ); 
        $obj->title       = $request->title;
        $obj->short_name  = $request->short_name;

        
        
        // $obj->image  = $request->$file->getClientOriginalName();
        $obj->save();

        return redirect( url('admin-control/unit') )->with('success', 'Success! A record has been updated.');
    }
     public function remove(  $id ){
         
        $social = Unit::findOrFail($id);

        $social->delete();


        return back();
    }

    public function removeMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'checked' => 'required',
		]);

		if ($validator->fails()) {

			return back()->with('deleted', 'Please select one of them to delete');
		}

		foreach ($request->checked as $checked) {

			$this->remove($checked);
			
		}

		return back()->with('deleted', 'Category has been deleted');
    }
    
}
