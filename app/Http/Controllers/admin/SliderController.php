<?php

namespace App\Http\Controllers\admin;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\model\Slider;

use App\model\Category;

class SliderController extends Controller{
   
    public function index(request $request){

        $query = Slider::latest();

        if( !empty( $request->title ) ) {
            $query->where('title', 'LIKE', '%'.$request->title.'%');
        }


        $slider = $query->paginate(20);        

        $data = compact( 'slider' ); // Variable to array convert
        return view('backend.inc.slider.index', $data);
    }
    

    

   
    public function add()
    {
        // $categories = Category::get();
        // $parentArr = [
        //     ''  => 'Select Category'
        // ];

        // foreach($categories as $c) {

        //     $parentArr[ $c->id ] = $c->category;
        // }
        // $data = compact('parentArr');
        return view('backend.inc.slider.add');
    }

    
    public function addData(Request $request)
    {
        //
        $rules = [
            'title'             => 'required',            
                ];
            // $file = $request->file('image','image_hover');
         
        $request->validate( $rules );
        $obj = new Slider;
        $obj->title             = $request->title;
        $obj->description       = $request->description;

        if($request->hasFile('image'))  
        { 
            $image        = $request->file('image');
            $filename     = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(1440, 762);
            $image_resize->save(public_path('imgs/slider/' .$filename));
            $obj->image   = $image->getClientOriginalName();
        }
        
        
        // $obj->image  = $request->$file->getClientOriginalName();
        
        $obj->save();

        return redirect( url('admin-control/slider/') )->with('success', 'Success! New record has been added.');
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit = Slider::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();
        
        $lists1 = Slider::latest()->paginate(20);

        $data = compact( 'lists1','edit' );

        return view('backend.inc.slider.edit',$data);
    }

    
    public function editData(Request $request, $id)
    {
        $rules = [
            'title'          => 'required',           
            
        ];
        $request->validate( $rules );
        

        $obj = Slider::findOrFail( $id );
        $obj->title             = $request->title;
        $obj->description       = $request->description;
        
        if($request->hasFile('image'))  { 
            
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(1440, 762);
            $image_resize->save(public_path('imgs/slider/' .$filename));
            $obj->image   = $image->getClientOriginalName();
            
        }
        
        $obj->save();

        return redirect( url('admin-control/slider') )->with('success', 'Success! A record has been updated.');
    }
     public function remove(  $id ){
         
        $social = Slider::findOrFail($id);

        $social->delete();


        return back();
    }

    public function removeMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'checked' => 'required',
		]);

		if ($validator->fails()) {

			return back()->with('deleted', 'Please select one of them to delete');
		}

		foreach ($request->checked as $checked) {

			$this->remove($checked);
			
		}

		return back()->with('deleted', 'Category has been deleted');
    }

   
}
