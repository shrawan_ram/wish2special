<?php

namespace App\Http\Controllers\admin;

use Intervention\Image\ImageManagerStatic as Image;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\model\Blog;
use Illuminate\Support\Facades\Validator;
use App\model\Category;

class BlogController extends Controller{
   
    public function index(request $request){

        $query = Blog::latest()->with('cat');

        if( !empty( $request->title ) ) {
            $query->where('title', 'LIKE', '%'.$request->title.'%');
        }


        $blog = $query->paginate(20);        

        $data = compact( 'blog' ); // Variable to array convert
        return view('backend.inc.blog.index', $data);
    }
    

    

   
    public function add()
    {
        //

        $categories = Category::get();
        $parentArr = [
            ''  => 'Select Category'
        ];

        foreach($categories as $c) {

            $parentArr[ $c->id ] = $c->title;
        }
        $data = compact('parentArr');
        return view('backend.inc.blog.add',$data);
    }

    
    public function addData(Request $request)
    {
        //
        $rules = [
            'title'             => 'required',            
                ];
            // $file = $request->file('image','image_hover');
                   
        $request->validate( $rules );
        $obj = new Blog;
        $obj->title             = $request->title;
        $obj->slug              = $request->slug == '' ? Str::slug($request->title) : Str::lower($request->slug);
        $obj->excerpt           = $request->excerpt;
        $obj->description       = $request->description;       
        $obj->cid               = $request->cid;
        $obj->seo_title         = $request->seo_title;
        $obj->seo_keywords      = $request->seo_keywords;
        $obj->seo_description   = $request->seo_description;

        if($request->hasFile('image'))  { 
            
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(750, 500);
            $image_resize->save(public_path('imgs/blogs/' .$filename));
            $obj->image   = $image->getClientOriginalName();
            // $banner_resize = Image::make($image->getRealPath());
            // $banner_resize->resize(750,500);
            // $banner_resize->save(public_path('imgs/blogs/original/' .$filename));
        }
        
        
        // $obj->image  = $request->$file->getClientOriginalName();
        
        $obj->save();

        return redirect( url('admin-control/blog/') )->with('success', 'Success! New record has been added.');
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit = Blog::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();
        $categories = Category::get();
        $parentArr = [
            ''  => 'Select Category'
        ];

        foreach($categories as $c) {
            $parentArr[ $c->id ] = $c->title;
        }
        


        $lists1 = Blog::latest()->paginate(20);
        //
        

        

        $data = compact( 'lists1','edit','parentArr' );

        return view('backend.inc.blog.edit',$data);
    }

    
    public function editData(Request $request, $id)
    {
        //
        $rules = [
            'title'          => 'required',
            
            
        ];
        $request->validate( $rules );
        

        $obj = Blog::findOrFail( $id );
        $obj->title           = $request->title;
        $obj->slug            = $request->slug;
        $obj->slug            = $request->slug == '' ? Str::slug($request->title) : Str::lower($request->slug); 
        $obj->excerpt         = $request->excerpt;
        $obj->description     = $request->description;
        $obj->cid             = $request->cid;
        $obj->seo_title       = $request->seo_title;
        $obj->seo_keywords        = $request->seo_keywords;
        $obj->seo_description = $request->seo_description;
        
        if($request->hasFile('image'))  { 
            
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(750, 500);
            $image_resize->save(public_path('imgs/blogs/' .$filename));
            $obj->image   = $image->getClientOriginalName();
            // $banner_resize = Image::make($image->getRealPath());
            // $banner_resize->resize(750,500);
            // $banner_resize->save(public_path('imgs/blogs/original/' .$filename));
        }
        
        $obj->save();

        return redirect( url('admin-control/blog') )->with('success', 'Success! A record has been updated.');
    }
     public function remove(  $id ){
         
        $social = Blog::findOrFail($id);

        $social->delete();


        return back();
    }

    public function removeMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'checked' => 'required',
		]);

		if ($validator->fails()) {

			return back()->with('deleted', 'Please select one of them to delete');
		}

		foreach ($request->checked as $checked) {

			$this->remove($checked);
			
		}

		return back()->with('deleted', 'Blogs has been deleted');
    }

   
}
