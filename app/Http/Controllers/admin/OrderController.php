<?php

namespace App\Http\Controllers\admin;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\model\Setting;
use App\model\Order;




class OrderController extends Controller {
    
    public function order()
    {
        $order = Order::with('billingaddress')->get();
        $setting = Setting::findOrFail(1);
        // dd($order);
        $data  = compact('order','setting');

        return view('backend.inc.order',$data);
    }
    public function infopage(Order $list, Request $request)
    {
        $status = $request->status;
        $ids = $request->id;
        
        
          if($ids!='' & $status !=''){
              
              $iq = Order::findOrFail($ids);

              $iq->status = $status;
              $iq->save();
              
          }
        

        $setting = Setting::find(1);
        $lists = Order::where('id', $list->id)->first();
        // set page and title ------------------
        
        $data  = compact('lists', 'setting');
        // return data to view
        return view('backend.inc.order-info',$data);
    }
    public function change_status(Request $request, Order $order)
    {
        $order->update([ $request->field => $request->status ]);

        return redirect()->back()->with('success', "{$request->field} status has been changed.");
    }
    
}