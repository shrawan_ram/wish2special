<?php

namespace App\Http\Controllers\admin;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\model\City;
use App\model\State;
class CityController extends Controller{
   
    public function index(request $request){
        $query = City::withCount('vendor')->latest();
    
        if( !empty( $request->name ) ) {
            $query->where('name', 'LIKE', '%'.$request->name.'%');
        }
        $city = $query->paginate(20);
        
        $data = compact( 'city' ); // Variable to array convert
        return view('backend.inc.city.index', $data);
    }   
    public function add()
    {
        $state = State::get();
        $parentArr = [null => 'Select State'];
            if (!$state->isEmpty()) {
            foreach ($state as $mcat) {
            $parentArr[$mcat->id] = $mcat->name;
            }
        }
        $data = compact('parentArr');
        return view('backend.inc.city.add', $data);
    }

    
    public function addData(Request $request)
    {
        //
        $rules = [
            'name'        => 'required'           
        ];
        $request->validate( $rules );
        $obj = new City;
        $obj->name       = $request->name;       
        $obj->slug        = $request->slug == '' ? Str::slug($request->name) : Str::lower($request->slug); 
        $obj->sid      = $request->sid; 

        
        $obj->save();
        return redirect( url('admin-control/city/') )->with('success', 'Success! New record has been added.');
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit = City::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();

        $state = State::get();
        $parentArr = [
            ''  => 'Select State'
        ];

        foreach($state as $c) {
            $parentArr[ $c->id ] = $c->name;
        }
        $data = compact('parentArr','edit');
        return view('backend.inc.city.edit', $data);
    }

    
    public function editData(Request $request, $id)
    {
        //
        $rules = [
            'name'        => 'required'
            
        ];
        
        $request->validate( $rules );
        $obj = City::findOrFail( $id ); 
        $obj->name       = $request->name;       
        $obj->slug           = $request->slug == '' ? Str::slug($request->name) : Str::lower($request->slug); 
        $obj->sid         = $request->sid;


        $obj->save();
        
        
        // $obj->image  = $request->$file->getClientOriginalName();
        $obj->save();

        return redirect( url('admin-control/city') )->with('success', 'Success! A record has been updated.');
    }
     public function remove(  $id ){
         
        $social = City::findOrFail($id);

        $social->delete();


        return back();
    }

    public function removeMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'checked' => 'required',
		]);

		if ($validator->fails()) {

			return back()->with('deleted', 'Please select one of them to delete');
		}

		foreach ($request->checked as $checked) {

			$this->remove($checked);
			
		}

		return back()->with('deleted', 'City has been deleted');
    }
    
}
