<?php

namespace App\Http\Controllers\admin;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\model\Category;
class CategoryController extends Controller{
   
    public function index(request $request){
        $query = Category::withCount('products')->latest();
    
        if( !empty( $request->category ) ) {
            $query->where('category', 'LIKE', '%'.$request->category.'%');
        }
        $category = $query->paginate(20);
        $data = compact( 'category' ); // Variable to array convert
        return view('backend.inc.category.index', $data);
    }   
    public function add()
    {
        $m_category = Category::where('parent', null)->get();
        $parentArr = [null => 'ROOT'];
            if (!$m_category->isEmpty()) {
            foreach ($m_category as $mcat) {
            $parentArr[$mcat->id] = $mcat->title;
            }
        }
        $data = compact('parentArr');
        return view('backend.inc.category.add', $data);
    }

    
    public function addData(Request $request)
    {
        //
        $rules = [
            'title'        => 'required'           
        ];
        $request->validate( $rules );
        $obj = new Category;
        $obj->title       = $request->title;       
        $obj->slug        = $request->slug == '' ? Str::slug($request->title) : Str::lower($request->slug); 
        $obj->parent      = $request->parent; 

        if($request->hasFile('image_i'))  
        { 
            $image        = $request->file('image_i');
            $filename     = $image->getClientOriginalName('image_i');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(480,480);
            $image_resize->save(public_path('imgs/category/' .$filename));
            $obj->image_i   = $image->getClientOriginalName();
        }

        $obj->save();
        return redirect( url('admin-control/category/') )->with('success', 'Success! New record has been added.');
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit = Category::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();

        $categories = Category::get();
        $parentArr = [
            ''  => 'Select Category'
        ];

        foreach($categories as $c) {
            $parentArr[ $c->id ] = $c->title;
        }
        $data = compact('parentArr','edit');
        return view('backend.inc.category.edit', $data);
    }

    
    public function editData(Request $request, $id)
    {
        //
        $rules = [
            'title'        => 'required'
            
        ];
        
        $request->validate( $rules );
        $obj = Category::findOrFail( $id ); 
        $obj->title       = $request->title;       
        $obj->slug           = $request->slug == '' ? Str::slug($request->title) : Str::lower($request->slug); 
        $obj->parent         = $request->parent;

        if($request->hasFile('image_i'))  
        { 
            $image        = $request->file('image_i');
            $filename     = $image->getClientOriginalName('image_i');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(480,480);
            $image_resize->save(public_path('imgs/category/' .$filename));
            $obj->image_i   = $image->getClientOriginalName();
        }

        $obj->save();
        
        
        // $obj->image  = $request->$file->getClientOriginalName();
        $obj->save();

        return redirect( url('admin-control/category') )->with('success', 'Success! A record has been updated.');
    }
     public function remove(  $id ){
         
        $social = Category::findOrFail($id);

        $social->delete();


        return back();
    }

    public function removeMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'checked' => 'required',
		]);

		if ($validator->fails()) {

			return back()->with('deleted', 'Please select one of them to delete');
		}

		foreach ($request->checked as $checked) {

			$this->remove($checked);
			
		}

		return back()->with('deleted', 'Category has been deleted');
    }
    
}
