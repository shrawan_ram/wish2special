<?php

namespace App\Http\Controllers\admin;

use Intervention\Image\ImageManagerStatic as Image;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\model\Client;

use App\Model\Category;

class ClientController extends Controller{
   
    public function index(request $request){

        $query = Client::latest();

        if( !empty( $request->title ) ) {
            $query->where('title', 'LIKE', '%'.$request->title.'%');
        }


        $lists1 = $query->paginate(20);        

        $data = compact( 'lists1' ); // Variable to array convert
        return view('backend.inc.client.index', $data);
    }
    

    

   
    public function add()
    {
        //

        $categories = Category::get();
        $parentArr = [
            ''  => 'Select Category'
        ];

        foreach($categories as $c) {

            $parentArr[ $c->id ] = $c->category;
        }
        $data = compact('parentArr');
        return view('backend.inc.client.add',$data);
    }

    
    public function addData(Request $request)
    {
        //
        $rules = [
            'name'             => 'required',            
                ];
            // $file = $request->file('image','image_hover');
                   
        $request->validate( $rules );
        $obj = new Client;
        $obj->name       = $request->name;
        $obj->excerpt    = $request->excerpt;
        $obj->work       = $request->work;

        if($request->hasFile('image'))  { 
            
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(750, 500);
            $image_resize->save(public_path('imgs/client/' .$filename));
            $obj->image   = $image->getClientOriginalName();
            
        }
        
        
        // $obj->image  = $request->$file->getClientOriginalName();
        
        $obj->save();

        return redirect( url('admin-control/client/') )->with('success', 'Success! New record has been added.');
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit = Client::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();
       


        $lists1 = Client::latest()->paginate(20);
        //
        

        

        $data = compact( 'lists1','edit');

        return view('backend.inc.client.edit',$data);
    }

    
    public function editData(Request $request, $id)
    {
        //
        $rules = [
            'name'          => 'required',
            
            
        ];
        $request->validate( $rules );
        

        $obj = Client::findOrFail( $id );
        

        $obj->name       = $request->name;
        $obj->excerpt    = $request->excerpt;
        $obj->work       = $request->work;

        if($request->hasFile('image'))  { 
            
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(750, 500);
            $image_resize->save(public_path('imgs/client/' .$filename));
            $obj->image   = $image->getClientOriginalName();
            
        }
        
        $obj->save();

        return redirect( url('admin-control/client') )->with('success', 'Success! A record has been updated.');
    }
     public function remove(  $id ){
        $remove = Client::where('id',$id)->delete();
        return redirect( url('admin-control/client') )->with('success', 'Success! A record has been deleted.');   
    }

    public function removeMultiple(Request $request)
    {
        $check = $request->check; // input type="checkbox" name="check[]"
        Client::whereIn("id", $check)->delete(); // DELETE FROM news WHERE news_id IN (3,5,4)

        return redirect()->back()->with('success', 'Item(s) removed.');
    }

   
}
