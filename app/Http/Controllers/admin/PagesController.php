<?php

namespace App\Http\Controllers\admin;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\Pages;
use App\model\Order;
use App\User;




class PagesController extends Controller {
    
    public function user()
    {
        $user = User::with('roles')->where('role',3)->get();
        
        // dd($order);
        $data  = compact('user');

        return view('backend.inc.user',$data);
    }

   
    public function privacyedit( Request $request, $id ) {

        $edit = Pages::findOrFail( 1 );
        $request->replace($edit->toArray());
        $request->flash();

        return view('backend.inc.privacy', compact('edit'));

    }
    public function privacyeditData( Request $request, $id ) {
        $rules = [
            'title'        => 'required'
            
        ];
        $request->validate( $rules );
        

        $obj = Pages::findOrFail( $id );
        $obj->title         = $request->title;       
        $obj->description      = $request->description;
        $obj->seo_title      = $request->seo_title;
        $obj->seo_keywords      = $request->seo_keywords;
        $obj->seo_description      = $request->seo_description;
        

        if($request->hasFile('image'))  { 
            
            $image        = $request->file('image');
            $filename     = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(445, 450);
            $image_resize->save(public_path('imgs/about/' .$filename)); 
            $obj->image   = $image->getClientOriginalName();     
        }
        $obj->save();

        return redirect()->back()->with('success', 'Success! A record has been updated.');
    }
    public function termedit( Request $request, $id ) {

        $edit = Pages::findOrFail( 2 );
        $request->replace($edit->toArray());
        $request->flash();

        return view('backend.inc.term', compact('edit'));

    }
    public function termeditData( Request $request, $id ) {
        $rules = [
            'title'        => 'required'
            
        ];
        $request->validate( $rules );
        

        $obj = Pages::findOrFail( $id );
        $obj->title         = $request->title;       
        $obj->description      = $request->description;
        $obj->seo_title      = $request->seo_title;
        $obj->seo_keywords      = $request->seo_keywords;
        $obj->seo_description      = $request->seo_description;
        

        if($request->hasFile('image'))  { 
            
            $image        = $request->file('image');
            $filename     = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(445, 450);
            $image_resize->save(public_path('imgs/about/' .$filename)); 
            $obj->image   = $image->getClientOriginalName();     
        }
        $obj->save();

        return redirect()->back()->with('success', 'Success! A record has been updated.');
    }
    public function securityedit( Request $request, $id ) {

        $edit = Pages::findOrFail( 3 );
        $request->replace($edit->toArray());
        $request->flash();

        return view('backend.inc.security', compact('edit'));

    }
    public function securityeditData( Request $request, $id ) {
        $rules = [
            'title'        => 'required'
            
        ];
        $request->validate( $rules );
        

        $obj = Pages::findOrFail( $id );
        $obj->title         = $request->title;       
        $obj->description      = $request->description;
        $obj->seo_title      = $request->seo_title;
        $obj->seo_keywords      = $request->seo_keywords;
        $obj->seo_description      = $request->seo_description;
        

        if($request->hasFile('image'))  { 
            
            $image        = $request->file('image');
            $filename     = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(445, 450);
            $image_resize->save(public_path('imgs/about/' .$filename)); 
            $obj->image   = $image->getClientOriginalName();     
        }
        $obj->save();

        return redirect()->back()->with('success', 'Success! A record has been updated.');
    }
    

// SELECT * FROM `news` WHERE `news_id` = 1 or `news_id` = 2
    // News::where('news_id', 1)->orWhere('news_id', 2)->get();
}
