<?php

namespace App\Http\Controllers\admin;

use Intervention\Image\ImageManagerStatic as Image;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\model\Vendor;
use Illuminate\Support\Facades\Validator;
use App\model\City;
use App\model\State;

class VendorController extends Controller{
   
    public function index(request $request){

        $query = Vendor::withCount('products')->latest()->with('city');

        if( !empty( $request->shop_name ) ) {
            $query->where('shop_name', 'LIKE', '%'.$request->shop_name.'%');
        }


        $vendor = $query->paginate(20);
        // dd($vendor);        

        $data = compact( 'vendor' ); // Variable to array convert
        return view('backend.inc.vendor.index', $data);
    }
    public function add()
    {
        $city = City::get();
        $cityArr = [
            ''  => 'Select City'
        ];

        foreach($city as $c) {

            $cityArr[ $c->id ] = $c->name;
        }
        $state = State::get();
        $stateArr = [
            ''  => 'Select State'
        ];

        foreach($state as $c) {

            $stateArr[ $c->id ] = $c->name;
        }
        $data = compact('cityArr', 'stateArr');
        return view('backend.inc.vendor.add',$data);
    }

    
    public function addData(Request $request)
    {
        //
        $rules = [
            'shop_name'             => 'required',            
                ];
            // $file = $request->file('image','image_hover');
                   
        $request->validate( $rules );
        $obj = new Vendor;
        $obj->shop_name             = $request->shop_name;
        $obj->owner_name             = $request->owner_name;
        $obj->pincode             = $request->pincode;
        $obj->slug              = $request->slug == '' ? Str::slug($request->shop_name) : Str::lower($request->slug);       
        $obj->city_id               = $request->city_id;
        $obj->address               = $request->address;
        $obj->state_id               = $request->state_id;

        if($request->hasFile('image'))  { 
            
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(750, 500);
            $image_resize->save(public_path('imgs/vendors/' .$filename));
            $obj->image   = $image->getClientOriginalName();
        }
        
        $obj->save();

        return redirect( url('admin-control/vendor/') )->with('success', 'Success! New record has been added.');
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit = Vendor::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();
        $city = City::get();
        $cityArr = [
            ''  => 'Select City'
        ];

        foreach($city as $c) {

            $cityArr[ $c->id ] = $c->name;
        }
        $state = State::get();
        $stateArr = [
            ''  => 'Select State'
        ];

        foreach($state as $c) {

            $stateArr[ $c->id ] = $c->name;
        }
        


        $lists1 = Vendor::latest()->paginate(20);
        //
        

        

        $data = compact('lists1', 'edit', 'cityArr', 'stateArr');

        return view('backend.inc.vendor.edit',$data);
    }

    
    public function editData(Request $request, $id)
    {
        //
        $rules = [
            'shop_name'          => 'required',
            
            
        ];
        $request->validate( $rules );
        

        $obj = Vendor::findOrFail( $id );
        $obj->shop_name        = $request->shop_name;
        $obj->owner_name       = $request->owner_name;
        $obj->pincode          = $request->pincode;
        $obj->slug             = $request->slug == '' ? Str::slug($request->shop_name) : Str::lower($request->slug);       
        $obj->city_id          = $request->city_id;
        $obj->address          = $request->address;
        $obj->state_id         = $request->state_id;

        if($request->hasFile('image'))  { 
            
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(750, 500);
            $image_resize->save(public_path('imgs/vendors/' .$filename));
            $obj->image   = $image->getClientOriginalName();
        }
        
        $obj->save();

        return redirect( url('admin-control/vendor') )->with('success', 'Success! A record has been updated.');
    }
     public function remove(  $id ){
         
        $social = Vendor::findOrFail($id);

        $social->delete();


        return back();
    }

    public function removeMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'checked' => 'required',
		]);

		if ($validator->fails()) {

			return back()->with('deleted', 'Please select one of them to delete');
		}

		foreach ($request->checked as $checked) {

			$this->remove($checked);
			
		}

		return back()->with('deleted', 'Vendor has been deleted');
    }

   
}
