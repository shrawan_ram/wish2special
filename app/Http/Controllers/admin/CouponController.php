<?php

namespace App\Http\Controllers\admin;

use Intervention\Image\ImageManagerStatic as Image;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\model\Coupon;
use Illuminate\Support\Facades\Validator;
use App\model\Category;

class CouponController extends Controller{
   
    public function index(request $request){

        $query = Coupon::latest();

        if( !empty( $request->code ) ) {
            $query->where('code', 'LIKE', '%'.$request->code.'%');
        }


        $coupon = $query->paginate(20);        

        $data = compact( 'coupon' ); // Variable to array convert
        return view('backend.inc.coupon.index', $data);
    }
    

    

   
    public function add()
    {
        //
        $disArr = [
            ''  => 'Select Discount Type',
            "Flat" => "Flat",
            "Percent" => "Percent",
        ];
        $data = compact('disArr');
        return view('backend.inc.coupon.add',$data);
    }

    
    public function addData(Request $request)
    {
        $record         = new Coupon;
        $input          = $request->record;
        // $input['slug']  = $input['slug'] == '' ? Str::slug($input['title'], '-') : $input['slug'];
        $record->fill($input);
        if ($record->save()) {
            return redirect(url('admin-control/coupon/'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(url('admin-control/coupon/'))->with('danger', 'Error! Something going wrong.');
        }
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit      = Coupon::find($id);
        $edit_data = [
            'record' => $edit->toArray()
        ];
        $request->replace($edit_data);
        //send to view
        $request->flash();
        // set page and title ------------------

        $disArr = [
            ''  => 'Select Discount Type',
            "Flat" => "Flat",
            "Percent" => "Percent",
        ];
        $lists1 = Coupon::latest()->paginate(20);
        $data = compact( 'lists1','edit','disArr');

        return view('backend.inc.coupon.edit',$data);
    }

    
    public function editData(Request $request, $id)
    {
        $record         = Coupon::find($id);
        $input          = $request->record;
        // $input['slug']  = $input['slug'] == '' ? Str::slug($input['title'], '-') : $input['slug'];
        $record->fill($input);
        if ($record->save()) {
            return redirect(url('admin-control/coupon/'))->with('success', 'Success! Record has been edided');
        }
    }
     public function remove(  $id ){
         
        $social = Coupon::findOrFail($id);

        $social->delete();


        return back();
    }

    public function removeMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'checked' => 'required',
		]);

		if ($validator->fails()) {

			return back()->with('deleted', 'Please select one of them to delete');
		}

		foreach ($request->checked as $checked) {

			$this->remove($checked);
			
		}

		return back()->with('deleted', 'Coupon has been deleted');
    }

   
}
