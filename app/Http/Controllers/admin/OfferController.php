<?php

namespace App\Http\Controllers\admin;

use Intervention\Image\ImageManagerStatic as Image;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\model\Offer;

use App\model\Category;

class OfferController extends Controller{
   
    public function index(request $request){

        $query = Offer::latest();

        if( !empty( $request->title ) ) {
            $query->where('title', 'LIKE', '%'.$request->title.'%');
        }


        $offer = $query->paginate(20);        

        $data = compact( 'offer' ); // Variable to array convert
        return view('backend.inc.offer.index', $data);
    }
    

    

   
    public function add()
    {
        // $categories = Category::get();
        // $parentArr = [
        //     ''  => 'Select Category'
        // ];

        // foreach($categories as $c) {

        //     $parentArr[ $c->id ] = $c->category;
        // }
        // $data = compact('parentArr');
        return view('backend.inc.offer.add');
    }

    
    public function addData(Request $request)
    {
        //
        $rules = [
            'title'             => 'required',            
                ];
                  
            
           
        $request->validate( $rules );
        $obj = new offer;
        $obj->title             = $request->title;
        $obj->price             = $request->price;
        
        if($request->hasFile('image'))  { 
            
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(1145, 200);
            $image_resize->save(public_path('imgs/offer/' .$filename));
            $obj->image   = $image->getClientOriginalName();
            
        }
        // $obj->image  = $request->$file->getClientOriginalName();
        
        $obj->save();

        return redirect( url('admin-control/offer/') )->with('success', 'Success! New record has been added.');
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit = Offer::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();
        
        $lists1 = Offer::latest()->paginate(20);

        $data = compact( 'lists1','edit' );

        return view('backend.inc.offer.edit',$data);
    }

    
    public function editData(Request $request, $id)
    {
        $rules = [
            'title'          => 'required',           
            
        ];
        $request->validate( $rules );
        

        $obj = Offer::findOrFail( $id );
        $obj->title           = $request->title;
        $obj->price           = $request->price;
        
        if($request->hasFile('image'))  { 
            
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName('image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(1145, 200);
            $image_resize->save(public_path('imgs/offer/' .$filename));
            $obj->image   = $image->getClientOriginalName();
            
        }
        
        $obj->save();

        return redirect( url('admin-control/offer') )->with('success', 'Success! A record has been updated.');
    }
     public function remove(  $id ){
        $remove = Offer::where('id',$id)->delete();
        return redirect( url('admin-control/offer') )->with('success', 'Success! A record has been deleted.');   
    }

    public function removeMultiple(Request $request)
    {
        $check = $request->check; // input type="checkbox" name="check[]"
        Offer::whereIn("id", $check)->delete(); // DELETE FROM news WHERE news_id IN (3,5,4)

        return redirect()->back()->with('success', 'Item(s) removed.');
    }

   
}
