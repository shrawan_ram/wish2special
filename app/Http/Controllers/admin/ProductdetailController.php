<?php

namespace App\Http\Controllers\admin;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\model\Productdetail;
use App\model\Product;
use App\model\Unit;
use App\model\Category;

class ProductdetailController extends Controller{
   
    public function index(request $request){

        $query = Productdetail::latest()->with('product');
        

        // if( !empty( $request->title ) ) {
        //     $query->where('title', 'LIKE', '%'.$request->title.'%');
        // }


        $product = $query->get();
             // dd($product);  

        $data = compact( 'product' ); // Variable to array convert
        return view('backend.inc.product-detail.index', $data);
    }
    

    

   
    public function add()
    {
        //

        $products = Product::get();
        $productArr = [
            ''  => 'Select Product'
        ];

        foreach($products as $c) {
            $productArr[ $c->id ] = $c->title;
        }

        $units = Unit::get();
        $unitArr = [
            ''  => 'Select Unit'
        ];

        foreach($units as $c) {
            $unitArr[ $c->short_name ] = $c->title;
        }
        $data = compact('productArr', 'unitArr');
        return view('backend.inc.product-detail.add',$data);
    }

    
    public function addData(Request $request)
    {
          $D = count($request->volume);
          // dd($D);
         $p =  new Productdetail();
        for($i=0; $i<$D; ){  

        $data = [
        'p_id' => $request->p_id,
        'volume' =>  $request->volume[$i],
        'unit' => $request->unit[$i],
        'weight' => trim($request->volume[$i].' '.$request->unit[$i]),
        'regular_price' => $request->regular_price[$i],
        'discount' =>  $request->discount[$i],
        'sale_price' => $request->sale_price[$i]
        ] ;
        // echo $request->volume[$i];   
        
        // $p->save($data);
        Productdetail::insert($data);

        $i = $i+1;
        }
        
        // $obj->image  = $request->$file->getClientOriginalName();    

        return redirect( url('admin-control/product-detail/') )->with('success', 'Success! New record has been added.');
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit = Productdetail::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();
        $products = Product::get();
        $productArr = [
            ''  => 'Select Product'
        ];

        foreach($products as $c) {
            $productArr[ $c->id ] = $c->title;
        }

        $units = Unit::get();
        $unitArr = [
            ''  => 'Select Unit'
        ];

        foreach($units as $c) {
            $unitArr[ $c->short_name ] = $c->title;
        }
        


        $lists1 = Productdetail::latest()->paginate(20);
        //
        

        

        $data = compact( 'lists1','edit','productArr', 'unitArr' );

        return view('backend.inc.product-detail.edit',$data);
    }

    
    public function editData(Request $request, $id)
    {
             

         
         $obj =  Productdetail::findOrFail( $id );
        
         $obj->p_id  = $request->p_id;
         $obj->volume  =  $request->volume;
         $obj->unit  = $request->unit;
         $obj->regular_price  = $request->regular_price;
         $obj->discount  =  $request->discount;
         $obj->sale_price  = $request->sale_price;
         
        
        $obj->save();
       
        

        return redirect( url('admin-control/product-detail/') )->with('success', 'Success! New record has been added.');
    }
     public function remove(  $id ){
         
        $social = Productdetail::findOrFail($id);

        $social->delete();


        return back();
    }

    public function removeMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'checked' => 'required',
		]);

		if ($validator->fails()) {

			return back()->with('deleted', 'Please select one of them to delete');
		}

		foreach ($request->checked as $checked) {

			$this->remove($checked);
			
		}

		return back()->with('deleted', 'Products has been deleted');
    }
     

   
}
