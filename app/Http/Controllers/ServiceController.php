<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\model\Category;
use App\model\Product;
use App\model\Setting;


use Illuminate\Routing\Controller as BaseController;

class ServiceController extends BaseController {

    public function index(Request $request, $cslug = null)
    {
        $categoryInfo = [];
        // if (!empty($cslug)) {
        //     $categoryInfo   = Category::where('slug', $cslug)->where('category_is_deleted', 'N')->select('id')->first();
        // }

        $query = Product::with(['cat']);
        if (!empty($categoryInfo->id)) {
            $query->where('category_id', $categoryInfo->id);
        }
        if( !empty( $request->s ) ) {
            $query->where('name', 'LIKE', '%'.$request->s.'%');
        }

        

        $products   = $query->paginate(1000);

        // Get All Categories
        $categories     = Category::withCount('products')->has('products')->where('category_is_deleted', 'N')->where('parent', null)->orderBy('title', 'ASC')->get();

        $data = compact('categoryInfo', 'products', 'categories', 'cslug');
        return view('frontend.inc.service', $data);
    }

    public function single( Product $slug  )
    {
        $setting = Setting::findOrFail(1);
        $data1 = Product::where('id','!=', $slug->id)->paginate(3)->toArray();
        $data    = compact('setting','slug','data1');
        return view('frontend.inc.menu_single',$data);
    }
}
