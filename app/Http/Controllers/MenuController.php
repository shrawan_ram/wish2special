<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\model\Product;
use App\model\Setting;

class MenuController extends BaseController {
    public function index(Request $request) {
            
        $city_id = session()->get('city_session');

        $query       = Product::latest()->withCount('p');

        if( !empty( $request->title ) ) {
            $query->where('title', 'LIKE', '%'.$request->title.'%');
        }
        
        $menu       = $query->whereHas('vendor', function ($q) use ($city_id) {
            $q->where('city_id', $city_id);
        })->paginate(60);
       
        $setting = Setting::findOrFail(1);  
        $list = compact('menu','setting');          
        
        return view('frontend.inc.menu',$list);
    }
}