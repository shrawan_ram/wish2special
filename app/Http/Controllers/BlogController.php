<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\model\Blog;
use App\model\Category;
use App\model\Comment;
use App\model\Setting;

class BlogController extends BaseController {
    public function index(Request $request, Category $category) {

        $category1   = Category::with('service')->latest()->get();
        
        $cat_name = $category->title;

        $query       = Blog::latest();

        if (!empty($category->id)) {
            $query->where('cid', $category->id);
        }
        if (!empty($request->tags)) {
            $tagInfo = Tag::where('slug', $request->tags)->first();
            $query->whereRaw('FIND_IN_SET(?, tags)', @$tagInfo->id);
        }
        if( !empty( $request->title ) ) {
            $query->where('title', 'LIKE', '%'.$request->title.'%');
        }
        
        $blog       = $query->paginate(60);

        $latestblog=Blog::latest()->paginate(5);
        $setting = Setting::findOrFail(1);  
        $list = compact('blog','category1','cat_name','setting','latestblog');          
        
        return view('frontend.inc.blog',$list);
    }

    public function single($slug) {
        $category   = Category::with('service')->latest()->get();
        // $tag   = Tag::latest()->get()->take(7);
        
        $blog = Blog::latest()->where('slug', $slug)->with('cat')->first();

        $latestblog=Blog::latest()->paginate(3);
        $comment = Comment::where('blogid', $blog->id)->where('status', 'show')->with('user')->get();
        $setting = Setting::findOrFail(1);  
        $list = compact('blog','category','setting','latestblog','comment');         
        // dd($list); 
        
        return view('frontend.inc.single-blog',$list);
    }

    public function save_comment(Request $request)
    {
        $rules = [
            'name'      => 'required',
            'email'     => 'required',
            'comment'   => 'required'
        ];
        $request->validate($rules);

        $comment            =   new Comment;
        $comment->blogid    =   $request->blogid;
        $comment->name      =   $request->name;
        $comment->email     =   $request->email;
        $comment->comment   =   $request->comment;

        $comment->save();

        return redirect()->back()->with('success', 'Your comment has been placed.');
    }

}
