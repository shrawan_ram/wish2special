<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\model\Product;
use App\model\Productdetail;
use App\model\Setting;

class CartController extends BaseController
{
    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pid' => 'required|numeric',
            'qty' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $re = [
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
            $code = 401;
        } else {
            $productInfo = Productdetail::find($request->pid);
        


            if (!empty($productInfo->id)) {
                $cart = session()->get('cart');

                $qty  = !empty($cart[ $request->pid ]) ? $cart[ $request->pid ]['qty'] + $request->qty : $request->qty;

                $cart[$request->pid] = [
                    'qty'   => $qty,
                    'is_jain_food' => 0
                ];


                session()->put('cart', $cart);
                $header_cart = view('frontend.template.header_cart')->render();

                $re = [
                    'message'       => $productInfo->name.' added to cart successfully.',
                    'header_cart'   => $header_cart
                ];                
                $code = 200;
            } else {
                $re = [
                    'message' => 'Product not found.'
                ];
                $code = 401;
            }
        }
        return response()->json($re, $code);
    }
    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'qty' => 'required|array',
        ]);

        if ($validator->fails()) {
            $re = [
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
            $code = 401;
        } else {
            $cart = session()->get('cart');

            foreach (request('qty') as $pid => $qty) {

                $cart[$pid]['qty'] = $qty;

            }
            $jain_food = request('jain_food');
            foreach($cart as $pid => $pData) {

                $cart[$pid]['is_jain_food'] = $jain_food[$pid] ?? 0;
            }
            session()->put('cart', $cart);

            $cartProducts = [];
            $totalPrice   = 0;
            if (!empty($cart)) {
                foreach ($cart as $pid => $pData) {
                    $productInfo = \App\model\Productdetail::find($pid);
                    if (!empty($productInfo->id)) {
                        $productInfo->qty   = $pData['qty'];
                        $productInfo->is_jain_food   = $pData['is_jain_food'] ?? 0;
                        $cartProducts[]     = $productInfo;
                        $totalPrice        += $productInfo->sale_price * $pData['qty'];
                    }
                }
            }

            $setting     = Setting::findOrFail(1);

            $header_cart = view('frontend.template.header_cart')->render();
            $cart_table  = view('frontend.template.cart', compact('cartProducts', 'totalPrice'))->render();
            $cart_footer = view('frontend.template.cart_footer', compact('totalPrice', 'setting', 'cartProducts'))->render();

            $re = [
                'message'       => 'Cart updated successfully.',
                'header_cart'   => $header_cart,
                'cart_table'    => $cart_table,
                'cart_footer'   => $cart_footer,
            ];
            $code = 200;
        }
        return response()->json($re, $code);
    }
    public function index()
    {
        $cart = session()->get('cart');
        $cartProducts = [];
        $totalPrice   = 0;
        if (!empty($cart)) {
            foreach ($cart as $pid => $pData) {
                $productInfo = \App\model\Productdetail::find($pid);
        
                if (!empty($productInfo->id)) {
                    $productInfo->qty   = $pData['qty'];
                    $productInfo->is_jain_food   = $pData['is_jain_food'] ?? 0;
                    $cartProducts[]     = $productInfo;
                    $totalPrice        += $productInfo->sale_price * $pData['qty'];
                }
            }
        }

        $setting = Setting::findOrFail(1);

        $data = compact('cartProducts', 'totalPrice', 'setting');
        return view('frontend.inc.cart', $data);
    }
    public function remove(Request $request)
    {
        
        if (empty(request('pid'))) {
            $re = [
                'message' => 'Product id is required.'
            ];
            $code = 401;
        } else {
            $cart = session()->get('cart');

            if (!empty($cart[$request->pid])) {

                unset($cart[$request->pid]);
            }
            session()->put('cart', $cart);

            $cartProducts = [];
            $totalPrice   = 0;

            if (!empty($cart)) {
                foreach ($cart as $pid => $pData) {
                    $productInfo = \App\model\Productdetail::find($pid);

                    if (!empty($productInfo->id)) {
                        $productInfo->qty   = $pData['qty'];
                        $productInfo->is_jain_food   = $pData['is_jain_food'] ?? 0;
                        $cartProducts[]     = $productInfo;
                        $totalPrice        += $productInfo->sale_price * $pData['qty'];
                    }
                }
            }

            $setting     = Setting::findOrFail(1);

            $header_cart = view('frontend.template.header_cart')->render();
            $cart_table  = view('frontend.template.cart', compact('cartProducts', 'totalPrice'))->render();
            $cart_footer = view('frontend.template.cart_footer', compact('totalPrice', 'setting', 'cartProducts'))->render();

            $re = [
                'message'       => 'Cart updated successfully.',
                'header_cart'   => $header_cart,
                'cart_table'    => $cart_table,
                'cart_footer'   => $cart_footer,
            ];
            $code = 200;
        }

        return response($re, $code);
    }
}
