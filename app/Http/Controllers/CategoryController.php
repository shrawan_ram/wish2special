<?php

namespace App\Http\Controllers;
use App\model\Category;
use App\model\Blog;
use App\model\Setting;

use Illuminate\Routing\Controller as BaseController;

class CategoryController extends BaseController {
    public function index() {
        $category1   = Category::latest()->get();
        $latestblog = Blog::latest()->paginate(15);
        $setting= Setting::findOrFail(1);
        $list = compact('latestblog','category1','setting');
    	return view('frontend.inc.category', $list);
    }
}
